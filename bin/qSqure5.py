# Plot all functions in a .csv file.

## Frequently used
## header
#poType,pion,chiral
#sigmaIMeVPion, 0.5
#sigmaIMeVChi, 3
#lMin, 0
#lMax, 10
##sqrt(5) = 2.2360679775, 
#qChar, 2.2360679775
#mambdaMeV, 1600
#nGauPion, 1500
## (c)
#nGauChi, 200
## bGauR = ... / mambda
#bGauR, 300
#bGauRPion, 10000
## file will be saved in ./data/. This should have only one value
#fileName,newE.csv
## 'a' (append) or 'w' (rewrite). This should have only one value
#writeMethod,a
## Check if the response is fitted well
#checkOpt,on
## This should be sigmaRMeshMax = 250 / const.HBarC (c)
#sigmaRMeshMax, 200

## header
#poType,pion,chiral
#sigmaIMeVPion, 0.5
#sigmaIMeVChi, 3
#lMin, 0
#lMax, 10
##sqrt(5) = 2.2360679775, 
#qChar, 2.2360679775
#mambdaMeV, 400
#nGauPion, 600
## (c)
#nGauChi, 200
## bGauR = ... / mambda
#bGauR, 300
#bGauRPion, 2000
## file will be saved in ./data/. This should have only one value
#fileName,newE.csv
## 'a' (append) or 'w' (rewrite). This should have only one value
#writeMethod,w
## Check if the response is fitted well
#checkOpt,on
## This should be sigmaRMeshMax = 250 / const.HBarC (c)
#sigmaRMeshMax, 200

import binCOMMON # so that we can import lib in src
import matplotlib.pyplot as plt
import src.tool.inout as ios # read csv from input.csv
import constant as const
import numpy as np
import pandas as pd
import src.tool.plotTool as ptt # plot tool

fileName = 'newE.csv'
func = ios.readFunc('../data/' + fileName, 'r')

plt.figure(0)

cache = {}
for i in range(len(func)):
    aFunc = func[i]
    key = aFunc['funcName'][0]
    try:
        ptt.plotABand(func[cache[key]], func[i], key, legend = aFunc['funcName'][0])
    except KeyError:
        cache[key] = i

# Data from paper
loRPaper = pd.read_csv('../data/paperResult.csv', header = None).to_numpy() # read data from paper
plt.scatter(loRPaper[:, 0], loRPaper[:, 1], label = 'Data from paper', color = 'black') # print out the leading order

plt.xlabel(r'$\omega / $MeV')
plt.ylabel(r'R$(\omega) / ($MeV$^{-1})$')
plt.legend()
plt.grid()
plt.savefig('../fig/' + fileName[:-4] + '.pdf',  bbox_inches="tight")
plt.show()
