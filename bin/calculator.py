# calculate the response function with several parameters
import binCOMMON # so that we can import lib in src
import numpy as np
import src.tool.inout as ios # read csv from input.csv
import src.core.pionless.pionless as pionl # pionless solver
import src.exe.pionless.resPionUnitaryN2LO as pionlN2LO # pionless solver
import src.exe.chiral.chiralSolver as chi # pionless solver
import constant as const
from matplotlib import pyplot as plt
import csv

def outFuncPion(paras, omega, rMesh, writer, funcName, caphiWriter, sigmaRMesh0, caphi0, sigmaRMeshL, caphiL, eShift = 0):
    # print pion data to file
    paras['eShift'] = eShift
    ios.outFunc(paras, list(omega), list(rMesh), writer = writer, funcName = funcName) # Add NLO
    if caphiWriter: # write caphi
        if len(caphi0) > 0:
            ios.outFunc(paras, list(sigmaRMesh0), list(caphi0), writer = caphiWriter, funcName = funcName + ' caphi0')
        if len(caphiL) > 0:
            ios.outFunc(paras, list(sigmaRMeshL), list(caphiL), writer = caphiWriter, funcName = funcName + ' caphiL')

paras = ios.readIn('../input.csv') # this paras is a dict which contains all possible values of each para. e.g. paras = {'para1':[p11], 'para2':[p21, p22], ...}
l = ios.dict2list(paras) # to list without key. e.g. l = [[p1], [p21, p22], ...]
l = ios.combineDiff(l) # all possible combination e.g. l = [[p1, p21], [p1, p22], ...]

def list2dic():
    # convert a single row of data to dict
    i = 0
    for key, value in paras.items():
        try:
            paras[key] = row[i]
            i = i + 1
        except IndexError: # paras will be added with key 'elo' and 'enlo' at the last few codes
            break

def calculator(paras):
    # some variables need to be int
    nMax = int(paras['nMax'])
    nGauPion = int(paras['nGauPion'])
    nGauChi = int(paras['nGauChi'])
    lMax = int(paras['lMax'])
    lMin = int(paras['lMin'])
    mambda = paras['mambdaMeV'] / const.HBarC
    eloPion = paras['eloMeV'] / const.HBarC # this is only a parameters for the inversion bases, not the binding energy.
    aGau = paras['aGau'] * mambda
    bGau = paras['bGau'] * mambda
    aGauR = paras['aGauR'] / mambda
    bGauR = paras['bGauR'] / mambda
    bGauRPion = paras['bGauRPion'] / mambda
    sigmaIPion = paras['sigmaIMeVPion'] / const.HBarC
    sigmaIChi = paras['sigmaIMeVChi'] / const.HBarC
    NsigmaR = int(paras['NsigmaR'])
    sigmaRMax = paras['sigmaRMeshMax'] / const.HBarC
    # Start the calculation
    if paras['poType'] == 'pion' or paras['poType'] == 'unitaryPion':
        para = [paras['e0Inversion'] / const.HBarC, paras['e1']]
        (elo, omega, rMeshLO, enlo, omegaNLO, rMeshNLO, sigmaRMeshLO, caphiLO0, caphiLOL, sigmaRMeshNLO, caphiNLO0, caphiNLOL) = pionl.pionlessSolver(para = para, nMax = nMax, checkOpt = paras['checkOpt'], sigmaI = sigmaIPion , nGau = nGauPion, mambda = mambda, c0Method = paras['c0Method'], NsigmaR = NsigmaR, lMax = lMax, lMin = lMin, qChar = paras['qChar'], sigmaRMax = sigmaRMax, bGauR = bGauRPion, base = paras['basePion'], elo = eloPion, potype = paras['poType'], kappa0 = paras['kappa0'])
        return elo, omega, rMeshLO, enlo, omegaNLO, rMeshNLO, sigmaRMeshLO, caphiLO0, [], caphiLOL, sigmaRMeshNLO, caphiNLO0, [], caphiNLOL # no caphi2
    elif paras['poType'] == 'chiral' or paras['poType'] == 'chiralNLO':
        if(paras['poType'] == 'chiral'):
            order = 'LO'
        elif(paras['poType'] == 'chiralNLO'):
            order = 'NLO'
        (omega, rMesh, sigmaRMesh, caphi0, caphi2, caphiL) = chi.chiralSolver(mambda = mambda, nGau = nGauChi, aGau = paras['aGau'], bGau = bGau, aGauR = paras['aGauR'], bGauR = bGauR, qChar = paras['qChar'], sigmaI = sigmaIChi, NsigmaR = NsigmaR, checkOpt = paras['checkOpt'], lMin = lMin, lMax = lMax, sigmaRMeshMax = sigmaRMax, base0 = paras['base0'], base2 = paras['base2'], baseL = paras['baseL'], order = order)
        return -2.216587 / const.HBarC, omega, rMesh, 0, [], [], sigmaRMesh, caphi0, caphi2, caphiL, [], [], [], [] # No NLO in chiral case. -2.21 is the binding energy
    elif paras['poType'] == 'unitaryPionN2LO':
        para = [paras['e0Inversion'] / const.HBarC, paras['e1']]
        elo, omega, rMeshLO, enlo, omegaNLO, rMeshNLO, en2lo, omegaN2LO, rMeshN2LO, sigmaRMeshLO, caphiLO, caphiLOL, sigmaRMeshNLO, caphiNLO0, caphiNLOL, sigmaRMeshN2LO, caphiN2LO0, caphiN2LOL = pionlN2LO.pionUnitaryN2LO(para = para, nMax = nMax, checkOpt = paras['checkOpt'], sigmaI = sigmaIPion , nGau = nGauPion, mambda = mambda, c0Method = paras['c0Method'], NsigmaR = NsigmaR, lMax = lMax, lMin = lMin, qChar = paras['qChar'], sigmaRMax = sigmaRMax, bGauR = bGauRPion, base = paras['basePion'], elo = eloPion, potype = paras['poType'], kappa0 = paras['kappa0'], kappa1 = paras['kappa1'])
        return elo, omega, rMeshLO, enlo, omegaNLO, rMeshNLO, en2lo, omegaN2LO, rMeshN2LO, sigmaRMeshLO, caphiLO, caphiLOL, sigmaRMeshNLO, caphiNLO0, caphiNLOL, sigmaRMeshN2LO, caphiN2LO0, caphiN2LOL

fileName = paras['fileName'][0]
method = paras['writeMethod'][0]
csvfile = open('../data/' + fileName, method)
writer = csv.writer(csvfile) # file for writing function
fileNameCaphi = paras['fileNameCaphi'][0]
if fileNameCaphi == 'off':
    caphiWriter = None
else:
    caphiFile = open('../data/' + fileNameCaphi, paras['writeMethodCaphi'][0])
    caphiWriter = csv.writer(caphiFile) # file for writing caphi

for row in l:
    list2dic() # convert row to a dic, which represent a set of parameters
    if paras['poType'] == 'unitaryPionN2LO':
        elo, omegaLO, rMeshLO, enlo, omegaNLO, rMeshNLO, en2lo, omegaN2LO, rMeshN2LO, sigmaRMeshLO, caphiLO0, caphiLOL, sigmaRMeshNLO, caphiNLO0, caphiNLOL, sigmaRMeshN2LO, caphiN2LO0, caphiN2LOL = calculator(paras)
    else:
        elo, omegaLO, rMeshLO, enlo, omegaNLO, rMeshNLO, sigmaRMeshLO, caphiLO0, caphiLO2, caphiLOL, sigmaRMeshNLO, caphiNLO0, caphiNLO2, caphiNLOL = calculator(paras)
    paras['elo'] = elo
    paras['enlo'] = enlo
    if paras['poType'] == 'pion' or paras['poType'] == 'unitaryPion' or paras['poType'] == 'unitaryPionN2LO':
        outFuncPion(paras, omegaLO, rMeshLO, writer, 'PionLO', caphiWriter, sigmaRMeshLO, caphiLO0, sigmaRMeshLO, caphiLOL, eShift = elo)
        outFuncPion(paras, omegaNLO, rMeshNLO, writer, 'PionNLO', caphiWriter, sigmaRMeshNLO, caphiNLO0, sigmaRMeshNLO, caphiNLOL, eShift = enlo)
        if paras['poType'] == 'unitaryPionN2LO':
            paras['en2lo'] = en2lo
            outFuncPion(paras, omegaN2LO, rMeshN2LO, writer, 'PionN2LO', caphiWriter, sigmaRMeshN2LO, caphiN2LO0, sigmaRMeshN2LO, caphiN2LOL, eShift = en2lo)
    elif paras['poType'] == 'chiral' or paras['poType'] == 'chiralNLO':
        paras['eShift'] = elo
        if(paras['poType'] == 'chiral'):
            ios.outFunc(paras, list(omegaLO), list(rMeshLO), writer = writer, funcName = 'Chiral')
        elif(paras['poType'] == 'chiralNLO'):
            ios.outFunc(paras, list(omegaLO), list(rMeshLO), writer = writer, funcName = 'ChiralNLO')
        if caphiWriter:
            if len(caphiLO0) > 0:
                ios.outFunc(paras, list(sigmaRMeshLO), list(caphiLO0), writer = caphiWriter, funcName = 'chiral caphi0')
            if len(caphiLO2) > 0:
                ios.outFunc(paras, list(sigmaRMeshLO), list(caphiLO2), writer = caphiWriter, funcName = 'chiral caphi2')
            if len(caphiLOL) > 0:
                ios.outFunc(paras, list(sigmaRMeshLO), list(caphiLOL), writer = caphiWriter, funcName = 'chiral caphiL')

if paras['checkOpt'] == 'on': # Output the check result
    plt.show()

