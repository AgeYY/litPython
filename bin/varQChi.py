# Plot all functions in a .csv file.

import binCOMMON # so that we can import lib in src
import matplotlib.pyplot as plt
import src.tool.inout as ios # read csv from input.csv
import constant as const
import numpy as np
import pandas as pd
import src.tool.plotTool as ptt # plot tool

def factorFL(q2, frl):
    # add the factor before int{d cos(theta)} f_l
    frl[:, 1] = frl[:, 1] * 137 / 3 / np.pi / (1 - (frl[:, 0] + 2.22)**2 / q2)
    return frl

frl = pd.read_csv('../data/fL2RLSeb.csv', header = None).to_numpy() # MeV
frl4p8 = frl[:11 ,:]
frl9 = frl[11:22,:]
frl16 = frl[22: ,:]

frl4p8 = factorFL(4.8 * const.HBarC**2, frl4p8)
frl9 = factorFL(9 * const.HBarC**2, frl9)
frl16 = factorFL(16 * const.HBarC**2, frl16)

fileName = 'varQChar.csv'
func = ios.readFunc('../data/' + fileName, 'r')

plt.figure(0)

plt.plot(frl4p8[:, 0], frl4p8[:, 1], 'x-', label = r'$q^2(Seb) = 4.8$')
plt.plot(frl9[:, 0], frl9[:, 1], 'o-', label = r'$q^2(Seb) = 9.0$')
plt.plot(frl16[:, 0], frl16[:, 1], '*-', label = r'$q^2(Seb) = 16$')

for i in range(len(func)):
    aFunc = func[i]
    ## add a factor for comparing with int{\omega_p}f_L
    #omega = np.array(aFunc['x'])
    #rl = np.array(aFunc['y'])
    #q = aFunc['qChar'][0]
    #print(omega, q)
    #rl = -rl * (omega*omega / q / q - 1) * 6 * np.pi * np.pi / 137.0
    #aFunc['y'] = rl
    # convert omega to E_np
    if aFunc['poType'][0] == 'chiral':
        aFunc['x'] = np.array(aFunc['x']) - 2.22 / const.HBarC
    elif aFunc['funcName'][0] == 'PionLO':
        aFunc['x'] = np.array(aFunc['x']) - 1.70 / const.HBarC
    label = r'$q^2 = $' + str(aFunc['qChar'][0]**2)[:4]
    ptt.plotAFunc(aFunc, label)
    ptt.printInfo(aFunc, i)

# Data from paper
loRPaper = pd.read_csv('../data/paperResult.csv', header = None).to_numpy() # read data from paper
plt.scatter(loRPaper[:, 0], loRPaper[:, 1], label = 'Data from paper', color = 'black') # print out the leading order

plt.xlabel(r'$E_{np} / $MeV')
plt.ylabel(r'R$(\omega) / ($MeV$^{-1})$')
plt.legend()
plt.grid()
plt.savefig('../fig/' + fileName[:-4] + '.pdf',  bbox_inches="tight")
plt.title(r'Response function with different $q^2$' + '\n' + r'(Seb): $\frac{1}{3 \pi \alpha} \frac{1}{(1 - \omega^2 / q^2)}\int{d cos(\theta)} f_L$')
plt.show()
