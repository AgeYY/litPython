# general ploter
import binCOMMON # so that we can import lib in src
import matplotlib.pyplot as plt
import src.tool.inout as ios # read csv from input.csv
import constant as const
import numpy as np
import pandas as pd
import src.tool.plotTool as ptt # plot tool
from optparse import OptionParser

class ploter():
    def __init__(self, fileName, figlab = 0):
        self.func = ios.readFunc(fileName, 'r')
    def plot(self, labList = None):
        self.fig, self.ax = plt.subplots()
        # Data from paper
        loRPaper = pd.read_csv('../data/paperResult.csv', header = None).to_numpy() # read data from paper
        self.ax.scatter(loRPaper[:, 0], loRPaper[:, 1], label = 'Data from paper', color = 'black') # print out the leading order
        for i in range(len(self.func)):
            aFunc = self.func[i]
            # convert omega to E_np
            if labList is None:
                ptt.plotAFunc(aFunc, str(i), ax = self.ax)
                ptt.printInfo(aFunc, i)
            else:
                ptt.plotAFunc(aFunc, labList[i], ax = self.ax)
                ptt.printInfo(aFunc, labList[i])
        plt.xlabel(r'$E_{np} / $MeV')
        plt.ylabel(r'R$(\omega) / ($MeV$^{-1})$')
        plt.legend()
        plt.grid()
        return self.fig, self.ax
    def shiftX(self, eShift = None):
        # you can also mannually shift the curve. eShift = [eShift0, eShift1, ...], with the number of eShift same as number of functions
        for i in range(len(self.func)):
            aFunc = self.func[i]
            if eShift is None:
                try:
                    aFunc['x'] = np.array(aFunc['x']) + aFunc['eShift'][0]
                except KeyError:
                    if aFunc['funcName'][0] == 'PionNLO':
                        aFunc['x'] = np.array(aFunc['x']) + aFunc['enlo'][0]
                    elif aFunc['poType'][0] == 'chiral' or aFunc['funcName'][0] == 'PionLO':
                        aFunc['x'] = np.array(aFunc['x']) + aFunc['elo'][0]
            else:
                aFunc['x'] = np.array(aFunc['x']) + eShift[i]

