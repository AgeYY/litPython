import binCOMMON # so that we can import lib in src
import matplotlib.pyplot as plt
import src.tool.inout as ios # read csv from input.csv
import constant as const
import numpy as np
import src.tool.plotTool as ptt # plot tool
import pandas as pd

fileName = '../data/note0PotMatter/zPotMatter.csv'

func = ios.readFunc(fileName, 'r')

plt.figure(0)

def myLabel(i):
    if i == 0:
        return 'Chiral, ' + r'$R_{L = 0}$'
    elif i == 1:
        return 'Chiral, ' + r'$R_{L > 0}$'
    elif i == 2:
        return 'PionNLO, ' + r'$R_{L = 0}$'
    elif i == 3:
        return 'PionNLO, ' + r'$R_{L > 0}$'
    elif i == 4:
        return 'zeroPot, ' + r'$R_{L = 0}$'
    elif i == 5:
        return 'zeroPot, ' + r'$R_{L > 0}$'

for i in range(len(func)):
    aFunc = func[i]
    # convert omega to E_np
    if aFunc['poType'][0] == 'chiral' or aFunc['funcName'][0] == 'PionNLO':
        aFunc['x'] = np.array(aFunc['x']) - 2.22 / const.HBarC
    elif aFunc['funcName'][0] == 'PionLO':
        aFunc['x'] = np.array(aFunc['x']) - 1.70 / const.HBarC
    ptt.plotAFunc(aFunc, myLabel(i))
    ptt.printInfo(aFunc, i)

# Data from paper
loRPaper = pd.read_csv('../data/paperResult.csv', header = None).to_numpy() # read data from paper
plt.scatter(loRPaper[:, 0], loRPaper[:, 1], label = 'Data from paper', color = 'black') # print out the leading order

plt.xlabel(r'$E_{np} / $MeV')
plt.ylabel(r'R$(\omega) / ($MeV$^{-1})$')
plt.legend()
plt.grid()
plt.show()
