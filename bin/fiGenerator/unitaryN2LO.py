# Plot all functions in a .csv file.

import binCOMMON # so that we can import lib in src
import matplotlib.pyplot as plt
import src.tool.inout as ios # read csv from input.csv
import constant as const
import numpy as np
import pandas as pd
import src.tool.plotTool as ptt # plot tool

# load unitaryPionN2LO
fileName = 'unitaryN2LO/res.csv'
func = ios.readFunc('../data/' + fileName, 'r')
# Data from paper
loRPaper = pd.read_csv('../data/paperResult.csv', header = None).to_numpy() # read data from paper
# load pion lo
fileName = 'unitaryN2LO/resPionLO.csv'
funcPionLO = ios.readFunc('../data/' + fileName, 'r')

nFunc = len(func)
seg = np.arange(0, nFunc, 3) # 3 functions which correspond to LO, NLO, N2LO, are plotted in single subfigure
seg = np.append(seg, nFunc)
print(len(seg))

# shift and cut
for funci in func:
    funci['x'] = np.array(funci['x']) + (-2.225) / const.HBarC
    funci['y'] = np.array(funci['y'])
    idx = funci['x'] < 200 / const.HBarC
    funci['x'] = funci['x'][idx]
    funci['y'] = funci['y'][idx]
for funci in funcPionLO:
    funci['x'] = np.array(funci['x']) + funci['eShift'][0]
    funci['y'] = np.array(funci['y'])
    idx = funci['x'] < 200 / const.HBarC
    funci['x'] = funci['x'][idx]
    funci['y'] = funci['y'][idx]

def lab(j):
    if j % 3 == 0:
        return 'UPion LO'
    elif j % 3 == 1:
        return 'UPion NLO'
    elif j % 3 == 2:
        return 'UPion N2LO'

textList = [{}] * 4 # each entry includes info about text's x, y positions and text
textList[0]['x'] = 90
textList[0]['y'] = -0.04
for i in range(1, 4):
    textList[0]['x'] = 90
    textList[i]['y'] = -0.025
textList[0]['text'] = r'$\kappa^0 = 0.05$' + ' fm'
textList[1]['text'] = r'$\kappa^0 = 0.1$' + ' fm'
textList[2]['text'] = r'$\kappa^0 = 0.15$' + ' fm'
textList[3]['text'] = r'$\kappa^0 = 0.185$' + ' fm'

plt.figure(0)
for i in range(1, len(seg)): # 4 pannel: kappa0 = 0.05, 0.1, 0.15, 1 / 5.4
    plt.subplot(2,2,i)
    if not i == 1:
        plt.axis([0,200, -0.002, 0.033])
    plt.scatter(loRPaper[:, 0], loRPaper[:, 1], label = 'Data from paper', color = 'black') # Plot paper's data
    ptt.plotAFunc(funcPionLO[0], 'Pionless LO') # plot pion lo
    for j in np.arange(seg[i-1], seg[i]):
        ptt.plotAFunc(func[j], lab(j)) # plot pion lo
    plt.grid()
    plt.legend()
plt.show()
