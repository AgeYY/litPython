# compare the three different inversion methods: tranditional, Normal equation, and Gradient decent
import matplotlib.pyplot as plt
import binCOMMON # so that we can import lib in src
import numpy as np
import pandas as pd
import src.tool.inout as ios # read csv from input.csv
import src.tool.plotTool as ptt # plot tool
import src.core.general.constant as const
from genPloter import ploter
import litInversion as litinv
from scipy.interpolate import interp1d
import myMath as mymath
from src.core.general.novelInv import NInv as ninv
import invNetWork as invnn

path = '../data/normalInversion/caphi.csv'
reader = ploter(path) # read functions in the file

def sumCap(reader, key, idx):
    # Sum over the first three caphi. key = 'x', or 'y'
    out = np.array(reader.func[idx[0]][key])
    out = out + np.array(reader.func[idx[1]][key])
    out = out + np.array(reader.func[idx[2]][key])
    return out

# Parameter
sigmaI = [[], []] 
mambda = [[], []] 
sigmaI[0] = reader.func[0]['sigmaIMeVChi'][0] / const.HBarC
sigmaI[1] = reader.func[-1]['sigmaIMeVChi'][0] / const.HBarC
mambda[0] = reader.func[0]['mambdaMeV'][0] / const.HBarC
mambda[1] = reader.func[-1]['mambdaMeV'][0] / const.HBarC

# function
caphi = [[], []] # caphi[0] the first caphi with parameter1
sigmaR = [[], []] # same above
sigmaR[0] = np.array(reader.func[0]['x'])
sigmaR[1] = np.array(reader.func[1]['x'])
caphi[0] = sumCap(reader, 'y', [0, 1, 2])
caphi[1] = sumCap(reader, 'y', [3, 4, 5])
# interpolate to obtain more caphi data
nSigmaR = 400 # new sigmaR Mesh
caphiInter = [[], []]
caphiInter[0] = interp1d(sigmaR[0], caphi[0], kind='linear')
caphiInter[1] = interp1d(sigmaR[1], caphi[1], kind='linear')
sigmaR[0] = np.linspace(sigmaR[0][0], sigmaR[0][-1], nSigmaR)
sigmaR[1] = np.linspace(sigmaR[1][0], sigmaR[1][-1], nSigmaR)
caphi[0] = caphiInter[0](sigmaR[0])
caphi[1] = caphiInter[1](sigmaR[1])

# plot the original caphi
#plt.figure(0)
#plt.title('Caphi')
#plt.plot(sigmaR[0], caphi[0], label = 'caphi0 \n sigmaI: %f' %(sigmaI[0]))
#plt.plot(sigmaR[1], caphi[1], label = 'caphi1 \n sigmaI: %f' %(sigmaI[1]))
#plt.show()

def cutres(omega, wOmega, rMesh, sigmaR):
    # cut the rMesh to the same range with caphi
    idx = np.all([(sigmaR[0] < omega), (omega < sigmaR[-1])], axis = 0)
    return omega[idx], wOmega[idx], rMesh[idx]

def plotRCheck(invF, method, sigmaR, caphi, para):
    # para should at least include omega and wOmeaga
    xMesh, rMesh = method(sigmaR, caphi, *para)
    cutwOmega = wOmega[-len(xMesh):]
    xMeshnew, cutwOmeganew, rMeshnew = cutres(xMesh, cutwOmega, rMesh, sigmaR)
    fig, ax = plt.subplots(2,1)
    ax[1].plot(xMeshnew * const.HBarC, rMeshnew / const.HBarC, label = 'Response function')
    caphiCheck = invF.kenel(sigmaR, xMesh, cutwOmega).dot(rMesh)
    fig.subplots_adjust(hspace = 0)
    ax[0].plot(sigmaR * const.HBarC, caphiCheck / const.HBarC / const.HBarC, label = 'Recovered')
    ax[0].plot(sigmaR * const.HBarC, caphi / const.HBarC / const.HBarC, label = r'Original $\Phi$')
    ax[1].set_xlabel(r'(a) $\sigma_R$ / MeV; (b) $\omega$ / MeV')
    ax[0].set_ylabel(r'$\Phi$ / MeV$^{-2}$')
    ax[1].set_ylabel(r'$R$ / MeV$^{-1}$')
    ax[0].text(175, 2000 / const.HBarC / const.HBarC, '(a)', fontsize = 21)
    ax[1].text(175, 0.01, '(b)', fontsize = 21)
    ax[1].grid()
    ax[0].grid()
    ax[0].legend()
    ax[0].legend()
    ax[1].legend()
    plt.show()

nOmega = 4 * nSigmaR # set the output omega mesh for the response function
#omegaMin = const.ETH
dsR = (sigmaR[0][-1] - sigmaR[0][0]) / nSigmaR
omegaMin = sigmaR[0][0] - 50 * dsR
omegaMax = sigmaR[0][-1] + 50 * dsR
omega = np.linspace(omegaMin, omegaMax, nOmega)
wOmega = np.ones(nOmega) * (omegaMax - omegaMin) / nOmega

## fig1: normal eq
invF = ninv(sigmaI[0])
para = [omega, wOmega]
plotRCheck(invF, invF.normalEq, sigmaR[0], caphi[0], para)
