import binCOMMON # so that we can import lib in src
import matplotlib.pyplot as plt
import src.tool.inout as ios # read csv from input.csv
import constant as const
import numpy as np
import src.tool.plotTool as ptt # plot tool
import pandas as pd

fileName = '../data/withOrNo3p0/withOrNo3p0.csv'

func = ios.readFunc(fileName, 'r')

plt.figure(0)

def myLabel(i):
    if i == 0:
        return 'Chiral (No 3p0) '
    elif i == 1:
        return 'Chiral (with 3p0)'

for i in range(len(func)):
    aFunc = func[i]
    # convert omega to E_np
    if aFunc['poType'][0] == 'chiral' or aFunc['funcName'][0] == 'PionNLO':
        aFunc['x'] = np.array(aFunc['x']) - 2.22 / const.HBarC
    elif aFunc['funcName'][0] == 'PionLO':
        aFunc['x'] = np.array(aFunc['x']) - 1.70 / const.HBarC
    ptt.plotAFunc(aFunc, myLabel(i))
    ptt.printInfo(aFunc, i)

# Data from paper
loRPaper = pd.read_csv('../data/paperResult.csv', header = None).to_numpy() # read data from paper
plt.scatter(loRPaper[:, 0], loRPaper[:, 1], label = 'Data from paper', color = 'black') # print out the leading order

plt.xlabel(r'$E_{np} / $MeV')
plt.ylabel(r'R$(\omega) / ($MeV$^{-1})$')
plt.legend()
plt.grid()
plt.show()
