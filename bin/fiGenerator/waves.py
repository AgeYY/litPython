# plot LO chiral SD Waves and LO pionless wave in one figure
import binCOMMON # so that we can import lib in src
import constant as const
import numpy as np
from potChiral import potChiral as potChi
import myMath
from src.core.chiral.SDWave import SDWave
import matplotlib.pyplot as plt
from waveFunc import WaveFunc as wfunc
import plotTool as pltool
from potential import Pot

########## Pras ##########
mambda = 600 / const.HBarC
nGau = 400
aGau, bGau = 0, 3 * mambda
para = []
chs = np.array([1, 0, 1])
c0Method = 'scattering length'
########## Pionless ##########
pot = Pot(mambda, const.A0, c0Method) # potential
waveFunc = wfunc(mambda, nGau, bGaur = 100 / 3) # init
waveFunc.setPot(pot)
(xMesh, wMesh, xrMesh, wrMesh) = waveFunc.osXW() # use os to read data
size = waveFunc.osGau()[0]
philocd = waveFunc.osPhilocd()
########## Chiral ##########
# Method in potChi
potC = potChi(mambda, xMesh, wMesh, xrMesh, wrMesh, para)
# Method in potChiral, p, q are setted as the same mesh
potC.potChiral(chs) # Generate potential mesh
sdWave = SDWave(potC.v00MeshR, potC.v01MeshR, potC.v10MeshR, potC.v11MeshR, potC.rMesh, potC.wrMesh)
(sWave, dWave) = sdWave.sdgSolver(atol = 1e-3) # Calculate S- and D- wave function
########## plot ##########
plt.figure(0)
plt.plot(xrMesh, xrMesh * philocd, label = "Pionless S wave")
plt.plot(xrMesh, sWave, label = "Chiral S wave")
plt.plot(xrMesh, dWave, label = "Chiral D wave")
plt.xlabel(r'$r$ / fm')
plt.ylabel(r'$\phi$ / fm$^{-1/2}$')
plt.legend()
plt.grid()
plt.show()
