# plot alpha in different channels

import binCOMMON
import pandas as pd
import constant as const
import os
import csv
import numpy as np
import matplotlib.pyplot as plt

#toSymbol[l]

def toKey(chs):
    l = [int(s) for s in chs.split() if s.isdigit()]
    return tuple(l)

def dic2List(dic):
    keys, item = [], []
    for key in dic:
        keys.append(key)
        item.append(dic[key])
    return keys, item

path = '../data/errorEstimate/channelAlpha'
data = open(path, 'r')
data_csv = csv.reader(data, delimiter=':')
flag = 1 # 1 means channel, -1 means alpha value
alphaDic = {}
for row in data_csv:
    if flag == 1:
        chs = toKey(row[-1]) # to tuple
    else:
        alphaDic[chs] = float(row[-1]) # to number
    flag = flag * -1

keys, item = dic2List(alphaDic)
fig, ax = plt.subplots()
ax.stem(item, use_line_collection = True)
ax.set_xlabel('Channels')
ax.set_ylabel(r'$\alpha_{\gamma}$')
plt.show()
