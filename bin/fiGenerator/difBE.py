import binCOMMON # so that we can import lib in src
import matplotlib.pyplot as plt
import src.tool.inout as ios # read csv from input.csv
import constant as const
import numpy as np
import src.tool.plotTool as ptt # plot tool
import pandas as pd

fileName = '../data/difBE/res.csv'

func = ios.readFunc(fileName, 'r')

plt.figure(0)

def myLabel(i, be):
    beStr = str(round(-be, 3))
    beStr = 'BE = ' + beStr
    if i == 0:
        return 'LO, ' + beStr
    elif i == 1:
        return 'NLO, ' + beStr
    elif i == 2:
        return 'LO, ' + beStr
    elif i == 3:
        return 'NLO, ' + beStr
    elif i == 4:
        return 'LO, ' + beStr
    elif i == 5:
        return 'NLO, ' + beStr
    elif i == 6:
        return 'LO, ' + beStr
    elif i == 7:
        return 'NLO, ' + beStr

for i in range(len(func)):
    aFunc = func[i]
    # convert omega to E_np
    if aFunc['funcName'][0] == 'PionNLO':
        aFunc['x'] = np.array(aFunc['x']) + aFunc['enlo'][0]
        be = aFunc['enlo'][0] * const.HBarC
    elif aFunc['poType'][0] == 'chiral' or aFunc['funcName'][0] == 'PionLO':
        aFunc['x'] = np.array(aFunc['x']) + aFunc['elo'][0]
        be = aFunc['elo'][0] * const.HBarC
    ptt.plotAFunc(aFunc, myLabel(i, be), lStylePLO = '-')

# Data from paper
loRPaper = pd.read_csv('../data/paperResult.csv', header = None).to_numpy() # read data from paper
plt.scatter(loRPaper[:, 0], loRPaper[:, 1], label = 'Data from paper', color = 'black') # print out the leading order

plt.title(r'$R(E_{np})$' + ' under the Pionless interaction with different binding energy.\n' + r'$q^2 = 4.8, \lambda = 400$' + ' MeV')
plt.xlabel(r'$E_{np} / $MeV')
plt.ylabel(r'R$(E_{np}) / ($MeV$^{-1})$')
plt.legend()
plt.grid()
plt.show()
