# Comparing the result of 1. f_L to R_L by integration 2. LIT inversion from Efros1993

import matplotlib.pyplot as plt
import binCOMMON # so that we can import lib in src
import numpy as np
import pandas as pd
import src.tool.inout as ios # read csv from input.csv
import src.tool.plotTool as ptt # plot tool
import src.core.general.constant as const
import myMath

def readSebRl(path):
    # read sebastian's .dat and output E_np and R_L
    sentimentlist = ios.readWholeDat(path)
    df_train=pd.DataFrame(sentimentlist,columns=['e_np','r_l_impulse','r_l_full']).to_numpy()
    enp = df_train[8:, 0]
    rl = df_train[8:, 2]
    enpNum = np.zeros(len(enp))
    rlNum = np.zeros(len(enp))
    for i in range(len(enp)):
        enpNum[i] = float(enp[i])
        rlNum[i] = float(rl[i]) / const.HBarC
        i = i + 1
    return (enpNum, rlNum)

path4p8 = '../data/sebRL/R_L-4p8-4-48-32.dat'
path9 = '../data/sebRL/R_L-9p0-4-48-32.dat'
(enpNum4p8, rlNum4p8) = readSebRl(path4p8)
(enpNum9, rlNum9) = readSebRl(path9)

fileName = 'q4p8And9/varQChar.csv'
func = ios.readFunc('../data/' + fileName, 'r')

plt.figure(0)
plt.plot(enpNum4p8, rlNum4p8, '-o', label = 'From f_L: q^2 = 4.8')
plt.plot(enpNum9, rlNum9, '-*', label = 'From f_L: q^2 = 9')

def aFunclabel(aFunc):
    label = 'chiral: q^2 = ' + str(round(aFunc['qChar'][0]**2, 2))
    return label

for i in range(len(func)):
    aFunc = func[i]
    # convert omega to E_np
    if aFunc['poType'][0] == 'chiral' or aFunc['funcName'][0] == 'PionNLO':
        aFunc['x'] = np.array(aFunc['x']) - 2.22 / const.HBarC
    elif aFunc['funcName'][0] == 'PionLO':
        aFunc['x'] = np.array(aFunc['x']) - 1.70 / const.HBarC

cache = {} # store the functions which we have queried
for i in range(len(func)):
    aFunc = func[i]
    key = aFunc['funcName'][0] + '\n' + r'$q^2 =$' + str(round(aFunc['qChar'][0]**2, 1))
    # convert omega to E_np
    if aFunc['funcName'][0] == 'PionLO': # we do not plot pionNLO
        continue
    else: # Plot the NLO and chiral with different cutoff
        try:
            ptt.map2y1x(func[cache[key]], func[i])
            #plt.plot(func[cache[key]]['x'] * const.HBarC, func[cache[key]]['y'] / const.HBarC, '--')
            ptt.plotABand(func[cache[key]], func[i], key)
        except KeyError:
            cache[key] = i
# Data from paper
loRPaper = pd.read_csv('../data/paperResult.csv', header = None).to_numpy() # read data from paper
plt.scatter(loRPaper[:, 0], loRPaper[:, 1], label = 'Data from paper', color = 'black') # print out the leading order

plt.xlabel(r'$E_{np} / $MeV')
plt.ylabel(r'R$(E_{np}) / ($MeV$^{-1})$')
plt.legend()
plt.grid()

## find the factor
#figure(1)

plt.show()
