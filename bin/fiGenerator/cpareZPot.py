from genPloter import ploter
import matplotlib.pyplot as plt

fileName = '../data/cpareZPot/res.csv'
pter = ploter(fileName)
labList = [r'Chiral $L > 0$', r'zPot $L > 0$', r'zPot $L = 0$', r'Chiral $L = 0$']
pter.shiftX()
fig, ax = pter.plot(labList)
ax.set(ylim = [-0.002, 0.023])
plt.show()
