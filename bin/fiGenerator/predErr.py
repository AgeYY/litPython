# predict the curve of different cu and cw
import binCOMMON
import pandas as pd
import constant as const
import os
import csv
import numpy as np
from genPloter import ploter
import matplotlib.pyplot as plt

fileName = '../data/predError/respred.csv'
pter = ploter(fileName)
labList = ['(1.5, 0)', '(1, 1.5)', '(1.5, 1.5)', '(1, 1)', '(1, 2)']
eShift = [-5.22 / const.HBarC] * 5
pter.shiftX(eShift)
fig, ax = pter.plot(labList)
ax.set(ylim = [-0.001, 0.047])
########## theo
def readAlpha(fileName):
    # tipical alpha values in one period. First col is alpha, second col is delta
    alpha = []
    afile = open(fileName, 'r')
    alpha_csv = csv.reader(afile)
    for row in alpha_csv:
        alpha.append(np.array(row, dtype = float))
    return np.array(alpha)

def sumc2(cu, cw, delta, alpha, beta):
    c = delta * cu + alpha * beta * cw
    return c.dot(c)

alpha = readAlpha('../data/predError/tipicalAlpha.csv')
beta = 0.218362
curvePara = [(1.5, 0), (1, 1.5), (1.5, 1.5), (1, 1), (1, 2)]
c2 = []
for cp in curvePara:
    c2Temp = sumc2(cp[0], cp[1], alpha[:, 1], alpha[:, 0], beta)
    c2.append(c2Temp)
xchi = np.array(pter.func[3]['x']) * const.HBarC
ychi = np.array(pter.func[3]['y']) / const.HBarC
ratio = c2 / c2[3]
i = 0 # label index
for r in ratio:
    ax.plot(xchi, ychi * r, linestyle = '--', label = labList[i])
    i = i + 1
ax.legend()
plt.show()

########## fig 2: similar pion and chiral ##########

########## Chiral and Pion ##########
c2chi = sumc2(1, 1, alpha[:, 1], alpha[:, 0], beta)
c2pion = sumc2(1, 0, alpha[:, 1], alpha[:, 0], beta)
print('error between chi and pion %f' % (1 - c2chi / c2pion)) # 5%
