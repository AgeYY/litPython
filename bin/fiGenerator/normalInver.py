# compare the three different inversion methods: tranditional, Normal equation, and Gradient decent
import matplotlib.pyplot as plt
import binCOMMON # so that we can import lib in src
import numpy as np
import pandas as pd
import src.tool.inout as ios # read csv from input.csv
import src.tool.plotTool as ptt # plot tool
import src.core.general.constant as const
from genPloter import ploter
import litInversion as litinv
from scipy.interpolate import interp1d
import myMath as mymath
from src.core.general.novelInv import NInv as ninv
import invNetWork as invnn

path = '../data/normalInversion/caphi.csv'
reader = ploter(path) # read functions in the file

def sumCap(reader, key, idx):
    # Sum over the first three caphi. key = 'x', or 'y'
    out = np.array(reader.func[idx[0]][key])
    out = out + np.array(reader.func[idx[1]][key])
    out = out + np.array(reader.func[idx[2]][key])
    return out

# Parameter
sigmaI = [[], []] 
mambda = [[], []] 
sigmaI[0] = reader.func[0]['sigmaIMeVChi'][0] / const.HBarC
sigmaI[1] = reader.func[-1]['sigmaIMeVChi'][0] / const.HBarC
mambda[0] = reader.func[0]['mambdaMeV'][0] / const.HBarC
mambda[1] = reader.func[-1]['mambdaMeV'][0] / const.HBarC

# function
caphi = [[], []] # caphi[0] the first caphi with parameter1
sigmaR = [[], []] # same above
sigmaR[0] = np.array(reader.func[0]['x'])
sigmaR[1] = np.array(reader.func[1]['x'])
caphi[0] = sumCap(reader, 'y', [0, 1, 2])
caphi[1] = sumCap(reader, 'y', [3, 4, 5])
# interpolate to obtain more caphi data
nSigmaR = 400 # new sigmaR Mesh
caphiInter = [[], []]
caphiInter[0] = interp1d(sigmaR[0], caphi[0], kind='linear')
caphiInter[1] = interp1d(sigmaR[1], caphi[1], kind='linear')
sigmaR[0] = np.linspace(sigmaR[0][0], sigmaR[0][-1], nSigmaR)
sigmaR[1] = np.linspace(sigmaR[1][0], sigmaR[1][-1], nSigmaR)
caphi[0] = caphiInter[0](sigmaR[0])
caphi[1] = caphiInter[1](sigmaR[1])

# plot the original caphi
#plt.figure(0)
#plt.title('Caphi')
#plt.plot(sigmaR[0], caphi[0], label = 'caphi0 \n sigmaI: %f' %(sigmaI[0]))
#plt.plot(sigmaR[1], caphi[1], label = 'caphi1 \n sigmaI: %f' %(sigmaI[1]))
#plt.show()

nOmega = 4 * nSigmaR # set the output omega mesh for the response function
#omegaMin = const.ETH
dsR = (sigmaR[0][-1] - sigmaR[0][0]) / nSigmaR
omegaMin = sigmaR[0][0] - 50 * dsR
omegaMax = sigmaR[0][-1] + 50 * dsR
omega = np.linspace(omegaMin, omegaMax, nOmega)
wOmega = np.ones(nOmega) * (omegaMax - omegaMin) / nOmega

def cutres(omega, wOmega, rMesh, sigmaR):
    # cut the rMesh to the same range with caphi
    idx = np.all([(sigmaR[0] < omega), (omega < sigmaR[-1])], axis = 0)
    return omega[idx], wOmega[idx], rMesh[idx]

def plotRCheck(invF, method, sigmaR, caphi, para):
    # para should at least include omega and wOmeaga
    xMesh, rMesh = method(sigmaR, caphi, *para)
    cutwOmega = wOmega[-len(xMesh):]
    xMeshnew, cutwOmeganew, rMeshnew = cutres(xMesh, cutwOmega, rMesh, sigmaR)
    fig, ax = plt.subplots(2,1)
    ax[1].plot(xMeshnew * const.HBarC, rMeshnew / const.HBarC, label = 'Response function')
    caphiCheck = invF.kenel(sigmaR, xMesh, cutwOmega).dot(rMesh)
    fig.subplots_adjust(hspace = 0)
    ax[0].plot(sigmaR * const.HBarC, caphiCheck / const.HBarC / const.HBarC, label = 'Recovered')
    ax[0].plot(sigmaR * const.HBarC, caphi / const.HBarC / const.HBarC, label = r'Original $\Phi$')
    ax[1].set_xlabel(r'$\omega$ / MeV')
    ax[0].set_ylabel(r'$\Phi$ / MeV$^{-2}$')
    ax[1].set_ylabel(r'$R$ / MeV$^{-1}$')
    ax[0].text(175, 2000 / const.HBarC / const.HBarC, '(a)', fontsize = 21)
    ax[1].text(175, 0.01, '(b)', fontsize = 21)

    secax = ax[0].secondary_xaxis('top')
    secax.set_xlabel(r'$\sigma_R$ / MeV')

    for axn in ax:
        axn.grid()
        axn.legend()
    plt.show()

def calculate(invF, method, sigmaR, caphi, para):
    # para should at least include omega and wOmeaga
    xMesh, rMesh = method(sigmaR, caphi, *para)
    cutwOmega = wOmega[-len(xMesh):]
    xMeshnew, cutwOmeganew, rMeshnew = cutres(xMesh, cutwOmega, rMesh, sigmaR)
    caphiCheck = invF.kenel(sigmaR, xMesh, cutwOmega).dot(rMesh)
    return xMeshnew* const.HBarC, rMeshnew / const.HBarC, sigmaR * const.HBarC, caphiCheck / const.HBarC / const.HBarC

def fourPlot(xMeshnew0, rMeshnew0, caphiCheck0, sigmaR0, xMeshnew1, rMeshnew1, sigmaR1, caphiCheck1, caphi0, caphi1):
    fig1, ax1 = plt.subplots(2, 2)
    fig1.subplots_adjust(hspace = 0, wspace = 0)
    ax1[1, 0].plot(xMeshnew0, rMeshnew0, label = 'Traditional \n method')
    ax1[0, 0].plot(sigmaR0, caphiCheck0, label = 'Recovered')
    ax1[0, 0].plot(sigmaR0, caphi0/ const.HBarC / const.HBarC, label = 'Original')
    ax1[1, 1].plot(xMeshnew1, rMeshnew1, label = 'New Method')
    ax1[0, 1].plot(sigmaR1, caphiCheck1, label = 'Recovered')
    ax1[0, 1].plot(sigmaR1, caphi1 / const.HBarC / const.HBarC, label = 'Original')

    ax1[1, 1].set_yticklabels([])
    ax1[1,0].set_ylim( -0.001, 0.019 )
    ax1[1,1].set_ylim( ax1[1, 0].get_ylim() )
    ax1[0, 1].set_yticklabels([])
    ax1[0,0].set_ylim( -0.01, 0.12 )
    ax1[0,1].set_ylim( ax1[0, 0].get_ylim() )

    ax1[0, 0].tick_params(labeltop = True, top = True)
    secax = ax1[0, 0].secondary_xaxis('top')
    secax.set_xlabel(r'$\sigma_R$ / MeV')
    ax1[0, 1].tick_params(labeltop = True, top = True)
    secax = ax1[0, 1].secondary_xaxis('top')
    secax.set_xlabel(r'$\sigma_R$ / MeV')
    ax1[1, 0].set_xlabel(r'$\omega$ / MeV')
    ax1[1, 1].set_xlabel(r'$\omega$ / MeV')

    ax1[1, 0].set_ylabel(r'$R(\omega)$ / MeV$^{-1}$')
    ax1[0, 0].set_ylabel(r'$\Phi(\sigma_R)$ / MeV$^{-2}$')

    for ax in ax1.flatten():
        ax.legend()
        ax.grid()
    return fig1, ax1

## fig1: normal eq
invF = ninv(sigmaI[0])
para = [omega, wOmega]
plotRCheck(invF, invF.normalEq, sigmaR[0], caphi[0], para)

## fig2: compare trad and ai
###### ai
invF = ninv(sigmaI[0])
paraLoss = [0]
invF.setLoss(sigmaR[0], omega, wOmega, paraLoss = paraLoss)
para = [omega, wOmega, 0.0001, 200]
xMeshnewai0, rMeshnewai0, sigmaRai0, caphiCheckai0 = calculate(invF, invF.neuron, sigmaR[0], caphi[0], para)
invF = ninv(sigmaI[1])
invF.setLoss(sigmaR[1], omega, wOmega, paraLoss = paraLoss)
para = [omega, wOmega, 0.0001, 200]
xMeshnewai1, rMeshnewai1, sigmaRai1, caphiCheckai1 = calculate(invF, invF.neuron, sigmaR[1], caphi[1], para)
#
#fourPlot(xMeshnewai0, rMeshnewai0, caphiCheckai0, sigmaRai0, xMeshnewai1, rMeshnewai1, sigmaRai1, caphiCheckai1, caphi[0], caphi[1])

##### trad
nOmega = 4 * nSigmaR # set the output omega mesh for the response function
dsR = (sigmaR[0][-1] - sigmaR[0][0]) / nSigmaR
omegaMin = sigmaR[0][0]
omegaMax = sigmaR[0][-1]
omega = np.linspace(omegaMin, omegaMax, nOmega)
wOmega = np.ones(nOmega) * (omegaMax - omegaMin) / nOmega

invF = ninv(sigmaI[0])
para = [omega, wOmega, 1, 8]
xMeshnew0, rMeshnew0, sigmaR0, caphiCheck0 = calculate(invF, invF.trad, sigmaR[0], caphi[0], para)
invF = ninv(sigmaI[1])
para = [omega, wOmega, 1, 20]
xMeshnew1, rMeshnew1, sigmaR1, caphiCheck1 = calculate(invF, invF.trad, sigmaR[1], caphi[1], para)

#fourPlot(xMeshnew0, rMeshnew0, caphiCheck0, sigmaR0, xMeshnew1, rMeshnew1, sigmaR1, caphiCheck1, caphi[0], caphi[1])
fourPlot(xMeshnew0, rMeshnew0, caphiCheck0, sigmaR0, xMeshnewai0, rMeshnewai0, sigmaRai0, caphiCheckai0, caphi[0], caphi[0])
plt.show()

#para = [omega, wOmega, 0.00001, 10000, invF.l2reg, invF.l2dreg, 0.00001]
#plotRCheck(invF, invF.GDes, sigmaR[0], caphi[0], para)
#paraLoss = [0]
#invF.setLoss(sigmaR[0], omega, wOmega, paraLoss = paraLoss)
#para = [omega, wOmega, 0.0001, 2000]
#plotRCheck(invF, invF.neuron, sigmaR[0], caphi[0], para)
#paraLoss = [100]
#invF.setLoss(sigmaR[0], omega, wOmega, Loss = invnn.PenHLoss,paraLoss = paraLoss)
#para = [omega, wOmega, 0.00001, 10000]
#plotRCheck(invF, invF.neuron, sigmaR[0], caphi[0], para)
#invF.printLoss()

#invF = ninv(sigmaI[1])
#para = [omega, wOmega]
#plotRCheck(invF, invF.normalEq, sigmaR[1], caphi[1], para)
#para = [omega, wOmega, 1, 20]
#plotRCheck(invF, invF.trad, sigmaR[1], caphi[1], para)
#para = [omega, wOmega, 0.00001, 10000, invF.l2reg, invF.l2dreg, 0.00001]
#plotRCheck(invF, invF.GDes, sigmaR[1], caphi[1], para)
#paraLoss = [0]
#invF.setLoss(sigmaR[0], omega, wOmega, paraLoss = paraLoss)
#para = [omega, wOmega, 0.0001, 2000]
#plotRCheck(invF, invF.neuron, sigmaR[1], caphi[1], para)
#paraLoss = [100]
#invF.setLoss(sigmaR[1], omega, wOmega, Loss = invnn.PenHLoss,paraLoss = paraLoss)
#lr, nSteps = 0.0001, 2000
#para = [omega, wOmega, lr, nSteps]
#plotRCheck(invF, invF.neuron, sigmaR[1], caphi[1], para)
#invF.printLoss()
