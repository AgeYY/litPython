# Comparing the result of 1. f_L to R_L by integration 2. LIT inversion from Efros1993

import matplotlib.pyplot as plt
import binCOMMON # so that we can import lib in src
import numpy as np
import pandas as pd
import src.tool.inout as ios # read csv from input.csv
import src.tool.plotTool as ptt # plot tool
import src.core.general.constant as const
import myMath

fileName = 'q4p8And9/varQChar.csv'
func = ios.readFunc('../data/' + fileName, 'r')

plt.figure(0)

def aFunclabel(aFunc):
    label = 'chiral: q^2 = ' + str(round(aFunc['qChar'][0]**2, 2))
    return label

for i in range(len(func)):
    aFunc = func[i]
    # convert omega to E_np
    if aFunc['poType'][0] == 'chiral' or aFunc['funcName'][0] == 'PionNLO':
        aFunc['x'] = np.array(aFunc['x']) - 2.22 / const.HBarC
    elif aFunc['funcName'][0] == 'PionLO':
        aFunc['x'] = np.array(aFunc['x']) - 1.70 / const.HBarC

cache = {} # store the functions which we have queried
for i in range(len(func)):
    aFunc = func[i]
    key = aFunc['funcName'][0] + '\n' + r'$q^2 =$' + str(round(aFunc['qChar'][0]**2, 1))
    # convert omega to E_np
    if (aFunc['poType'][0] == 'chiral' or aFunc['funcName'][0] == 'PionNLO') and abs(aFunc['qChar'][0] - 2.19) < 0.1:
        try:
            ptt.map2y1x(func[cache[key]], func[i])
            #plt.plot(func[cache[key]]['x'] * const.HBarC, func[cache[key]]['y'] / const.HBarC, '--')
            ptt.plotABand(func[cache[key]], func[i], key)
        except KeyError:
            cache[key] = i
# Data from paper
loRPaper = pd.read_csv('../data/paperResult.csv', header = None).to_numpy() # read data from paper
plt.scatter(loRPaper[:, 0], loRPaper[:, 1], label = 'Data from paper', color = 'black') # print out the leading order

plt.xlabel(r'$E_{np} / $MeV')
plt.ylabel(r'R$(E_{np}) / ($MeV$^{-1})$')
plt.legend()
plt.grid()

## find the factor
#figure(1)

plt.show()
