import binCOMMON # so that we can import lib in src
import matplotlib.pyplot as plt
import src.tool.inout as ios # read csv from input.csv
import constant as const
import numpy as np
import src.tool.plotTool as ptt # plot tool

fileName = 'varQChar.csv'
func = ios.readFunc('../data/' + fileName, 'r')

plt.figure(0)

def firstLabel(iamGoPlot):
    # if this is the first time of ploting pion,  assign legend = 'pion'. if ... the first time of ploting chiral, assign legend = 'chiral'
    global lbPion
    global lbChi
    legend = None
    if iamGoPlot == 'pion':
        if lbChi:
            legend = 'pionless'
            lbChi = False
    elif iamGoPlot == 'chiral':
        if lbPion:
            legend = 'chiral'
            lbPion = False
    return legend

def lab(aFunc):
    return r'$q^2 = $' + str(format(aFunc['qChar'][0] ** 2, '.1f')) + r' fm$^{-2}$'

lbChi = True
lbPion = True # flag so you know if this is the first time for ploting pion
cache = {} # store the functions which we have queried
for i in range(len(func)):
    aFunc = func[i]
    key = aFunc['funcName'][0] + str(aFunc['qChar'][0])[:3]
    if aFunc['poType'][0] == 'pion':
        if aFunc['funcName'][0] == 'PionNLO': # we do not plot pionNLO
            continue
        else: # Plot the LO with different cutoff
            try:
                ptt.plotABand(func[cache[key]], func[i], key)
            except KeyError:
                cache[key] = i
    elif aFunc['poType'][0] == 'chiral':
        if aFunc['mambdaMeV'][0] > 401: # the possible value are 400 and 1600, we don't plot 1600
            continue
        else:
            legend = lab(aFunc)
            ptt.plotAFunc(aFunc, legend)

plt.xlabel(r'$\omega / $MeV')
plt.ylabel(r'R$(\omega) / ($MeV$^{-1})$')
plt.legend()
plt.grid()
plt.savefig('../fig/varChar.pdf',  bbox_inches="tight")
plt.show()
