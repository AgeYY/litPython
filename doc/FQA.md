# Please contact *zeyuanye@hotmail.com* for all the *.pdf* files

# Clone a gitlab project though the address.

This is the address

>https://gitlab.com/AgeYY/mixcoding

The correspond ssh address is

>git@gitlab.com:AgeYY/mixcoding.git

You don't need to input the pwd if you clone the repo with ssh address. I surggest you to create a README.md file in the new repo since this would create a new branch.

# New a branch in git

>git checkout -b newBranchName

# Run shell script in Python
ref: [os.system](https://stackabuse.com/executing-shell-commands-with-python/)

```Python
import os

os.system("shell commands here")
```

# learn mark down

[Cheat sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

# Convert Python string to c bytes

There are two methods:

1. ```Python
b_string1 = string1.encode('utf-8')
```
2. ```Python
b"string here"
```

[ref](https://stackoverflow.com/questions/27127413/converting-python-string-object-to-c-char-using-ctypes)

# Can I use '==' in Python

'==' means two variables have the same value, while 'is' means the two variables point to the same object.

The ans is no, you'd better use abs() < error

# What is potval

```Fortran
! %%%%%%
! Out
! %%%%%%
! potval is the value of partial-wave potentials
! Storage of potval:
!   2-by-2 matrix
!   For uncoupled channels, e.g., 1S0(LSJ = 000) or 3P0(LSJ = 110)
!     potval(1, 1) = <L, p1 | V | L, p2 > and (1, 2), (2, 1), and (2, 2) are
!     zero
!   For coupled channels, e.g., 3S1 - 3D1 (LSJ = 011)
!     potval(1, 1) = < L, p1   | V | L, p2 >
!     potval(1, 2) = < L, p1   | V | L+2, p2 >
!     potval(2, 1) = < L+2, p1 | V | L, p2 >
!     potval(2, 2) = < L+2, p1 | V | L+2, p2 >
!
```

Check for *chengdu_V0.14.f90* in *nnscat* for more detail.

# Numpy tutorial

[Quickstart tutorial](https://docs.scipy.org/doc/numpy/user/quickstart.html)
