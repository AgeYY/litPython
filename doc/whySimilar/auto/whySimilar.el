(TeX-add-style-hook
 "whySimilar"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "bbm"
    "geometry"
    "graphicx"
    "amsmath"
    "float"
    "listings"
    "indentfirst"
    "braket"
    "authblk"
    "hyperref"))
 :latex)

