import common
import constant as const
import numpy as np
import sys

class harmonic1D:
    # Hbar = 1
    mass = 0
    omega = 1

    def __init__(self, mass, omega):
        self.mass = mass
        self.omega = omega
        self.__update()

    def __update(self):
        pass

    def osPara(self):
        return (self.mass, self.omega)

    def energy(self, n):
        return (n + 1 / 2) * self.omega

    def groundWave(self, x):
        return (self.mass * self.omega / np.pi)**(1/4) * np.exp(- self.mass * self.omega / 2 * x * x)
