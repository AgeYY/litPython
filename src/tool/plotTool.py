from matplotlib import pyplot as plt
import numpy as np
import src.core.general.constant as const
import myMath

def simplePlot(xMesh, yMesh, xlab = ' ', ylab = ' ', lab = ' ', fig = 0, titleStr = ' '):
    plt.figure(fig)
    plt.title(titleStr)
    plt.plot(xMesh, yMesh, label = lab)
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    plt.legend()

def plotAFunc(aFunc, legend, lStylePLO = '-.', lStylePNLO = '--', lStyleC = '-', ax = None):
# Plot figures with only lines
    if aFunc['poType'][0] == 'chiral':
        aFunc['x'] = np.array(aFunc['x'])
        lineStyle = lStyleC
    elif aFunc['funcName'][0] == 'PionNLO':
        lineStyle = lStylePNLO
    elif aFunc['funcName'][0] == 'PionLO':
        lineStyle = lStylePLO
    else:
        lineStyle = '-'
    if ax is None:
        plt.plot(np.array(aFunc['x']) * const.HBarC, np.array(aFunc['y']) / const.HBarC, linestyle = lineStyle, label = legend)
    else:
        ax.plot(np.array(aFunc['x']) * const.HBarC, np.array(aFunc['y']) / const.HBarC, linestyle = lineStyle, label = legend)

def printInfo(aFunc, i, youWant = None):
    # print the infomation of function i
    print('function '+ str(i) + ':')
    if youWant is None:
        youWant = {'sigmaIMeVChi', 'sigmaIMeVPion', 'lMin', 'lMax', 'qChar', 'mambdaMeV', 'nGauChi', 'nGauPion', 'funcName', 'sigmaIMeV', 'elo', 'enlo'}
    for key, value in aFunc.items():
        if key in youWant:
            print(key + ':', value[0])
    print('\n')

def plotABand(func1, func2, legend = None): # plot response function which have same key but different cutoff, i.e. same poType and qChar
# Plot figures with error band
    plt.fill_between(np.array(func1['x']) * const.HBarC, np.array(func1['y']) / const.HBarC, np.array(func2['y']) / const.HBarC, label = legend, alpha = 0.7)

def map2y1x(func1, func2):
    # map two function to one x mesh
    x1 = func1['x']
    y1 = func1['y']
    x2 = func2['x']
    y2 = func2['y']
    xMin, xMax = myMath.findIsectInv(x1, x2)# find the intersection
    x = np.linspace(xMin, xMax, 80)
    func1['y'], func2['y'] = myMath.ys2x(x1, y1, x2, y2, x)
    func1['x'] = x
    func2['x'] = x
