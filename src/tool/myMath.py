import common
from scipy import integrate as intg
import numpy as np
from scipy.interpolate import interp1d
from scipy import special


def normCal(waveFunc, aGau, bGau):
    def waveSquare(x, waveFunc):
        waveVal = waveFunc(x) * x
        return waveVal * np.conj(waveVal)
    n2 = intg.quad(waveSquare, aGau, bGau, args=(waveFunc))[0]
    return np.sqrt(n2)

def normCalD(waveMesh, xMesh, wMesh):
    return np.sqrt(np.sum(waveMesh * np.conj(waveMesh) * xMesh * xMesh * wMesh))

def fredholm1(gFunc, hFunc, xMesh, wMesh):
    '''
    1st kind of fredholm solver.
    Ref: https://wenku.baidu.com/view/90c78519a45177232e60a258.html
    In:
    gFunc(function(x, y)): Function in the integrate on the left side.
    gFunc(function(x)): Function in the right side
    xMesh: the required y values' x positions

    Out:
    yMesh: the required y values
    '''
    size = xMesh.size
    gMat = np.empty((size, size))
    hArr = np.empty(size)
    i = 0
    for x in xMesh:
        gMat[i, :] = gFunc(x, xMesh) * wMesh # different cols have different weights for they are the integral vars
        i += 1
    hArr = hFunc(xMesh)
    return np.linalg.pinv(gMat).dot(hArr)

def legGaussPoint(nGau, aGau, bGau, method = 'plain'): # Generate legGaussPoints.
    mean = (aGau + bGau) / 2
    diff = (bGau - aGau) / 2
    mesh = np.polynomial.legendre.leggauss(nGau)
    xMesh = mesh[0]
    wMesh = mesh[1]
    if(method == "plain"):
        xMesh = xMesh * diff + mean
        wMesh = wMesh * diff
    else: # You can add other methods
        pass
    return (xMesh, wMesh)

def fredholm1Func(gFunc, hFunc, nGau, aGau, bGau):
    '''
    1st kind of fredholm solver.
    Ref: https://wenku.baidu.com/view/90c78519a45177232e60a258.html
    In:
    gFunc(function(x, y)): Function in the integrate on the left side.
    hFunc(function(x)): Function in the right side
    nGau(int): number of mesh points
    aGau, bGau(double): integrate boundary

    Out:
    y(function(x)): solution
    '''
    (xMesh, wMesh) = legGaussPoint(nGau, aGau, bGau, "plain")
    yMesh = fredholm1(gFunc, hFunc, xMesh, wMesh)
    return interp1d(xMesh, yMesh, kind = 'linear')

def addIntShell(opt, xMesh, wMesh, position):
    '''
    Add the integral shell on the left or right
    IN:
    opt(size * size): operator matrix
    xMesh(size), wMesh(size): xMesh and its weight
    potision(str): "left" or "right". left means the shell add on the left, i.e. int{dx}(x x opt). right means int{dx}(opt x x). For 2D integral, int{dx1 dx2}(x1 x1 opt x2 x2) you must add on both sides.
    '''
    if(position == "right"):
        for i in np.arange(size): # add the integrate shell
            opt[i, :] = opt[i, :] * xMesh * xMesh * wMesh
    elif(position == "left"):
        for i in np.arange(size): # add the integrate shell
            opt[:, i] = opt[:, i] * xMesh * xMesh * wMesh

def fourier3DRadical(xMesh, wMesh, yMesh, rMesh, wrMesh, order = 0):
    '''
    Fourier transform of a 3D radical function. Note that phase (-i)^l (in the case of moementum to real), or i^l (in the case real to moementum) has been ignored.

    In:
    xMesh(array): x mesh from the original space
    wMesh(array): correspond weight
    yMesh(array): y mesh from the original space
    rMesh(array): x mesh from the target space
    wrMesh(array): correspond weight
    order(double): partial wave order

    Out:
    ryMesh(array): y mesh of the target space
    '''
    ryMesh = np.zeros(rMesh.size)
    for i in np.arange(rMesh.size):
        ryMesh[i] =np.sum(xMesh * xMesh * special.spherical_jn(order, (xMesh * rMesh[i]))  * yMesh * wMesh)
    return ryMesh * np.sqrt(2 / np.pi)

def fourier3DRadical2(xMesh, wxMesh, vMesh, rMesh, wrMesh, orderL = 0, orderR = 0):
    '''
    Fourier transform of a v(p, p'). Note the phase, (-i)^l and i^l' has been ignored
    input: same as above.
    orderL: partial wave order on the left handside
    orderR: partial wave order on the right handside
    '''
    xwxMesh = xMesh * wxMesh * xMesh
    handL = np.zeros((rMesh.size, xMesh.size))
    handR = np.zeros((rMesh.size, xMesh.size))
    for i in range(rMesh.size):
        handL[i, :] = xwxMesh * special.spherical_jn(orderL, (xMesh * rMesh[i]))
        handR[i, :] = xwxMesh * special.spherical_jn(orderR, (xMesh * rMesh[i]))
    vMeshR = handL.dot(vMesh).dot(handR.T)
    return vMeshR * 2 / np.pi

def ys2x(x1, y1, x2, y2, x):
    # map two function to one x mesh. xi, yi are the original function, x is the target x Mesh you want. x should be bounded by both x1 and x2
    f1 = interp1d(x1, y1)
    f2 = interp1d(x2, y2)
    return f1(x), f2(x)

def findIsectInv(x1, x2):
    # find the intersection of two intervals. The input should be np array
    xMin = max(min(x1), min(x2))
    xMax = min(max(x1), max(x2))
    return xMin, xMax

