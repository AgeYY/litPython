import common
from numpy.linalg import svd
import numpy as np

def dOpt(xMesh):
    # Remember to cut out two end points. If you are solving a second d equation, you need to cut the first two and the end two values. This is very important.
    size = xMesh.size
    DOpt = np.zeros((size, size))
    # End points use the simpliest algoeithm
    DOpt[0, 1] = 1 / (xMesh[1] - xMesh[0])
    DOpt[0, 0] = -1 / (xMesh[1] - xMesh[0])
    DOpt[size - 1, size - 1] = 1 / (xMesh[size - 1] - xMesh[size - 2])
    DOpt[size - 1, size - 2] = -1 / (xMesh[size - 1] - xMesh[size - 2])
    # middle points
    for i in np.arange(1, size - 1):
        for j in np.arange(size):
            if((i + 1) == j):
                DOpt[i, j] = 1
            elif((i - 1) == j):
                DOpt[i, j] = -1
        DOpt[i, :] /= (xMesh[i + 1] - xMesh[i - 1])
    return DOpt

def nullspace(A, atol=1e-13, rtol=0):
    A = np.atleast_2d(A)
    u, s, vh = svd(A)
    tol = max(atol, rtol * s[0])
    nnz = (s >= tol).sum()
    ns = vh[nnz:].conj().T
    return ns

def addBound(lhs, rhs, *args):
    '''
    Do several times of oneBound operation (see the discreption of oneBound below).
    *args(list[2]): several length-2 lists, e.g. [1, 2], [3, 4]. The first entry of every list is xPos, and the second one is yVal. xPos and yVal is defined in oneBound.
    '''
    def oneBound(lhs, rhs, xPos, yVal):
        '''
        Add a single with boundary condictions.
        In:
        lhs((size, size)): matrix on the left handside.
        rhs(size): array on the right hand side
        xPos(int): x positions of the boundaries. For example, xMesh = (1 fm, 2 fm, 3 fm). This mesh generates a lhs(xMesh, xMesh) and rhs(xMesh). If the wavefunction (i.e, the solution of lhs_inverse.dot(rhs)) has bound \phi(2 fm) = 0. Then the second row of lhs (because x[1] = 2 fm) would be replaced by (0, 1, 0), and the second entry of rhs would be replaced by yVal.
        yVal(complex or real): y values of the corresponds postions
        out:
        lhs((size, size)): left matrix with the two ends deleted and the boundary condition added
        rhs(size): right array with the two ends deleted and the boundary condition added
            '''
        con = np.zeros(rhs.size)
        con[xPos] = 1
        lhs[xPos, :] = con
        rhs[xPos] = yVal
        return (lhs, rhs)
    for pos in args:
        oneBound(lhs, rhs, pos[0], pos[1])
    return (lhs, rhs)
