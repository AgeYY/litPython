import src.core.chiral.resChi as resChi
import src.core.chiral.resChiNLO as resChiNLO
import numpy as np
import pandas as pd
import src.core.general.constant as const

def chiralSolver(mambda = 600 / const.HBarC, nGau = 130, aGau = 0, bGau = 9, aGauR = 0, bGauR = 50, qChar = np.sqrt(5), sigmaI = 5 / const.HBarC, sigmaRMeshMin = const.ETH, sigmaRMeshMax = 250 / const.HBarC, NsigmaR = 50, checkOpt = 'off', checkN = 20, omegaMax = 500 / const.HBarC, lMin = 0, lMax = 13, paraInv0 = 10, nMax0 = 10, base0 = 'base1', fitMethod0 = 'normalEq', paraInv2 = 17, nMax2 = 10, base2 = 'base1', fitMethod2 = 'normalEq', paraInvL = 10, nMaxL = 10, baseL = 'base2', fitMethodL = 'normalEq', order = 'LO'):
    para = []
    sigmaRMeshMin = const.ETH - sigmaI
    sigmaRMesh = np.linspace(sigmaRMeshMin, sigmaRMeshMax, NsigmaR) # See Efro1993
    omega = np.linspace(const.ETH, sigmaRMeshMax - sigmaI, 100) # unit = fm. xMesh for response function. We use a smaller range with sigma_R (ReviewEfros2008).
    if(order == 'NLO'):
        resSolver = resChiNLO.resChiNLO()
    else:
        resSolver = resChi.resChi()
    (caphi0, caphi2, caphiL) = resSolver.resCaphi(mambda, nGau, aGau, bGau, aGauR, bGauR, qChar, sigmaI, sigmaRMesh, lMin, lMax) # This would check the fitting status of L = 0
    (omegaLoc, response0) = resSolver.invCaphi(omega, caphi0, paraInv0, omegaMax, nMax0, base = base0, fitMethod = fitMethod0, checkOpt = checkOpt)
    (omegaLoc, response2) = resSolver.invCaphi(omega, caphi2, paraInv2, omegaMax, nMax2, base = base2, fitMethod = fitMethod2, checkOpt = checkOpt)
    (omegaLoc, responseL) = resSolver.invCaphi(omega, caphiL, paraInvL, omegaMax, nMaxL, base = baseL, fitMethod = fitMethodL, checkOpt = checkOpt)
    response = response0 + response2 + responseL
    return [omegaLoc, response, sigmaRMesh, caphi0, caphi2, caphiL]
