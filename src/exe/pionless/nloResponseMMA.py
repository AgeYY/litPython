# Inverse data from MMA
import common
import constant as const
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import litInversion as litinv
import plotTool as pltool

############ Input #############
nGau = 600 # Mesh points of the wave function
mambda = 600 / const.HBarC
sigmaI = 5 / const.HBarC
sigmaRMin, sigmaRMax = 10 / const.HBarC, 250 / const.HBarC
NsigmaR = 50
qChar = np.sqrt(5)
nMax0 = 7 # Max n value for the inversion of the l = 0
para0 = [const.EB, 7.5]
nMaxL = 10 # Max n value for the inversion of the l > 0
paraL = [const.EB, 10]
nlo0 = pd.read_csv('../../data/LITSigmaRPlot0.csv', header = None).to_numpy()
nloL = pd.read_csv('../../data/LITSigmaRPlot16.csv', header = None).to_numpy()
checkN = 20

########### Calculation #########
sigmaRMesh0 = nlo0[:, 0] / const.HBarC
phiCapL0 = nlo0[:, 1] * const.HBarC * const.HBarC
sigmaRMeshL = nloL[:, 0] / const.HBarC
phiCapLL = nloL[:, 1] * const.HBarC * const.HBarC # read data
sigmaRMeshDens = np.linspace(sigmaRMeshL.min(), sigmaRMeshL.max(), 300)

invF = litinv.inversion(sigmaI, mambda)
omegaMax = 500 / const.HBarC # This can be read off from the base function.
invF.setBase(litinv.base1, para0, omegaMax)
invF.fitData(sigmaRMesh0, phiCapL0, nMax0, method = 'smart')
invF.checkFit(sigmaRMesh0, phiCapL0, checkN)
(sigmaRMeshDens, rMesh) = invF.responseFunc(sigmaRMeshDens)
invF.setBase(litinv.base2, paraL, omegaMax)
invF.fitData(sigmaRMeshL, phiCapLL, nMaxL, method = 'smart')
invF.checkFit(sigmaRMeshL, phiCapLL, checkN)
(sigmaRMeshDens, rMesh1) = invF.responseFunc(sigmaRMeshDens)
rMesh += rMesh1
########### Figure ##########
loR = pd.read_csv('../../data/LITSigmaRLO.csv', header = None).to_numpy() # read data from leading order
loRPaper = pd.read_csv('../../data/paperResult.csv', header = None).to_numpy() # read data from leading order

pltool.simplePlot(loR[:, 0], loR[:, 1], lab = 'LO') # print out the leading order
plt.scatter(loRPaper[:, 0], loRPaper[:, 1], label = 'Data from paper', color = 'red') # print out the leading order
pltool.simplePlot(sigmaRMeshDens * const.HBarC, rMesh / const.HBarC, lab = 'NLO') # Convert to MeV and compare with the result in the paper
plt.title(r'R$_{tot}$($\omega$)' + "\n(All variables with unit MeV)")
plt.text(100, 0.01, r'$\Lambda = 600$', fontdict = {'size': 16})
plt.xlabel(r'$\omega / $MeV')
plt.ylabel(r'R$(\omega) / ($MeV$^{-1})$')
plt.legend()
plt.grid()
plt.savefig('../../fig/nlo.eps')
plt.show()
