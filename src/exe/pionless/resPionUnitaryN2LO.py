import common
import numpy as np
from waveFuncn2lo import WaveFuncn2lo as wfunc
import constant as const
import myMath
from matplotlib import pyplot as plt
import plotTool as pltool
from litSolvern2lo import LitSolvern2lo as lits
import matOpt
import scipy.linalg as linalg
from scipy import special
import litInversion as litinv
from unitaryPotn2lo import U2Pot

def pionUnitaryN2LO(nGau = 300, mambda = 600 / const.HBarC, sigmaI = 5 / const.HBarC, sigmaRMin = const.ETH, sigmaRMax = 250 / const.HBarC, NsigmaR = 50, qChar = np.sqrt(5), nMax = 10, para = [const.E0, 15], checkN = 20, checkOpt = 'off', fitMethod = 'normalEq', c0Method = 'scattering length', lMax = 10, lMin = 0, bGauR = 2000 / 3, base = 'base1', elo = const.E0, kappa0 = 1 / 20, potype = 'pion', kappa1 = 0.2):
    sigmaMin = const.ETH - sigmaI
    kappa0 = kappa0
    if kappa1 == 'None':
        kappa1 = 1 / const.A0 - kappa0
    sigmaRMin, sigmaRMax, NsigmaR = 0, 250 / const.HBarC, 100
    pot = U2Pot(mambda, const.A0, kappa0 = kappa0, kappa1 = kappa1) # potential
    waveFunc = wfunc(mambda, nGau, bGaur = bGauR) # init
    waveFunc.setPot(pot)
    waveFunc.loSolver()
    waveFunc.nloSolver()
    waveFunc.n2loSolver()
    e0, e1, e2 = waveFunc.findE0() # 0, 0, -1.41
    vlocdMesh = np.zeros((nGau, nGau))
    vnlocdMesh = np.zeros((nGau, nGau))
    vn2locdMesh = np.zeros((nGau, nGau))
    # generate potential
    for i in range(nGau):
        for j in range(nGau):
            vlocdMesh[i, j] = pot.potlo(waveFunc.xrMesh[i], waveFunc.xrMesh[j], 'cd')
            vnlocdMesh[i, j] = pot.potnlo(waveFunc.xrMesh[i], waveFunc.xrMesh[j], 'cd')
            vn2locdMesh[i, j] = pot.potn2lo(waveFunc.xrMesh[i], waveFunc.xrMesh[j], 'cd')
            
    litSolver = lits(mambda, nGau, waveFunc.aGau, 3 * mambda, 0, bGauR, waveFunc.xrMesh, waveFunc.wrMesh)
    litSolver.setLO(vlocdMesh, waveFunc.ulocdMesh, waveFunc.e0)
    litSolver.setNLO(vnlocdMesh, waveFunc.unlocdMesh, waveFunc.e1) # e1: NLO correction of energy
    litSolver.setN2LO(vn2locdMesh, waveFunc.un2locdMesh, waveFunc.e2)
    sigmaRMesh = np.linspace(sigmaRMin, sigmaRMax, NsigmaR)
    omega = np.linspace(const.ETH, sigmaRMax - sigmaI, 200) # pick the range of omega a bit smaller than sigmaR. See 3.3, ReviewEfros2008
    invF = litinv.inversion(sigmaI, mambda)
    omegaMax = 500 / const.HBarC # This can be read off from the base function.
    if base == 'base2':
        invF.setBase(litinv.base2, para, omegaMax)
    elif base == 'base3':
        invF.setBase(litinv.base3, para, omegaMax)
    else:
        invF.setBase(litinv.base1, para, omegaMax)

    def fitCheck(sigmaRMesh, phiCap, omega):
        invF.fitData(sigmaRMesh, phiCap, nMax, method = fitMethod)
        (omegaLoc, rMesh) = invF.responseFunc(omega) # leading order response function. responseFunc will cut some points in omega to omegaLoc
        if(checkOpt == 'on'):
            invF.checkFit(sigmaRMesh, phiCap, checkN)
        return (omegaLoc, rMesh)

    def solveRes(sublMin, sublMax, fit = 'normalEq'):
        phiCapLO, phiCapNLO, phiCapN2LO = litSolver.solverN2LO(qChar, sigmaI, sigmaRMesh, sublMin, sublMax) # L = 0
        (omegaLoc, rMesh) = fitCheck(sigmaRMesh, phiCapLO, omega)
        (omegaLoc, rMeshNLO) = fitCheck(sigmaRMesh, phiCapNLO, omega)
        (omegaLoc, rMeshN2LO) = fitCheck(sigmaRMesh, phiCapN2LO, omega)
        return (omegaLoc, rMesh, omegaLoc, rMeshNLO, omegaLoc, rMeshN2LO, sigmaRMesh, phiCapLO, phiCapNLO, phiCapN2LO)


    if(lMax >= 1 and lMin == 0):
        (xMesh, res0, xMeshNLO, resNLO0, xMeshN2LO, resN2LO0, sigmaRMesh, phiCapLO0, phiCapNLO0, phiCapN2LO0) = solveRes(0, 0, fitMethod)
        (xMesh, resL, xMeshNLO, resNLOL, xMeshN2LO, resN2LOL, sigmaRMesh, phiCapLOL, phiCapNLOL, phiCapN2LOL) = solveRes(1, lMax, fitMethod)
        res = resL + res0
        resNLO = resNLOL + resNLO0
        resN2LO = resN2LOL + resN2LO0
        return e0, xMesh, res, e1, xMeshNLO, resNLO, e2, xMeshN2LO, resN2LO, sigmaRMesh, phiCapLO0, phiCapLOL, sigmaRMesh, phiCapNLO0, phiCapNLOL, sigmaRMesh, phiCapN2LO0, phiCapN2LOL
    elif(lMax >= 1 and lMin >= 1):
        (xMesh, resL, xMeshNLO, resNLOL, xMeshN2LO, resN2LOL, sigmaRMesh, phiCapLOL, phiCapNLOL, phiCapN2LOL) = solveRes(lMin, lMax, fitMethod)
        return e0, xMesh, resL, e1, xMeshNLO, resNLOL, e2, xMeshN2LO, resN2LOL, sigmaRMesh, [], phiCapLOL, sigmaRMesh, [], phiCapNLOL, sigmaRMesh, [], phiCapN2LOL
    elif(lMax == 0 and lMin == 0):
        (xMesh, res0, xMeshNLO, resNLO0, xMeshN2LO, resN2LO0, sigmaRMesh, phiCapLO0, phiCapNLO0, phiCapN2LO0) = solveRes(0, 0, fitMethod)
        return e0, xMesh, res0, e1, xMeshNLO, resNLO0, e2, xMeshN2LO, resNLO0, sigmaRMesh, phiCapLO0, [], sigmaRMesh, phiCapNLO0, [], sigmaRMesh, phiCapN2LO0, []
    else:
        return False
