# Generate the plot data for LO Pionless and NLO Pionless, with error controled by varing mambda (cutoff)
import common
import numpy as np
import constant as const
import plotTool as pltool
from matplotlib import pyplot as plt
import pionless as pionl
import pandas as pd

nMax = 10
NsigmaR = 60
e1 = 12
sigmaIMeV = 5
lMax = 10
########## next leading order and leading order from scattering length ##########
e0 = -1.41 / const.HBarC
para = [e0, e1]
c0Method = 'scattering length'
mambdaMeV0 = 400
nGau = 600
(sigmaRMeshDens, rMeshLO0, rMeshNLO0) = pionl.pionlessSolver(para = para, nMax = nMax, checkOpt = 'off', sigmaI = sigmaIMeV / const.HBarC, nGau = nGau, mambda = mambdaMeV0 / const.HBarC, c0Method = c0Method, e0 = e0, NsigmaR = NsigmaR, lMax = lMax)
mambdaMeV1 = 1200
nGau = 1000
(sigmaRMeshDens, rMeshLO1, rMeshNLO1) = pionl.pionlessSolver(para = para, nMax = nMax, checkOpt = 'off', sigmaI = sigmaIMeV / const.HBarC, nGau = nGau, mambda = mambdaMeV1 / const.HBarC, c0Method = c0Method, e0 = e0, NsigmaR = NsigmaR, lMax = lMax)
########## Output data ##########
def outPutRMesh(sigmaRMeshDens, rMesh, mambda, orderState):
    '''output the response function to /data/
    orderState: LO or NLO'''
    rMeshR = np.vstack( (sigmaRMeshDens, rMesh) ).T
    rMeshRPd = pd.DataFrame(rMeshR)
    rMeshRPd.to_csv('../../../data/resPion' + orderState + str(int(mambda)) + '.csv', header = None, index = None)
outPutRMesh(sigmaRMeshDens, rMeshLO0, 400, 'LO')
outPutRMesh(sigmaRMeshDens, rMeshLO1, 1200, 'LO')
outPutRMesh(sigmaRMeshDens, rMeshNLO0, 400, 'NLO')
outPutRMesh(sigmaRMeshDens, rMeshNLO1, 1200, 'NLO')
