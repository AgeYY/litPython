# Inversion the lit you obtained from litSolver.py to get the response function

import common
import constant as const
from scipy import integrate as intg
import numpy as np
import scipy.optimize
from scipy.optimize import lsq_linear
import plotTool as pltool
from matplotlib import pyplot as plt
import myMath # personal mathermatical function

#def base1(omega, n, para):
#    '''
#    para(list): [eb, e1]
#    fit bases for lit. Ref: efros1994
#    500 / HBarC is the upper limit of base for n smaller than 15 (base has value). the minimum must be greater than para[0].
#    '''
#    ans = np.power(omega * const.HBarC + para[0], n - 0.5) * np.exp(-(omega * const.HBarC + para[0]) / para[1]) # convert the omega's unit to fm
#    return ans

def base1(omega, n, para):
    '''
    para(list): [eb, e1], e1 is 10 to 20
    fit bases for lit. Ref: S. Martinelli1995
    500 / HBarC is the upper limit of base for n smaller than 15 (base has value).
    '''
    ans = np.power((omega + const.EB) * const.HBarC , n - 0.5) * np.exp(-((omega + const.EB) * const.HBarC) / para[1]) # convert the omega's unit to fm
    return ans

def base2(omega, n, para):
    '''
    para(list): [eb, e1]
    fit bases for lit. Ref: ReviewEfros2008, alpha1 = para[1], alpha2 = 1
    500 / HBarC is the upper limit of base for n smaller than 15 (base has value). omega start from 0
    '''
    return np.power((omega + const.EB) * const.HBarC, para[1]) * np.exp(-((omega + const.EB) * const.HBarC) / n) # convert the omega's unit to fm

def base3(omega, n, para):
    '''
    para(list): [eb, e1]
    fit bases for lit. Ref: ReviewEfros2008, alpha1 = para[1], alpha2 = 1
    500 / HBarC is the upper limit of base for n smaller than 15 (base has value). omega start from 0
    '''
    return np.power((omega + const.EB) * const.HBarC, para[1]) * np.exp(-((omega + const.EB) * const.HBarC)**1.25 / n) # convert the omega's unit to fm

class inversion:
    base = 0
    para = []
    sigmaI = 5 / const.HBarC
    mambda = 600
    omegaMin = const.ETH # break up energy
    omegaMax = 1.41 # max omege = sigmaRMax - sigmaI
    coe = np.array([])
    nMax = 1
    rMesh = np.array([])

    def __init__(self, sigmaI, mambda):
        '''
        All inputs have unit fm or fm^-1
        Input:
        xMesh: x mesh of the original function (or sigmaR).
        yMesh: y mesh of the original function (or lit)
        omegaMin, omegaMax: the minimum and maximum x values when integrating the base to obtain the response function

        Out
        response function(outSize*2): The first colum is the x variable of the response function and second colum is the response.
        '''
        self.sigmaI = sigmaI
        self.mambda = mambda
        self.update()

    def setBase(self, base, para, omegaMax):
        '''
        base(functional): base function, the first parameter is the x var, second one is order n, the rests are para
        para: parameters in the base function. This must be consists with the para order in the base function. One possible para is eb, binding energy of the deteron. If only leading order is considered, eb = -1.41 / HBarC. If NLO is included, eb = -2.22 / HBarC. The unit is fm. e1: free parameter in the base
        '''
        self.base = base
        self.para = para
        self.omegaMin = const.ETH # omega must greater than -para[0]
        self.omegaMax = omegaMax # 500 / HBarC is a good choice. The range of omega must cover all the base function

    def baseLit4Check(self, sigmaRMesh, n):
        # self.baseLit can generate the baseLit table. This is only for checking.
        def baseAdd(omega, n, para, base, sigmaR, sigmaI):
            # The function inside the integrate of the baselit function
            return base(omega, n, para) / ((sigmaI * sigmaI) + np.power((omega - sigmaR), 2))
        size = sigmaRMesh.size
        yMesh = np.empty(size)
        for i in np.arange(size):
            yMesh[i] = intg.quad(baseAdd, self.omegaMin, self.omegaMax, args=(n, self.para, self.base, sigmaRMesh[i], self.sigmaI))[0]
        return yMesh

    def baseLit(self, sigmaRMesh, n, nGau = 300):
        self.omegaMin = const.ETH
        self.omegaMax = np.max(sigmaRMesh) - self.sigmaI
        (omegaMesh, wMesh) = myMath.legGaussPoint(nGau, self.omegaMin, self.omegaMax, method = 'plain') # Generate legGaussPoints for omega.
        baseMesh = np.zeros((n, nGau)) # base * weight
        for i in range(n):
            for j in range(nGau):
                baseMesh[i, j] = self.base(omegaMesh[j], i + 1, self.para) * wMesh[j]
        kernelMesh = np.zeros((nGau, sigmaRMesh.size)) # mesh points for the lorentz kernel
        for i in range(nGau):
            for j in range(sigmaRMesh.size):
                kernelMesh[i, j] = 1.0 / ((self.sigmaI * self.sigmaI) + np.power((omegaMesh[i] - sigmaRMesh[j]), 2))
        baseLitMesh = baseMesh.dot(kernelMesh)
        return baseLitMesh

    def update(self):
        return

    def fitData(self, xMesh, yMesh, nMax, method = 'normalEq', nGau = 300):
        '''
        give the coe of the bases. This is a linear lsq problem
        '''
        self.nMax = nMax
        size = xMesh.size
        baseLitMesh = self.baseLit(xMesh, nMax, nGau = nGau)
        # Generate A
        A = np.zeros((size, nMax))
        for i in np.arange(nMax):
            A[:, i] = baseLitMesh[i, :]
        if(method == 'normalEq'):
            res = np.linalg.solve(A.T.dot(A), A.T.dot(yMesh)) # Normal equation algorithm
        elif(method == 'smart'):
            res = lsq_linear(A, yMesh, bounds=(0, xMesh[-1]), lsmr_tol='auto').x # Built-in function
        else:
            res = lsq_linear(A, yMesh, bounds=(0, xMesh[-1]), lsmr_tol='auto').x # Built-in function
        self.coe = res
        return self.coe

    def responseFunc(self, xMesh):
        '''
        give the response function according to the coe from fitData. xMesh will be modified in order to be greater than omegaMin
        '''
        self.xMesh = xMesh[xMesh >= self.omegaMin]
        self.rMesh = np.zeros(self.xMesh.size)
        for n in np.arange(self.nMax):
            self.rMesh += self.coe[n] * self.base(self.xMesh, n + 1, self.para) # n start from 1
        return (self.xMesh, self.rMesh)

    def checkFit(self, xMesh, yMesh, checkN, fig = 42):
        '''
        This should give the original yMesh in the fitData function
        fig = 1: plt.figure(1)
        '''
        xMesh1 = np.linspace(xMesh.min(), xMesh.max(), checkN)
        checkMesh = np.zeros(xMesh1.size)
        for n in np.arange(self.nMax):
            checkMesh += self.coe[n] * self.baseLit4Check(xMesh1, n + 1) # For base function, n starts from 1

        pltool.simplePlot(xMesh, yMesh / const.HBarC / const.HBarC, fig = fig, lab = 'Original curve') # Convert to MeV and compare with the result in the paper
        pltool.simplePlot(xMesh1, checkMesh / const.HBarC/ const.HBarC, fig = fig, lab = 'Your fit func') # Convert to MeV and compare with the result in the paper

    def outRF(self, a0, b0, outSize):
        '''
        print out the response function with mesh size as outSize
        '''
        pass

def invertFromAFunc(para):
    invF = inversion(para['sigmaI'], para['mambda'])
    if(para['base0'] == 'base1'):
        invF.setBase(base1, [const.E0, para['e1']], para['omegaMax'])
    elif(para['base0'] == 'base2'):
        invF.setBase(base2, [const.E0, para['e1']], para['omegaMax'])
    elif(para['base0'] == 'base3'):
        invF.setBase(base3, [const.E0, para['e1']], para['omegaMax'])
    else:
        os.abort()
    invF.fitData(para['x'], para['y'], para['nMax'], method = 'normalEq')
    return invF
