# new inversion methods
import common
import constant as const
import numpy as np
from matplotlib import pyplot as plt
import litInversion as litinv
import invNetWork as invnn
import torch.optim as optim
import torch
import torch.nn as nn
import torch.nn.functional as F

class NInv(litinv.inversion):
    def __init__(self, sigmaI):
        self.sigmaI = sigmaI

    def kenel(self, sigmaR, omega, wOmega):
        self.kMat = np.empty((sigmaR.size, omega.size))
        for i in range(self.kMat.shape[0]):
            self.kMat[i, :] = 1 / ( (omega - sigmaR[i])**2 + self.sigmaI**2 ) * wOmega
        return self.kMat

    def normalEq(self, sigmaR, caphi, omega, wOmega):
        # normal equation method
        self.kenel(sigmaR, omega, wOmega)
        self.rMesh = np.linalg.pinv(self.kMat.T.dot(self.kMat)).dot(self.kMat.T).dot(caphi)
        return omega, self.rMesh

    def GDes(self, sigmaR, caphi, omega, wOmega, alpha = 0.000001, iter_number = 10000, reg = None, dreg = None, mambda = 0):
        # linear regression ~ gradient descent
        self.kenel(sigmaR, omega, wOmega)
        alpha=alpha # learning rate
        rMesh=np.random.rand(len(omega))
        iter_number =iter_number 
        J = np.empty(iter_number)
        for i in np.arange(iter_number):    
            if dreg is None:
                rMesh = rMesh - alpha*np.dot(self.kMat.T,(np.dot(self.kMat,rMesh)-caphi))/self.kMat.shape[0]
            else:
                rMesh = rMesh - alpha*np.dot(self.kMat.T,(np.dot(self.kMat,rMesh)-caphi))/self.kMat.shape[0] + dreg(mambda, rMesh)
        diff = self.kMat.dot(rMesh)-caphi
        if reg is None:
            J[i] = diff.dot(diff.T)/2/self.kMat.shape[0]
        else:
            J[i] = diff.dot(diff.T)/2/self.kMat.shape[0] + reg(mambda, rMesh)/2/self.kMat.shape[0]
        self.rMesh = rMesh
        return omega, self.rMesh

    def l2reg(self, mambda, rMesh):
        # regulator term for GDes
        return mambda * rMesh.dot(rMesh)
    def l2dreg(self, mambda, rMesh):
        # differential regulator 
        return mambda * rMesh

    def trad(self, sigmaR, caphi, omega, wOmega, fitOmega = 500, nMax = 10, para1 = 20, base = None):
        # traditional method
        fitOmega = fitOmega / const.HBarC
        para = [const.E0, para1]
        nMax = nMax
        if base is None:
            super().setBase(litinv.base2, para, fitOmega)
        else:
            super().setBase(base, para, fitOmega)
        super().fitData(sigmaR, caphi, nMax, method = 'normalEq')
        xMesh, rMesh = super().responseFunc(omega)
        return self.xMesh, self.rMesh

    def plotRes(self, xMesh, rMesh, figlab=0):
        # plot out the response function
        plt.figure(figlab)
        plt.plot(xMesh * const.HBarC, rMesh / const.HBarC)
        plt.grid()
        plt.show()

    def checkRes(self, sigmaR, caphi, omega, wOmega, rMesh, figlab = 0):
        # invert the response function to caphi, plot out both the original can new caphi for checking
        kMat = self.kenel(sigmaR, omega, wOmega)
        caphiCheck = kMat.dot(rMesh)
        plt.figure(figlab)
        plt.plot(sigmaR * const.HBarC, caphiCheck, label = 'Check your result')
        plt.plot(sigmaR * const.HBarC, caphi, label = 'Original Curve')
        plt.legend()
        plt.grid()
        plt.show()

    def setLoss(self, sigmaR, omega, wOmega, Loss = invnn.SmoothLoss, paraLoss = None):
        # set the loss function
        sigmaR, omega, wOmega = torch.from_numpy(sigmaR), torch.from_numpy(omega), torch.from_numpy(wOmega) # convert to torch.tensor
        if paraLoss is None:
            self.criterion = Loss(sigmaR, omega, wOmega, self.sigmaI)
        else:
            self.criterion = Loss(sigmaR, omega, wOmega, self.sigmaI, *paraLoss)

    def neuron(self, sigmaR, caphi, omega, wOmega, lr = 0.00001, nSteps = 2000, ANN = invnn.Net):
        net = ANN(len(sigmaR), len(omega))
        sigmaR, omega, wOmega, caphi = torch.from_numpy(sigmaR), torch.from_numpy(omega), torch.from_numpy(wOmega), torch.from_numpy(caphi).float() # convert to torch.tensor

        #caphibar = torch.rfft(caphi, signal_ndim = 1)
        #caphibar[20:] = 0
        #caphi = torch.irfft(caphibar, signal_ndim = 1, signal_sizes = caphi.shape)
        optimizer = optim.Adam(net.parameters(), lr=lr)
        self.running_loss = np.zeros(nSteps)
        for i in range(nSteps):
            # zero the parameter gradients
            optimizer.zero_grad()
            outputs = net(caphi)
            lossVal = self.criterion(outputs, caphi)
            lossVal.backward()
            optimizer.step()
            self.running_loss[i] = lossVal.item()
        self.rMesh = self.criterion.trans(outputs.detach()).numpy() # the loss function is probably contains a transform function.
        return omega.numpy(), self.rMesh

    def printLoss(self):
        plt.figure(0)
        plt.plot(self.running_loss)
        plt.show()
        return self.running_loss
