import math

# Define global variables, all vars' values with unit fm or fm^(-1)
global MNucleon, HBarC, E0, E1, R0, Rs1, A0, EB, atolSDGChiralMM, DeuSpin, ETH

HBarC = 197.3286 # MeV * fm
MNucleon = 939.8 / HBarC # Mass of nucleon
E0 = -1.41 / HBarC # Binding energy obtained by leading order
E1 = -2.22 / HBarC - E0 # Binding energy correction from the next leading order. -2.21 is the experimental binding of the deuteron.
EB = -2.22 / HBarC
R0 = 4 * math.pi * math.sqrt(2 * math.pi) * MNucleon # A parameter used for calculating the next leading order potential
Rs1 = 1.75 # Scattering length for the next leading order
DeuSpin = 1 # Deuter total spin
A0 = 5.42 # Leading order scattering length
ETH = 3.34 / HBarC # deuteron breakup threshold. https://journals.aps.org/prc/abstract/10.1103/PhysRevC.52.1193

########## Globale Variable ##########
atolSDGChiralMM = 1e-3
QMEPS = 1e-4
