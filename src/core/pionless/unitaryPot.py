import common
from potential import Pot
import numpy as np
import constant as const
import math
from scipy import optimize
from matplotlib import pyplot as plt

class UnitaryPot(Pot):
    def __init__(self, mambda, a0, E0 = const.E0, kappa0 = 1 / 20, kappa1 = None):
        self.mambda = mambda
        self.a0 = a0
        self.E0 = E0
        self.update(kappa0, kappa1)

    def update(self, kappa0 = 1 / 20, kappa1 = 0.2):
        self.kappa0 = kappa0
        if kappa1 is None:
            kappa1 = 1 / self.a0 - self.kappa0
        self.kappa1 = kappa1
        self.l1 = - math.sqrt(2 * math.pi) * math.pi * const.MNucleon * self.mambda
        self.c0 = 1 / (self.l1 + 2 * math.pi**2 * const.MNucleon * self.kappa0) * 4 * math.pi
        # option 1
        if self.kappa1 is None:
            self.c01 = - np.pi * const.MNucleon / 2 * (1 / self.a0 - self.kappa0) * self.c0**2
        else:
            self.c01 = - np.pi * const.MNucleon / 2 * self.kappa1 * self.c0**2
        self.E0 = self.__findE0() # in scattering method, e0 is determined by letting Ilambda = 1 /c0, while in binding energy method, e0 can be setted manualy.
    def potnlo(self, k, kp, spaceStr):
        if(spaceStr == "mm"):
            return self.c01 * self.regulator(k, kp, spaceStr)
        elif(spaceStr == "cd"):
            return self.c01 * self.mambda**6 / 8 * self.regulator(k, kp, spaceStr)
        else:
            sys.exit("Please indicate the space where the pot lives")
    def __findE0(self):
        be = 0
        # find the binding energy given scattering length
        def ilambda(mambda, be):
            # mambda: cutoff.
            # be: binding energy, should be negtive
            inside = 2 * np.exp(-2 * be * const.MNucleon / mambda / mambda) * np.sqrt(abs(be) * const.MNucleon) * np.pi - mambda * np.sqrt(2 * np.pi) # this should be sqrt(-be * const.MNucleon), yetwill lead to numerical error if your binding energy is 0 (very small positive number). Therefore coonvert to abs
            return np.pi * const.MNucleon * inside / 4 / np.pi
        def c0aEqILambda(be, mambda, a0):
            return ilambda(mambda, be) - 1 / self.c0
        sol = optimize.root_scalar(c0aEqILambda, args=(self.mambda, self.a0), bracket=[-10 / const.HBarC, 0/ const.HBarC], method='brentq')
        be = sol.root
        ########### Plot be curve, be = 0 ##########
        #xMesh = np.linspace(-10 / const.HBarC, 3/ const.HBarC, 100)
        #plt.figure(0)
        #plt.plot(xMesh, c0aEqILambda(xMesh, self.mambda, self.a0))
        #plt.show()
        return be
