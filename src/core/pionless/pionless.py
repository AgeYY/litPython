# Find the leading order response function
import common
from waveFunc import WaveFunc as wfunc
from litSolver import litSolver as lits
import numpy as np
import litInversion as litinv
import constant as const
import plotTool as pltool
from matplotlib import pyplot as plt
import pandas as pd
from potential import Pot
from unitaryPot import UnitaryPot

def pionlessSolver(nGau = 300, mambda = 600 / const.HBarC, sigmaI = 5 / const.HBarC, sigmaRMin = const.ETH, sigmaRMax = 250 / const.HBarC, NsigmaR = 50, qChar = np.sqrt(5), nMax = 10, para = [const.E0, 15], checkN = 20, checkOpt = 'off', fitMethod = 'normalEq', c0Method = 'scattering length', lMax = 10, lMin = 0, bGauR = 2000 / 3, base = 'base1', elo = const.E0, kappa0 = 1 / 20, potype = 'pion'):
    '''
    Find the leading order and next leading order response function of the pionless potential.
    Input:
    nGau(int): Mesh points of the wave function
    mambda(float): cut off
    sigmaI(float, float): para for getting the response function though the lit method
    sigmaRMin, sigmaRMax(float, float): the x-range of the lit function
    NsigmaR(int): mesh points of lit wave function
    qChar(float): charge operator
    nMax(int): Max n value for the inversion
    para(float list): para for the inversion basis
    checkN(int): mesh points for checking if you got the right fit in the inversion
    checkOpt(string): if checkOpt = 'on', this program will plot the fit in the inversion
    '''
    ########## init ##########
    sigmaRMin = const.ETH - sigmaI # see 3.3,ReviewEfros2008
    if potype == 'unitaryPion':
        pot = UnitaryPot(mambda, const.A0, kappa0 = kappa0) # potential
    else:
        pot = Pot(mambda, const.A0, c0Method, E0 = elo) # potential
    waveFunc = wfunc(mambda, nGau, bGaur = bGauR, e0 = elo) # init waveFunc
    waveFunc.setPot(pot)
    e0, e1 = waveFunc.findE0()
    litSolver = lits(waveFunc) # init lit solver
    sigmaRMesh = np.linspace(sigmaRMin, sigmaRMax, NsigmaR) # set sigmaR mesh
    omega = np.linspace(const.ETH, sigmaRMax - sigmaI, 200) # pick the range of omega a bit smaller than sigmaR. See 3.3, ReviewEfros2008
    invF = litinv.inversion(sigmaI, mambda)
    omegaMax = 500 / const.HBarC # This can be read off from the base function.
    if base == 'base2':
        invF.setBase(litinv.base2, para, omegaMax)
    elif base == 'base3':
        invF.setBase(litinv.base3, para, omegaMax)
    else:
        invF.setBase(litinv.base1, para, omegaMax)

    def solveRes(sublMin, sublMax, fit = fitMethod):
        (phiCapLO, phiCapNLO) = litSolver.solverNLOL(qChar, sigmaI, sigmaRMesh, sublMin, sublMax) # L > 0
        invF.fitData(sigmaRMesh, phiCapLO, nMax, method = fit)
        (omegaLoc, rMesh1) = invF.responseFunc(omega) # leading order response function. responseFunc will cut some points in omega to omegaLoc
        if(checkOpt == 'on'):
            invF.checkFit(sigmaRMesh, phiCapLO, checkN)
        invF.fitData(sigmaRMesh, phiCapNLO, nMax, method = fit)
        (omegaLoc, rMeshNLO1) = invF.responseFunc(omega) # next leading order response function
        if(checkOpt == 'on'):
            invF.checkFit(sigmaRMesh, phiCapNLO, checkN)
        return (omegaLoc, rMesh1, omegaLoc, rMeshNLO1, sigmaRMesh, phiCapLO, phiCapNLO)

    if(lMax >= 1 and lMin == 0):
        (xMesh, res0, xMeshNLO, resNLO0, sigmaRMesh, phiCapLO0, phiCapNLO0) = solveRes(0, 0)
        (xMesh, resL, xMeshNLO, resNLOL, sigmaRMesh, phiCapLOL, phiCapNLOL) = solveRes(1, lMax)
        res = resL + res0
        resNLO = resNLOL + resNLO0
        return (e0, xMesh, res, e1, xMeshNLO, resNLO, sigmaRMesh, phiCapLO0, phiCapLOL, sigmaRMesh, phiCapNLO0, phiCapNLOL)
    elif(lMax >= 1 and lMin >= 1):
        (xMesh, resL, xMeshNLO, resNLOL, sigmaRMesh, phiCapLOL, phiCapNLOL) = solveRes(lMin, lMax)
        return (e0, xMesh, resL, e1, xMeshNLO, resNLOL, sigmaRMesh, [], phiCapLOL, sigmaRMesh, [], phiCapNLOL)
    elif(lMax == 0 and lMin == 0):
        (xMesh, res0, xMeshNLO, resNLO0, sigmaRMesh, phiCapLO0, phiCapNLO0) = solveRes(0, 0)
        return (e0, xMesh, res0, e1, xMeshNLO, resNLO0, sigmaRMesh, phiCapLO0, [], sigmaRMesh, phiCapNLO0, [])
    else:
        return False

def plotRes(sigmaRMeshDens, rMeshNLO, rMeshLO):
    pltool.simplePlot(sigmaRMeshDens * const.HBarC, rMeshNLO / const.HBarC, fig = 1,  lab = 'next leadinf order') # Convert to MeV and compare with the result in the paper
    pltool.simplePlot(sigmaRMeshDens * const.HBarC, rMeshLO / const.HBarC, fig = 1,  titleStr = 'response function', lab = 'leading order') # Convert to MeV and compare with the result in the paper
    plt.grid()
    plt.show()

def outRes(sigmaRMeshDens, rMeshNLO):
    ########### output to file ##########
    rFunc = pd.DataFrame(np.vstack((sigmaRMeshDens * const.HBarC, rMesh / const.HBarC)).T)
    rFunc.to_csv('../../data/LITSigmaRLO.csv', index = None, header = None)
