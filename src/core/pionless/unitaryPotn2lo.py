from unitaryPot import UnitaryPot as UPot
import numpy as np
import constant as const

class U2Pot(UPot):
    def update(self, kappa0 = 1 / 20, kappa1 = 0.2):
        super().update(kappa0 = kappa0, kappa1 = kappa1)
        self.c2 = self.c0**3 * (const.MNucleon * self.kappa1)**2 /  (4 * np.pi)**4

    def potn2lo(self, k, kp, spaceStr):
        if(spaceStr == "mm"):
            return self.c2 * self.regulator(k, kp, spaceStr)
        elif(spaceStr == "cd"):
            return self.c2 * self.mambda**6 / 8 * self.regulator(k, kp, spaceStr)
        else:
            sys.exit("Please indicate the space where the pot lives")
