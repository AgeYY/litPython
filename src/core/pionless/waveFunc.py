import constant as const
from scipy import integrate as intg
from potential import Pot
import math
import myMath
import numpy as np
from scipy.interpolate import interp1d

class WaveFunc:
    # LO and NLO wave function. We only use the partial wave function which is Sqrt(4 Pi) times the whole wave function
    mambda = 1 # Cutoff

    nGau = 400 # number of mesh points in momentum space
    #nGaur = 400 # number of mesh points in coordinate space
    aGau = 0 # Integrate start point
    bGau = 3 * mambda # Integrate end point

    pot = 0
    xMesh, wMesh = np.array([]), np.array([])
    xrMesh, wrMesh = np.array([]), np.array([])
    philommMesh, phinlommMesh = np.array([]), np.array([])
    philocdMesh, phinlocdMesh = np.array([]), np.array([])
    ulommMesh, unlommMesh = np.array([]), np.array([])
    ulocdMesh, unlocdMesh = np.array([]), np.array([])
    e0 = 0

    def __init__(self, mambda, nGau, bGaur = 600 / 3, e0 = const.E0):
        self.mambda = mambda
        self.nGau = nGau
        self.bGaur = bGaur # boundary in coordinate space
        self.e0 = const.E0
        self.update()

    def isMambda(self, mambda):
        self.mambda = mambda
        self.update()

    def isNGau(self, nGau):
        self.nGau = nGau
        self.update()

    def osXW(self):
        return (self.xMesh, self.wMesh, self.xrMesh, self.wrMesh)
    def osPhilomm(self):
        return np.copy(self.philommMesh)
    def osPhinlomm(self):
        return np.copy(self.phinlommMesh)
    def osPhilocd(self):
        return self.philocdMesh
    def osPhinlocd(self):
        return self.phinlocdMesh
    def osGau(self):
        return (self.nGau, self.aGau, self.bGau, self.bGaur)
    def osMambda(self):
        return mambda

    def gNot(self, z, k):
        return 1 / (z - k * k / const.MNucleon)

    def __calPhiLomm(self): # Leading order wave function in mm space
        mambda = self.mambda
        philommRaw = lambda k: np.exp(- k * k / mambda / mambda) / (self.e0 - k * k / const.MNucleon)
        norm = myMath.normCal(philommRaw, 0, self.bGau)
        self.philommMesh = philommRaw(self.xMesh) / norm # We prefer to use mesh points derectly

    def __calPhiNlomm(self): # Update the next leading order wave function in mm space
        size = self.nGau
        xMesh = self.xMesh # shallow copy is enought
        wMesh = self.wMesh
        gNotMat = np.zeros((size, size))
        vloMat = np.zeros((size, size))
        vnloMat = np.zeros((size, size))
        eyeMat = np.identity(size)

        for idx in np.arange(size):
            gNotMat[idx, idx] = self.gNot(self.e0, xMesh[idx])
            vloMat[idx, :] = self.pot.potlo(xMesh[idx], xMesh, "mm") * wMesh * xMesh * xMesh
            vnloMat[idx, :] = self.pot.potnlo(xMesh[idx], xMesh, "mm") * wMesh * xMesh * xMesh

        lhsMat = eyeMat - gNotMat.dot(vloMat)
        self.e1 = self.findE1()
        rhsMat = np.dot(gNotMat, np.dot((vnloMat - self.e1 * eyeMat), self.philommMesh))
        phinloMesh = np.linalg.pinv(lhsMat).dot(rhsMat)
        # Orthognal and Normalization
        phinloMesh = phinloMesh - phinloMesh.dot(self.philommMesh * xMesh * xMesh * wMesh) * self.philommMesh
        self.phinlommMesh = phinloMesh # You don't need normalization
        #self.phinlommMesh = phinloMesh / np.sqrt((phinloMesh * xMesh * xMesh * wMesh).dot(phinloMesh)) # We prefer to use mesh points derectly

    def __calPhiLocd(self):
        self.philocdMesh = myMath.fourier3DRadical(self.xMesh, self.wMesh, self.philommMesh, self.xrMesh, self.wrMesh)
    def __calPhiNlocd(self):
        self.phinlocdMesh = myMath.fourier3DRadical(self.xMesh, self.wMesh, self.phinlommMesh, self.xrMesh, self.wrMesh)

    def update(self): # Update vars which depends on the input
        self.bGau = 3 * self.mambda # boundary
        (self.xMesh, self.wMesh) = myMath.legGaussPoint(self.nGau, self.aGau, self.bGau, "plain")
        (self.xrMesh, self.wrMesh) = myMath.legGaussPoint(self.nGau, self.aGau, self.bGaur, "plain")
        self.nGau = self.xMesh.size # it probably has +_1 difference in using legGaussPoint.
        return 0

    def setPot(self, pot):
        # set the potential for wavefunc. pot is a instance of a class which must have same variables (functions) as in potential: self.E0, potlo, and potnlo
        self.pot = pot
        self.e0 = self.pot.E0
        self.__calPhiLomm() # leading order wave function in momentum space
        self.__calPhiNlomm() # next leading wave function in momentum space
        self.__calPhiLocd() # leading order wave function in coordinate space
        self.__calPhiNlocd() # next leading wave function in cd
        self.ulommMesh, self.unlommMesh = self.philommMesh * self.xMesh, self.phinlommMesh * self.xMesh
        self.ulocdMesh, self.unlocdMesh = self.philocdMesh * self.xrMesh, self.phinlocdMesh * self.xrMesh
        return 0

    def findE0(self):
        return (self.e0, self.e0 + self.e1)

    def findE1(self):
        # find the binding energy of the pionless, for both LO and NLO
        vnloMat = np.zeros((self.nGau, self.nGau))
        for i in np.arange(self.nGau): # Potential
            vnloMat[i, :] = self.pot.potnlo(self.xMesh[i], self.xMesh, "mm")
        philommShelled = self.philommMesh * self.xMesh * self.xMesh * self.wMesh
        e0nlo = (philommShelled).dot(vnloMat).dot(philommShelled)
        return e0nlo
