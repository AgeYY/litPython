import numpy as np
from litSolver import litSolver
import constant as const
import matOpt
from scipy import special

class LitSolvern2lo(litSolver):
    def __init__(self, mambda, nGau, aGau, bGau, aGaur, bGaur, xrMesh, wrMesh):
        # set some paras
        self.mambda = mambda
        self.nGau = nGau
        self.aGau = aGau
        self.bGau = bGau
        self.aGaur = aGaur
        self.bGaur = bGaur
        self.xrMesh = xrMesh
        self.wrMesh = wrMesh
        self.eye = np.identity(self.nGau)

    def setLO(self, vlocdMesh, ulocdMesh, e0):
        # vlocdMesh: leading order potential in coordinate space
        # e0: leading order binding energy (negtive)
        self.vlocdMeshShell = self.shellPot(vlocdMesh)
        self.ulocdMesh = ulocdMesh
        self.e0 = e0
    def setNLO(self, vnlocdMesh, unlocdMesh, e1):
        # you must firstly setLO
        self.vnlocdMeshShell = self.shellPot(vnlocdMesh)
        self.unlocdMesh = unlocdMesh
        self.e1 = e1
    def setN2LO(self, vn2locdMesh, un2locdMesh, e2):
        # you must firstly setLO and setNLO. e2: n2lo energy correction
        self.vn2locdMeshShell = self.shellPot(vn2locdMesh)
        self.un2locdMesh = un2locdMesh
        self.e2 = e2

    def shellPot(self, potMesh):
        return (self.xrMesh * self.eye).dot(potMesh * self.xrMesh * self.wrMesh)

    def __loMatGenSigmaR(self, sigmaR, sigmaI):
        mat = (- sigmaR + 1j * sigmaI) * self.eye
        return mat

    def __loMatGenNoSigmaRL(self, l):
        dOpt = matOpt.dOpt(self.xrMesh)
        ddOpt = dOpt.dot(dOpt)
        mat1 = - ddOpt / const.MNucleon - self.e0 * self.eye
        if(l == 0):
            mat2 = self.vlocdMeshShell
        else:
            mat2 = l * (l + 1) / self.xrMesh / self.xrMesh * self.eye / const.MNucleon
        return mat1 + mat2

    def fLjlGen(self, qChar, l, order = 'lo'):
        if(order == 'lo'):
            return special.spherical_jn(l, qChar * self.xrMesh / 2) * self.ulocdMesh
        elif(order == 'nlo'):
            return special.spherical_jn(l, qChar * self.xrMesh / 2) * self.unlocdMesh
        elif(order == 'n2lo'):
            return special.spherical_jn(l, qChar * self.xrMesh / 2) * self.un2locdMesh

    def potFunc(self, potMeshShell, l):
        if l == 0:
            return potMeshShell
        else:
            return np.zeros(potMeshShell.shape)

    def solverN2LO(self, qChar, sigmaI, sigmaRMesh, lMin, lMax):
        '''
        solve the leading order lit equations with l = {0, 1, 2, ..., lMax}. And then sum up all l's contribution
        '''
        size = sigmaRMesh.size
        phiCap = np.zeros(size, dtype = np.complex)
        phiCapNLO = np.zeros(size, dtype = np.complex)
        phiCapN2LO = np.zeros(size, dtype = np.complex)
        for l in np.arange(lMin, lMax + 1):
            mat1 = self.__loMatGenNoSigmaRL(l)
            fLjlMesh = self.fLjlGen(qChar, l, 'lo')
            fLjlMeshNLO = self.fLjlGen(qChar, l, 'nlo')
            fLjlMeshN2LO = self.fLjlGen(qChar, l, 'n2lo')
            for i in np.arange(size):
                matSigma = self.__loMatGenSigmaR(sigmaRMesh[i], sigmaI)
                lhs = mat1 + matSigma

                # solving LO
                lhsbd = np.copy(lhs) # Copy for adding the boundary condiction
                self.addBoundCondi(lhsbd, fLjlMesh, 0, 0, -1, 0)

                self.phiLITMesh = np.linalg.solve(lhsbd, fLjlMesh) # leading order lit wave function

                # solving NLO
                inhomo = - self.potFunc(self.vnlocdMeshShell, l) + self.e1 * self.eye
                rhs = fLjlMeshNLO + (inhomo).dot(self.phiLITMesh)  # right handside of the nlo equation
                self.addBoundCondi(lhs, rhs, 0, 0, -1, 0)
                self.phiLITMeshNLO = np.linalg.solve(lhs, rhs) # leading order lit wave function

                # solving N2LO
                inhomo2 = -self.potFunc(self.vn2locdMeshShell, l) + self.e2 * self.eye
                rhs = fLjlMeshN2LO + inhomo.dot(self.phiLITMeshNLO) + (inhomo2).dot(self.phiLITMesh)
                self.addBoundCondi(lhs, rhs, 0, 0, -1, 0)
                self.phiLITMeshN2LO = np.linalg.solve(lhs, rhs) # leading order lit wave function

                phiCap[i] += self.findPhiCapJ(l, self.phiLITMesh)
                #phiCapNLO[i] += self.findPhiCapJ(l, self.phiLITMesh, phiLITMeshNLO = self.phiLITMeshNLO, method = 'nlo')
                phiCapNLO[i] += self.findPhiCapJ(l, self.phiLITMesh, phiLITMeshNLO = self.phiLITMeshNLO, method = 'nlo')
                phiCapN2LO[i] += self.findPhiCapJ(l, self.phiLITMesh, phiLITMeshNLO = self.phiLITMeshNLO, phiLITMeshN2LO = self.phiLITMeshN2LO, method = 'n2lo')
        return (np.real(phiCap), np.real(phiCapNLO + phiCap), np.real(phiCap + phiCapNLO + phiCapN2LO)) # The imaginary component should be very small
