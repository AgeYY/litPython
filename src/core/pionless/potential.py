import common
import constant as const
import numpy as np
import sys
import math
from scipy import integrate as intg
from scipy import optimize
from matplotlib import pyplot as plt

class Pot:
    mambda = 1
    a0 = 0
    l1 = 0 # para for calculating potential, see Sebastian's note
    l3 = 0 # para for calculating potential
    c0 = 0 # leading order
    c2 = 0 # next leading order -- second term
    c01 = 0 # next leading order -- second term

    def __init__(self, mambda, a0, c0Method = 'scattering length', E0 = const.E0):
        '''
        input:
        c0Method(str): method of calculating c0. The default is calculating by the scattering length, which can also be used for calculating the nlo potential etc. If you pick method = 'binding energy', c0 will be fitted according to the binding energy, const.E0. The result of nlo calculation in this case is invaliad.
        '''
        self.mambda = mambda
        self.a0 = a0
        self.E0 = E0
        self.update(c0Method)

    def update(self, c0Method):
        self.l1 = - math.sqrt(2 * math.pi) * math.pi * const.MNucleon * self.mambda
        self.l3 = - self.mambda**3 * const.MNucleon * math.sqrt(math.pi / 2) * math.pi / 2
        if(c0Method == 'scattering length'):
            self.c0 = 1 / (self.l1 + 2 * math.pi**2 * const.MNucleon / self.a0) * 4 * math.pi
            self.E0 = self.__findE0() # in scattering method, e0 is determined by letting Ilambda = 1 /c0, while in binding energy method, e0 can be setted manualy.
        elif(c0Method == 'binding energy'):
            def ilambda(q, mambda, be):
                return q * q * np.exp(-2 * q * q / mambda / mambda) / (const.MNucleon * be - q * q) * const.MNucleon
            self.c0 = 1 / intg.quad(ilambda, 0, 3 * self.mambda, args=(self.mambda, self.E0))[0]
        else:
            self.c0 = 1 / (self.l1 + 2 * math.pi**2 / self.a0 * const.MNucleon) * 4 * math.pi
        self.c2 = - (- math.pi**2 * const.MNucleon * const.Rs1 + const.R0 / self.mambda) * self.c0**2 / 2 / 4 / math.pi
        self.c01 = -2 * self.c0 * self.c2 * self.l3 / 4 / math.pi

    def __findE0(self):
        # find the binding energy given scattering length
        def ilambda(mambda, be):
            # mambda: cutoff.
            # be: binding energy, should be negtive
            inside = 2 * np.exp(-2 * be * const.MNucleon / mambda / mambda) * np.sqrt(-be * const.MNucleon) * np.pi - mambda * np.sqrt(2 * np.pi)
            return np.pi * const.MNucleon * inside / 4 / np.pi
        def c0aEqILambda(be, mambda, a0):
            return ilambda(mambda, be) - 1 / self.c0
         
        sol = optimize.root_scalar(c0aEqILambda, args=(self.mambda, self.a0), bracket=[-10 / const.HBarC, 0/ const.HBarC], method='brentq')
        return sol.root

    def iosMambda(self, mambda):
        self.mambda = mambda
        self.update()

    def iosA0(self, a0):
        self.a0 = a0
        self.update()

    def regulator(self, k, kp, spaceStr):
        if(spaceStr == "mm"):
            return np.exp(- (k * k + kp * kp) / self.mambda / self.mambda)
        elif(spaceStr == "cd"):
            return np.exp(- self.mambda**2 * (k**2 + kp**2) / 4)
        else:
            sys.exit("Please indicate the space where the pot lives")

    def potlo(self, k, kp, spaceStr):
        if(spaceStr == "mm"): # pot in momentum space
            return self.c0 * self.regulator(k, kp, spaceStr)
        elif(spaceStr == "cd"): # pot in coordinate space
            return self.c0 * self.mambda**6 / 8 * self.regulator(k, kp, spaceStr)
        else:
            sys.exit("Please indicate the space where the pot lives")

    def __pot01(self, k, kp):
        return self.c01 * self.regulator(k, kp)

    def __pot1(self, k, kp):
        return self.c2 * self.regulator(k, kp) * (k**2 + kp**2)

    def potnlo(self, k, kp, spaceStr):
        if(spaceStr == "mm"):
            return (self.c2 *  (k**2 + kp**2) + self.c01) * self.regulator(k, kp, spaceStr)
        elif(spaceStr == "cd"):
            return (self.c01 * self.mambda**6 / 8 - self.c2 * self.mambda**8 / 32 * (-12 + self.mambda**2 * k**2 + self.mambda**2 * kp**2)) * self.regulator(k, kp, spaceStr)
        else:
            sys.exit("Please indicate the space where the pot lives")
