# Solving the lit equation in coordinate space
import common
import numpy as np
import constant as const
import matOpt
from scipy import special

import plotTool as pltool
from matplotlib import pyplot as plt

class litSolver:
    mambda = 1
    nGau = 400 # number of mesh points in momentum space
    #nGaur = 400 # number of mesh points in coordinate space
    aGau = 0 # Integrate start point
    bGau = 3 * mambda # Integrate end point
    bGaur = 150 / mambda # end boundary in coordinate space

    xrMesh, wrMesh = np.array([]), np.array([])
    vlocdMesh, vnlocdMesh = np.array([[]]), np.array([[]])
    ulocdMesh, unlocdMesh = np.array([]), np.array([])

    nCoe = 1

    eye = np.identity(nGau)
    phiLITMesh = np.array([])

    def __init__(self, waveFunc):
        self.mambda = waveFunc.mambda
        (self.nGau, self.aGau, self.bGau, self.bGaur) = waveFunc.osGau()
        self.xrMesh, self.wrMesh = waveFunc.xrMesh, waveFunc.wrMesh
        self.ulocdMesh, self.unlocdMesh = waveFunc.ulocdMesh, waveFunc.unlocdMesh
        self.vlocdMesh = np.zeros((self.nGau, self.nGau))
        self.vnlocdMesh = np.zeros((self.nGau, self.nGau))
        self.e0lo, self.e0nlo = waveFunc.findE0()
        for i in np.arange(self.nGau):
            self.vnlocdMesh[i, :] = waveFunc.pot.potnlo(self.xrMesh[i], self.xrMesh, "cd")
            self.vlocdMesh[i, :] = waveFunc.pot.potlo(self.xrMesh[i], self.xrMesh, "cd")
        self.update()

    def update(self):
        self.eye = np.identity(self.nGau)
        #self.__calCoe()

    #def __calCoe(self):
    #    # Find out the convention which the paper uses. nGau should be all 1. The ans is nCoe = 1
    #    dOpt = matOpt.dOpt(self.xrMesh)
    #    self.nCoe = np.zeros(self.nGau)
    #    intVPhi = np.zeros(self.nGau)
    #    for i in np.arange(self.nGau):
    #        intVPhi[i] = self.xrMesh[i] * np.sum(self.vlocdMesh[i, :].dot(self.xrMesh * self.ulocdMesh * self.wrMesh))
    #    self.nCoe = dOpt.dot(dOpt).dot(self.ulocdMesh) / const.MNucleon + const.E0 * self.ulocdMesh
    #    self.nCoe = self.nCoe / intVPhi

    def fLjlGen(self, qChar, l, order = 'lo'):
        if(order == 'lo'):
            return special.spherical_jn(l, qChar * self.xrMesh / 2) * self.ulocdMesh
        elif(order == 'nlo'):
            return special.spherical_jn(l, qChar * self.xrMesh / 2) * self.unlocdMesh

    def __loMatGenNoSigmaRL(self, l, potName = 'lo'):
        dOpt = matOpt.dOpt(self.xrMesh)
        ddOpt = dOpt.dot(dOpt)
        mat1 = - ddOpt / const.MNucleon
        potMesh = self.vlocdMesh
        if(l == 0):
            mat2 = (self.nCoe * self.xrMesh * self.eye).dot(potMesh * self.xrMesh * self.wrMesh)
        else:
            mat2 = l * (l + 1) / self.xrMesh / self.xrMesh * self.eye / const.MNucleon
        return mat1 + mat2

    def __loMatGenSigmaR(self, sigmaR, sigmaI, eType = 'lo'):
        be = self.e0lo
        mat = (- be - sigmaR + 1j * sigmaI) * self.eye
        return mat

    def findPhiCapJ(self, l, phiLITMesh, phiLITMeshNLO = np.array([]), method = 'lo', phiLITMeshN2LO = np.array([])):
        '''
        Find the lit though the lit wave function. if method = 'nlo', you must input the correspond nlo lit wave function
        '''
        if(l == 0):
            jFactor = 1 # L = 0 ==> j = 1 ==> jFactor = 3 / 3 = 1
        else:
             jFactor = (6 * l + 3) / 3 # jFactor = (2 * l + 1) / 3 + (2 * l + -1) / 3 + (2 * l + 3) / 3
        if(method == 'lo'):
            return  jFactor * phiLITMesh.dot(np.conj(phiLITMesh) * self.wrMesh)
        elif(method == 'nlo'):
            return  2 * jFactor * np.real(phiLITMesh.dot(np.conj(phiLITMeshNLO) * self.wrMesh))
        elif(method == 'n2lo'):
            return  2 * jFactor * np.real(phiLITMesh.dot(np.conj(phiLITMeshN2LO) * self.wrMesh)) + jFactor * phiLITMeshNLO.dot(np.conj(phiLITMeshNLO) * self.wrMesh)

    def addBoundCondi(self, lhs, rhs, xPos1, yVal1, xPos2, yVal2):
        '''
        replace the first and final positions with boundary condictions.
        In:
        lhs((size, size)): matrix on the left handside.
        rhs(size): array on the right hand side
        xPos1(int), xPos2(int): x positions of the boundaries. Remember the positions counts from the xrMesh which the two ends have been deleted
        yVal1(complex or real), yVal2(complex or real): y values of the corresponds postions
        out:
        lhs((size, size)): left matrix with the two ends deleted and the boundary condition added
        rhs(size): right array with the two ends deleted and the boundary condition added
        '''
        con1 = np.zeros(rhs.size)
        con1[xPos1] = 1
        lhs[xPos1, :] = con1
        rhs[xPos1] = yVal1

        con2 = np.zeros(rhs.size)
        con2[xPos2] = 1
        lhs[xPos2, :] = con2
        rhs[xPos2] = yVal2

        return (lhs, rhs)

    def solverL(self, qChar, sigmaI, sigmaRMesh, lMin, lMax):
        '''
        solve the leading order lit equations with l = {0, 1, 2, ..., lMax}. And then sum up all l's contribution
        '''
        size = sigmaRMesh.size
        phiCap = np.zeros(size, dtype = np.complex)
        for l in np.arange(lMin, lMax + 1):
            mat1 = self.__loMatGenNoSigmaRL(l,)
            fLjlMesh = self.fLjlGen(qChar, l, 'lo')
            for i in np.arange(size):
                mat2 = self.__loMatGenSigmaR(sigmaRMesh[i], sigmaI)
                lhs = mat1 + mat2
                (lhs, fLjlMesh) = self.addBoundCondi(lhs, fLjlMesh, 0, 0, -1, 0)
                self.phiLITMesh = np.linalg.solve(lhs, fLjlMesh)
                phiCap[i] += self.findPhiCapJ(l, self.phiLITMesh)
        return np.real(phiCap) # The imaginary component should be very small

    def solverNLOL(self, qChar, sigmaI, sigmaRMesh, lMin, lMax):
        '''
        solve the leading order lit equations with l = {0, 1, 2, ..., lMax}. And then sum up all l's contribution
        '''
        size = sigmaRMesh.size
        phiCap = np.zeros(size, dtype = np.complex)
        phiCapNLO = np.zeros(size, dtype = np.complex)
        for l in np.arange(lMin, lMax + 1):
            mat1 = self.__loMatGenNoSigmaRL(l, 'lo')
            fLjlMesh = self.fLjlGen(qChar, l, 'lo')
            fLjlMeshNLO = self.fLjlGen(qChar, l, 'nlo')
            if(l == 0):
                mat1rhs = (self.nCoe * self.xrMesh * self.eye).dot(self.vnlocdMesh * self.xrMesh * self.wrMesh) # potMesh on the right hand side
            else:
                mat1rhs = 0 * self.eye  # L > 0, potMesh = 0
            for i in np.arange(size):
                mat2 = self.__loMatGenSigmaR(sigmaRMesh[i], sigmaI)
                lhs = mat1 + mat2
                lhsbd = np.copy(lhs) # Copy for adding the boundary condiction
                self.addBoundCondi(lhsbd, fLjlMesh, 0, 0, -1, 0)

                self.phiLITMesh = np.linalg.solve(lhsbd, fLjlMesh) # leading order lit wave function
                rhs = fLjlMeshNLO - (mat1rhs - (self.e0nlo - self.e0lo) * self.eye).dot(self.phiLITMesh)  # right handside of the nlo equation
                self.addBoundCondi(lhs, rhs, 0, 0, -1, 0)
                self.phiLITMeshNLO = np.linalg.solve(lhs, rhs) # leading order lit wave function
                phiCap[i] += self.findPhiCapJ(l, self.phiLITMesh)
                #phiCapNLO[i] += self.findPhiCapJ(l, self.phiLITMesh, phiLITMeshNLO = self.phiLITMeshNLO, method = 'nlo')
                phiCapNLO[i] += self.findPhiCapJ(l, self.phiLITMesh, phiLITMeshNLO = self.phiLITMeshNLO, method = 'nlo')
        return (np.real(phiCap), np.real(phiCapNLO + phiCap)) # The imaginary component should be very small

   # def solverL0General(self, xMesh, wMesh, l, A, potMat, B, fMesh, yVal1, yVal2):
   #     # Solve the general form of the lit l = 0 equation
   #     size = xMesh.size
   #     phiCap = np.zeros(size, dtype = np.complex)

   #     #mat1 = self.__loMatGenNoSigmaRL0()
   #     dOpt = matOpt.dOpt(xMesh)
   #     ddOpt = dOpt.dot(dOpt)
   #     eye = np.identity(size)
   #     mat1 = (A * xMesh * eye).dot(potMat * xMesh * wMesh)
   #     mat2 = - ddOpt
   #     mat = mat1 + mat2

   #     #fLjlMesh = self.fLjlGen(qChar, 0, 'lo')

   #     #mat2 = self.__loMatGenSigmaRL0(sigmaRMesh[i], sigmaI)

   #     lhs = B * eye + mat
   #     (lhs, fMesh) = self.addBoundCondi(lhs, fMesh, 0, yVal1, -1, yVal2)
   #     self.phiLITL0Mesh = np.linalg.solve(lhs, fMesh)
