# Second order correction to wavefunction
import myMath
from waveFunc import WaveFunc
import numpy as np
import constant as const

class WaveFuncn2lo(WaveFunc):
    def __calPhiNlomm(self): # Update the next leading order wave function in mm space
        size = self.nGau
        xMesh = self.xMesh # shallow copy is enought
        wMesh = self.wMesh
        self.gNotMat = np.zeros((size, size))
        self.vloMat = np.zeros((size, size))
        self.vnloMat = np.zeros((size, size))
        eyeMat = np.identity(size)

        for idx in np.arange(size):
            self.gNotMat[idx, idx] = self.gNot(self.e0, xMesh[idx])
            self.vloMat[idx, :] = self.pot.potlo(xMesh[idx], xMesh, "mm") * wMesh * xMesh * xMesh
            self.vnloMat[idx, :] = self.pot.potnlo(xMesh[idx], xMesh, "mm") * wMesh * xMesh * xMesh

        self.lhsMat = eyeMat - self.gNotMat.dot(self.vloMat)
        self.e1 = self.findE1()
        self.g0ve1 = np.dot(self.gNotMat, self.vnloMat - self.e1 * eyeMat)
        rhsMat = np.dot(self.g0ve1, self.philommMesh)
        phinlommMesh = np.linalg.pinv(self.lhsMat).dot(rhsMat)
        # Orthognal
        self.phinlommMesh = phinlommMesh - self.waveDot(phinlommMesh, self.philommMesh, self.xMesh, self.wMesh) * self.philommMesh
        return 0

    def waveDot(self, phi1, phi2, xMesh, wMesh):
        return phi1.dot(phi2 * xMesh * xMesh * wMesh)

    def __calPhiN2lomm(self): # next 2 leading wave function in mm
        eyeMat = np.identity(self.nGau)
        self.vn2loMat = np.zeros((self.nGau, self.nGau))
        for idx in np.arange(self.nGau):
            self.vn2loMat[idx, :] = self.pot.potn2lo(self.xMesh[idx], self.xMesh, "mm") * self.wMesh * self.xMesh * self.xMesh
        self.e2 = self.findE2()
        self.g0ve2 = np.dot(self.gNotMat, self.vn2loMat - self.e2 * eyeMat)
        rhsMat = np.dot(self.g0ve1, self.phinlommMesh) + self.g0ve2.dot(self.philommMesh)
        phin2loMesh = np.linalg.pinv(self.lhsMat).dot(rhsMat)
        # normalization
        coe = - self.waveDot(self.phinlommMesh, self.phinlommMesh, self.xMesh, self.wMesh) / 2 - self.waveDot(self.philommMesh, phin2loMesh, self.xMesh, self.wMesh)
        self.phin2lommMesh = phin2loMesh + coe * self.philommMesh
        return 0

    def __calPhiN2locd(self): # next 2 leading wave function in cd
        self.phin2locdMesh = myMath.fourier3DRadical(self.xMesh, self.wMesh, self.phin2lommMesh, self.xrMesh, self.wrMesh)

    def setPot(self, pot):
        self.flag0 = False # you have not solved lo
        self.flag1 = False # you have not solved nlo
        self.flag2 = False # you have not solved n2lo
        self.pot = pot
        self.e0 = self.pot.E0
        return 0

    def loSolver(self):
        self._WaveFunc__calPhiLomm() # leading order wave function in momentum space
        self._WaveFunc__calPhiLocd() # leading order wave function in coordinate space
        self.flag0 = True # you have solved lo
        self.ulommMesh = self.philommMesh * self.xMesh
        self.ulocdMesh = self.philocdMesh * self.xrMesh

    def nloSolver(self):
        if not self.flag0:
            print("you should calculate lo")
            exit(1)
        self.__calPhiNlomm() # next leading wave function in momentum space
        super()._WaveFunc__calPhiNlocd() # next leading wave function in cd
        self.unlommMesh = self.phinlommMesh * self.xMesh
        self.unlocdMesh = self.phinlocdMesh * self.xrMesh
        self.flag1 = True # you have solved nlo

    def n2loSolver(self):
        if not self.flag0 or not self.flag1:
            print("you should calculate lo and nlo")
            exit(1)
        self.__calPhiN2lomm() # next leading wave function in momentum space
        self.__calPhiN2locd() # next leading wave function in cd
        self.un2lommMesh = self.phin2lommMesh * self.xMesh
        self.un2locdMesh = self.phin2locdMesh * self.xrMesh
        self.flag2 = True # you have solved nlo

    def findE2(self):
        # self.e2 = - self.pot.kappa1**2 / const.MNucleon
        if not self.flag1:
            print('you should calculate LO and NLO')
            exit(1)
        philommShelled = self.philommMesh * self.xMesh * self.xMesh * self.wMesh
        phinlommShelled = self.phinlommMesh * self.xMesh * self.xMesh * self.wMesh
        # you have added a shell to vnloMat and vn2loMat, therefore use phinlommMesh is good enough
        c1 = (philommShelled).dot(self.vnloMat).dot(self.phinlommMesh)
        c2 = (philommShelled).dot(self.vn2loMat).dot(self.philommMesh)
        return c2 + c1

    def osPhin2lomm(self):
        return np.copy(self.phin2lommMesh)

    def osPhin2locd(self):
        return np.copy(self.phin2locdMesh)

    def findE0(self):
        return (self.e0, self.e0 + self.e1, self.e0 + self.e1 + self.e2)
