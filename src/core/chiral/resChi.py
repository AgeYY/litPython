# resChi.py
# Zeyuan Ye, Feb 10, 2020

# Obtaining the response function of the deteron though LIT. Ref: Efro1993

import numpy as np
from src.core.chiral.caphi import Caphi # calculate caphi
import constant as const # constants
import myMath # personal mathermatical function
from matplotlib import pyplot as plt
from src.core.chiral.litSolverChi import litSolverChi as lits # solve the lit equations
import matOpt # personal matrix operator
import scipy.linalg as linalg
from scipy import special
import litInversion as litinv # inversion
from potChiral import potChiral as potChi # chiral potential
from src.core.chiral.SDWavemm import SDWavemm as SDWave # wave function

def plotResponse(rMeshResponse, responseL, l0, lMax):
    '''Plot the response function'''
    plt.plot(rMeshResponse * const.HBarC, responseL / const.HBarC, label = 'L = %d to L = %.d' % (l0, lMax))
    plt.legend()

def plotCaphi(caphi, tail = None):
    '''Show caphi for the last tail chennls'''
    if(tail == None): tail = len(caphi.chs)
    i = len(caphi.chs) # Counter to judge if the loop has reached the last tail channels
    for chn in caphi.chs: # Plot caphi of all channels
        if(i <= tail):
            plt.plot(caphi.sigmaRMesh * const.HBarC, np.array(caphi.chs[chn][0]) / const.HBarC / const.HBarC, label = (chn[0], chn[1], chn[2])) # The label is (j, L, l1)
            plt.plot(caphi.sigmaRMesh * const.HBarC, np.array(caphi.chs[chn][1]) / const.HBarC / const.HBarC, label = (chn[0], chn[1], chn[3])) # The label is (j, L, l2)
        i = i - 1
    plt.legend()

class resChi:
    def resCaphi(self, mambda, nGau, aGau, bGau, aGauR, bGauR, qChar, sigmaI, sigmaRMesh, lMin, lMax):
        ''' Generate three caphi with different shapes.
        mambda = 600 / const.HBarC # Cut off
        nGau = 100 # number of mesh points for wavefunction and potential in the coordinate space and momentum space
        aGau, bGau = 0, 3 * mambda # boundary of pMesh in momentum space
        aGauR, bGauR = 0 / mambda, 150 / mambda # boundary of pMesh in momentum space
        qChar = np.sqrt(5) # momentum transfer
        sigmaI = 5 / const.HBarC # See Efro1993
        sigmaRMesh = np.linspace(1 / const.HBarC, 250 / const.HBarC, 50) # See Efro1993
        lMax = 1 # max L
        '''
        #################### init ####################
        chnSD = np.array([1, 0, 1]) # S-D coupled channel. One must firstly calculate the uWave and wWave function
        (pMesh, wpMesh) = myMath.legGaussPoint(nGau, aGau, bGau, "plain") # generate momentum mesh points
        (rMesh, wrMesh) = myMath.legGaussPoint(nGau, aGauR, bGauR, "plain") # Generate coordinate mesh points. End boundary in coordinate space is usually be taken as 200 / mambda.
        potC = potChi(mambda, pMesh, wpMesh, rMesh, wrMesh, []) # Para is not used here, set it to blank
        potC.potChiral(chnSD) # Generate SD potential mesh
        wave = SDWave(potC.v00MeshP, potC.v01MeshP, potC.v10MeshP, potC.v11MeshP, potC.pMesh, potC.wpMesh, potC.rMesh, potC.wrMesh)
        # Calculate wavefunction
        wave.sdgSolver(atol = const.atolSDGChiralMM) # Calculate S- and D- wave function

        litC = lits(v00MeshR = potC.v00MeshR, v01MeshR = potC.v01MeshR, v10MeshR = potC.v10MeshR, v11MeshR = potC.v11MeshR, rMesh = rMesh, wrMesh = wrMesh) # init a 0 potential solver. Potential as well as wave function and binding energy would be seted when considering particular chn
        caphi = Caphi(sigmaRMesh, sigmaI, qChar) # init
        caphi.genChs(lMin = lMin, lMax = lMax) # generate channels up to L = lMax
        # Necessary parameters for inversion
        self.eb = wave.eb
        self.mambda = mambda
        self.sigmaRMesh = sigmaRMesh
        self.sigmaI = sigmaI
        #wave.wWave = np.zeros(wave.uWave.shape) # if the wWave important?
        wave.wWave = wave.wWave# if the wWave important?
        #################### caphi ####################
        for chn in caphi.chs: # Calculate every chn separately
            potC.potChiral([1, chn[2], chn[0]]) # Generate potential mesh. It only eat the first three quantum numbers. v_l2 would be generated as well. chn has formmat (j, L, l1, l2), while the input of potChiral requires (S, L, J), where S means total spin of the deuteron, usually be taken as 1, L means total orbital momentum which is l1 and l2, J is j, total angular momentum
            # Derteron wave function is unchanged. Set potential.

            ########### zeroPot ##########
            #shape = potC.v00MeshR.shape
            #potC.v00MeshR = np.zeros(shape)
            #potC.v01MeshR = np.zeros(shape)
            #potC.v10MeshR = np.zeros(shape)
            #potC.v11MeshR = np.zeros(shape)
            ###########  ##########

            #cu, cw = 1, 2
            cu, cw = 1, 1
            litC.setPot(wave.uWave * cu, wave.wWave * cw, potC.v00MeshR, potC.v01MeshR, potC.v10MeshR, potC.v11MeshR, wave.eb)
            litC.setQN(j = chn[0], L = chn[1], l1 = chn[2], l2 = chn[3])
            caphi.addSigma(litC, chn)
        ########## Check the summation of contributions from L = L1 to L =L2 ##########
        return caphi.split() # split the caphi into three shapes, and inverse them independently

    def invCaphi(self, omega, caphiShape, paraInv, omegaMax, nMax, checkOpt = 'off', checkN = 20, base = 'base1', fitMethod = 'normalEq'):
        '''
        paraInv = 10 # para for inversion
        omegaMax = 500 / const.HBarC # This can be read off from the base function.
        nMax = 10 # Maximum fiiting order of the inversion
        rMeshResponse = np.linspace(1 / const.HBarC, 250 / const.HBarC, 100) # unit = fm. xMesh for response function
        checkOpt = 'on' # Check if the response is fitted well
        checkN = 20 # Number of mesh points for checking, 20 is good enough
        caphiShape: the caphi you wanna to inverse
        '''
        ########## Inversion ##########
        paraInvList = [self.eb, paraInv]
        invF = litinv.inversion(self.sigmaI, self.mambda)
        if(base == 'base2'):
            invF.setBase(litinv.base2, paraInvList, omegaMax)
        else:
            invF.setBase(litinv.base1, paraInvList, omegaMax)
        invF.fitData(self.sigmaRMesh, caphiShape, nMax, method = fitMethod)
        (omegaLoc, response) = invF.responseFunc(omega) # response function. rMeshResponse would be modified a bit
        if(checkOpt == 'on'):
            invF.checkFit(self.sigmaRMesh, caphiShape, checkN, fig = 42)
            plt.show()
        return (omegaLoc, response)
