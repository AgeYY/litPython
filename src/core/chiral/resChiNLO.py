import resChi
from src.core.chiral.caphiNLO import CaphiNLO # calculate caphi
import numpy as np
import myMath # personal mathermatical function
from potChiral import potChiral as potChi # chiral potential
from src.core.chiral.SDWavemm import SDWavemm as SDWave # wave function
import constant as const # constants
from src.core.chiral.nloLitSolverChi import litSolverChiNLO as litsNLO # solve the lit equations

class resChiNLO(resChi.resChi):
    def resCaphi(self, mambda, nGau, aGau, bGau, aGauR, bGauR, qChar, sigmaI, sigmaRMesh, lMin, lMax):
        ''' Generate three caphi with different shapes.
        mambda = 600 / const.HBarC # Cut off
        nGau = 100 # number of mesh points for wavefunction and potential in the coordinate space and momentum space
        aGau, bGau = 0, 3 * mambda # boundary of pMesh in momentum space
        aGauR, bGauR = 0 / mambda, 150 / mambda # boundary of pMesh in momentum space
        qChar = np.sqrt(5) # momentum transfer
        sigmaI = 5 / const.HBarC # See Efro1993
        sigmaRMesh = np.linspace(1 / const.HBarC, 250 / const.HBarC, 50) # See Efro1993
        lMax = 1 # max L
        '''
        #################### init ####################
        chnSD = np.array([1, 0, 1]) # S-D coupled channel. One must firstly calculate the uWave and wWave function
        (pMesh, wpMesh) = myMath.legGaussPoint(nGau, aGau, bGau, "plain") # generate momentum mesh points
        (rMesh, wrMesh) = myMath.legGaussPoint(nGau, aGauR, bGauR, "plain") # Generate coordinate mesh points. End boundary in coordinate space is usually be taken as 200 / mambda.
        potC = potChi(mambda, pMesh, wpMesh, rMesh, wrMesh, []) # Para is not used here, set it to blank
        potC.potChiral(chnSD) # Generate SD potential mesh
        wave = SDWave(potC.v00MeshP, potC.v01MeshP, potC.v10MeshP, potC.v11MeshP, potC.pMesh, potC.wpMesh, potC.rMesh, potC.wrMesh)
        # Calculate wavefunction
        wave.sdgSolver(atol = const.atolSDGChiralMM) # Calculate S- and D- wave function
        litC = litsNLO(v00MeshR = potC.v00MeshR, v01MeshR = potC.v01MeshR, v10MeshR = potC.v10MeshR, v11MeshR = potC.v11MeshR, rMesh = rMesh, wrMesh = wrMesh) # init a 0 potential solver. Potential as well as wave function and binding energy would be seted when considering particular chn
        caphiNLO = CaphiNLO(sigmaRMesh, sigmaI, qChar) # init
        caphiNLO.genChs(lMin = lMin, lMax = lMax) # generate channels up to L = lMax
        # Necessary parameters for inversion
        self.eb = wave.eb
        self.mambda = mambda
        self.sigmaRMesh = sigmaRMesh
        self.sigmaI = sigmaI
        #################### caphi ####################
        for chn in caphiNLO.chs: # Calculate every chn separately
            potC.potChiral([1, chn[2], chn[0]]) # Generate potential mesh. It only eat the first three quantum numbers. v_l2 would be generated as well. chn has formmat (j, L, l1, l2), while the input of potChiral requires (S, L, J), where S means total spin of the deuteron, usually be taken as 1, L means total orbital momentum which is l1 and l2, J is j, total angular momentum
            potC.potChiralNLO([1, chn[2], chn[0]]) # Generate SD potential mesh
            # Derteron wave function is unchanged. Set potential.
            litC.setPot(wave.uWave, wave.wWave, potC.v00MeshR, potC.v01MeshR, potC.v10MeshR, potC.v11MeshR, wave.eb)
            litC.setPotNLO(v00MeshRNLO = potC.v00MeshRNLO, v01MeshRNLO = potC.v01MeshRNLO, v10MeshRNLO = potC.v10MeshRNLO, v11MeshRNLO = potC.v11MeshRNLO)
            litC.setQN(j = chn[0], L = chn[1], l1 = chn[2], l2 = chn[3])
            caphiNLO.addSigma(litC, chn)
        ########## Check the summation of contributions from L = L1 to L =L2 ##########
        return caphiNLO.split() # split the caphi into three shapes, and inverse them independently

