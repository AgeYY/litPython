# Solving the lit equation in coordinate space. Ref: Efro1993
import common
import numpy as np
import constant as const
import matOpt
from scipy import special
import sympy.physics.wigner as wgn
import sympy
import plotTool as pltool
from matplotlib import pyplot as plt
import litSolverChi as loLIT

class litSolverChiNLO(loLIT.litSolverChi):
    def setPotNLO(self, uWaveNLO = None, wWaveNLO = None, v00MeshRNLO = None, v01MeshRNLO = None, v10MeshRNLO = None, v11MeshRNLO = None, ebNLO = 0):
        # NLO must also use same mesh points in LO
        self.ebNLO = ebNLO
        size = self.rMesh.size
        if uWaveNLO is None:
            self.uWaveNLO = np.zeros(size)
        else:
            self.uWaveNLO = uWaveNLO
        if wWaveNLO is None:
            self.wWaveNLO = np.zeros(size)
        else:
            self.wWaveNLO = wWaveNLO
        self.v00MeshRShelNLO = np.zeros((size, size))
        self.v01MeshRShelNLO = np.zeros((size, size))
        self.v10MeshRShelNLO = np.zeros((size, size))
        self.v11MeshRShelNLO = np.zeros((size, size))
        for i in np.arange(self.rMesh.size):
            rTemp = self.con * self.rMesh[i] * self.rMesh * self.wrMesh
            self.v00MeshRShelNLO[i, :] = v00MeshRNLO[i, :] * rTemp
            self.v01MeshRShelNLO[i, :] = v01MeshRNLO[i, :] * rTemp
            self.v10MeshRShelNLO[i, :] = v10MeshRNLO[i, :] * rTemp
            self.v11MeshRShelNLO[i, :] = v11MeshRNLO[i, :] * rTemp
        return 0
    def fLjlGenNLO(self, L, j, l):
        fac1 = 0
        if(abs(L - l) < const.QMEPS):
            fac1 = self.uWaveNLO
        f0 = special.spherical_jn(l, self.qChar * self.rMesh / 2) * (fac1 + self.alphaLjl(L, j, l) * self.wWaveNLO)
        f1u = np.dot(self.ebNLO * self.eye - self.v00MeshRShelNLO, self.litUWave)
        f1w = np.dot(self.ebNLO * self.eye - self.v11MeshRShelNLO, self.litWWave)
        return f0 + f1u + f1w

    def eq28SolverPrepNLO(self, sigmaI, qChar):
        self.fLjlMeshNLO = np.hstack(
            (self.fLjlGenNLO(self.L, self.j, self.l1),
             self.fLjlGenNLO(self.L, self.j, self.l2))
        )
        return 0

    def eq28SolverNLO(self, sigmaR):
        size = self.rMesh.size
        self.eq28Solver(sigmaR)
        aMatSigma = self.aMatCmpx - np.identity(size * 2) * sigmaR # add sigmaR to aMatCmpx.
        (aMatSigma, fLjlMeshNLO) = matOpt.addBound(aMatSigma, self.fLjlMeshNLO, [0, 0], [-1, 0], [size - 1, 0], [size, 0]) # add four boundary condictions.
        (self.litUWaveNLO, self.litWWaveNLO) = self.obtainPhiLIT(aMatSigma, fLjlMeshNLO)
        return (self.litUWaveNLO, self.litWWaveNLO)
