# Solving the lit equation in coordinate space. Ref: Efro1993
import common
import numpy as np
import constant as const
import matOpt
from scipy import special
import sympy.physics.wigner as wgn
import sympy
import plotTool as pltool
from matplotlib import pyplot as plt

class litSolverChi:
    '''
    Please make sure the following parameters are stted properly before calculating the litWave function:
    v00MeshR, v01MeshR, v10MeshR, v11MeshR, rMesh, wrMesh, channel
    '''
    xrMesh, wrMesh = np.array([]), np.array([])
    v00MeshRShel = np.zeros((1, 1)) # All potential occurs in the form of $int{d r' }v[r, r'] f[r']$. Therefore we are going to use v[r, rp] * r * rp * wrp instead of v[r, rp].
    v01MeshRShel = np.zeros((1, 1))    
    v10MeshRShel = np.zeros((1, 1))
    v11MeshRShel = np.zeros((1, 1))

    con = 1

    eb = 0 # binding energy of deuteron.
    l1, l2 = 0, 0
    j = 0
    L = 0

    ##### These meshes are generated for speed consideration #####
    eye = np.identity(0)
    ddOpt = np.zeros((1, 1)) # Second derivative, which is frequently been used. Therefore we store one here.
    phiLITMesh = np.array([])
    qChar = np.sqrt(5)
    fLjlMesh = np.array([])
    aMatCmpx = np.array([])
    sigmaI = 0

    def __init__(self, uWave = None, wWave = None, rMesh = None, wrMesh = None, v00MeshR = None, v01MeshR = None, v10MeshR = None, v11MeshR = None, l1 = 0, l2 = 2, j = 1, L = 0, eb = -2.22 / const.HBarC, qChar = np.sqrt(5)):
        '''
        rMesh, wrMesh are fundamental variables, which could effect the value of v00MeshR, v01MeshR, v10MeshR, v11MeshR, uWave and wWave. Therefore instead of using setMesh, use __init__ directly is preferable.
        input:
        rMesh, wrMesh:
        v00MeshRShel, v01MeshRShel, v10MeshRShel. v11MeshRShel (double[rMesh.size][rMesh.size]): # All potential occurs in the form of $int{d r' }v[r, r'] f[r']$. Therefore we are going to use v[r, rp] * r * rp * wrp instead of v[r, rp]. Where v is the potential in coordinate space. Read the value from src.core.chiral.potChiral.
        l1, l2 (double): L of obital angular momentum in a coupled channel. The default set is S-D coupling.
        eb(double): binding energy of deuteron.
        '''
        self.l1, self.l2 = l1, l2
        self.j, self.L = j, L
        self.rMesh, self.wrMesh = np.copy(rMesh), np.copy(wrMesh)
        self.setPot(uWave, wWave, v00MeshR, v01MeshR, v10MeshR, v11MeshR, eb)
        self.__update()

    def __update(self):
        self.eye = np.identity(self.rMesh.size)
        self.ddOpt = matOpt.dOpt(self.rMesh) # Second derivative
        self.ddOpt = self.ddOpt.dot(self.ddOpt)
        return 0

    def setEb(self, eb):
        self.eb = eb
        return 0

    def setQN(self, j = 1, L = 0, l1 = 0, l2 = 2):
        ''' Set quantum numbers, j, L, l1, l2. See Efro1993 for more detail'''
        self.l1 = l1
        self.l2 = l2
        self.j = j
        self.L = L
        return 0

    def setQChar(self, qChar):
        self.qChar = qChar
        return 0

    def setPot(self, uWave = None, wWave = None, v00MeshR = None, v01MeshR = None, v10MeshR = None, v11MeshR = None, eb = const.EB):
        self.eb = eb
        self.uWave, self.wWave = uWave, wWave # u wave function and \omega wave function
        size = self.rMesh.size
        self.v00MeshRShel = np.zeros((size, size))
        self.v01MeshRShel = np.zeros((size, size))
        self.v10MeshRShel = np.zeros((size, size))
        self.v11MeshRShel = np.zeros((size, size))
        for i in np.arange(self.rMesh.size):
            rTemp = self.con * self.rMesh[i] * self.rMesh * self.wrMesh
            self.v00MeshRShel[i, :] = v00MeshR[i, :] * rTemp
            self.v01MeshRShel[i, :] = v01MeshR[i, :] * rTemp
            self.v10MeshRShel[i, :] = v10MeshR[i, :] * rTemp
            self.v11MeshRShel[i, :] = v11MeshR[i, :] * rTemp
        return 0

    def printWave(self, figlab = 0):
        # Show the S- and D- wave function for checking
        plt.figure(figlab)
        plt.plot(self.rMesh, self.uWave, label = 'u-Wave')
        plt.plot(self.rMesh, self.wWave, label = 'w-Wave')
        plt.xlabel('fm')
        plt.ylabel('fm^(-1)')
        plt.legend()
        plt.grid()
        plt.show()

    def alphaLjl(self, L, j, l):
        sign = 1
        if (j // 2 == 0): sign = -1
        return sign * np.sqrt(15) * np.sqrt((2 * l + 1) * (2 * L + 1)) * sympy.N(wgn.wigner_3j(L,l,2, 0,0,0) * wgn.wigner_6j(2,1,1, j,l,L), 16)

    def fLjlGen(self, L, j, l):
        fac1 = 0
        if(abs(L - l) < const.QMEPS):
            fac1 = self.uWave
        return special.spherical_jn(l, self.qChar * self.rMesh / 2) * (fac1 + self.alphaLjl(L, j, l) * self.wWave)

    def eq28SolverPrep(self, sigmaI, qChar):
        '''
         Store sigma, fLjlMesh and aMatCmpx to self. They can be used in eq28Solver() as well. This function is used to speed up eq28Solver because it can do the minimum calculation when sigmaR is changed.
        '''
        self.qChar = qChar
        self.sigmaI = sigmaI
        size = self.rMesh.size
        eye = np.identity(size * 2)
        aMat = self.genAMat() # generate mat in lhs with no sigma
        self.fLjlMesh = np.hstack(
            (self.fLjlGen(self.L, self.j, self.l1),
             self.fLjlGen(self.L, self.j, self.l2))
        )
        self.aMatCmpx = aMat +  1j * eye * sigmaI # Type casts to complex. Add sigmaI.
        return 0

    def eq28Solver(self, sigmaR):
        '''
        Solve equation (28) in Efro1993 (lit version). All potential are non-zero.
        '''
        size = self.rMesh.size
        aMatSigma = self.aMatCmpx - np.identity(size * 2) * sigmaR # add sigmaR to aMatCmpx.
        (aMatSigma, fLjlMesh) = matOpt.addBound(aMatSigma, self.fLjlMesh, [0, 0], [-1, 0], [size - 1, 0], [size, 0]) # add four boundary condictions.
        (self.litUWave, self.litWWave) = self.obtainPhiLIT(aMatSigma, fLjlMesh)
        # return (aMatSigma, self.litUWave, self.litWWave) # Uncomment this line for checking the aMat and litWavefunction
        return (self.litUWave, self.litWWave)

    def obtainPhiLIT(self, aMatSigma, fLjlMesh):
        litWave = np.linalg.solve(aMatSigma, np.array(fLjlMesh, dtype = np.complex))
        litUWave = litWave[:self.rMesh.size]
        litWWave = litWave[self.rMesh.size:]
        return (litUWave, litWWave)

    def eq27Solver0Pot(self):
        '''
        If pots are all 0, equation (28) clapse to one equation which is identical with equation (27)
        '''
        pass

    def genAMat(self):
        '''
        generate matrix of the left of equation (28)
        input:
        l1, l2 (double): see eq (28)
        v00, v10 (double[size][size]): potential mesh v_{jl_{1}}, and v_{jl_{2}}

        ouput:
        aMat(double[size*2][size*2]): mat on the left without sigma
        '''
        matProto = - self.ddOpt / const.MNucleon - self.eb * self.eye
        mat00 = matProto + self.l1 * (self.l1 + 1) / self.rMesh / self.rMesh / const.MNucleon * self.eye + self.v00MeshRShel # the first term in equation (28a)
        mat11 = matProto + self.l2 * (self.l2 + 1) / self.rMesh / self.rMesh / const.MNucleon * self.eye + self.v11MeshRShel # the first term in equation (28b)
        aMat = np.hstack( (  np.vstack((mat00, self.v10MeshRShel)), np.vstack((self.v01MeshRShel, mat11))  ) )
        return aMat
