from caphi import Caphi
import numpy as np

class CaphiNLO(Caphi):
    def addSigma(self, litC, chn):
        ''' 
        add a caphi_l(sigma) to chs.
        litC = litSolverChi(...):
        '''
        # Check if litC has prepared. -- Blank here
        # Calculate caphi_l
        litC.eq28SolverPrep(self.sigmaI, qChar = self.qChar) # prepare
        for sigmaR in self.sigmaRMesh:
            (litUWave, litWWave) = litC.eq28Solver(sigmaR) # obtain the litWave
            litC.eq28SolverPrepNLO(self.sigmaI, qChar = self.qChar) # prepare
            (litUWaveNLO, litWWaveNLO) = litC.eq28SolverNLO(sigmaR) # obtain the litWave
            caphi_l1 = (2 * chn[0] + 1) / 3 * litUWave.dot(np.conj(litUWave) * litC.wrMesh) + litUWaveNLO.dot(np.conj(litUWave) * litC.wrMesh) * 2 # this should be caphi_l1 = (2 * chn[0] + 1) / 3 * litUWave.dot(np.conj(litUWave) * litC.wrMesh) + litUWaveNLO.dot(np.conj(litUWave) * litC.wrMesh) + litUWave.dot(np.conj(litUWaveNLO) * litC.wrMesh), but we don't care the imaginary part anyway
            if(chn[2] == chn[3]): # Uncoupled channel, two results caphi_l1 = caphi_l2 = caphi_l / 2, so that when summing over all cahnnels, caphi_l is summed for only once. chn[2] = l1, chn[3] = l2
                caphi_l1 = caphi_l1 / 2
                caphi_l2 = caphi_l1
            else:
                caphi_l2 = (2 * chn[0] + 1) / 3 * litWWave.dot(np.conj(litWWave) * litC.wrMesh) + litWWaveNLO.dot(np.conj(litWWave) * litC.wrMesh) * 2 # same above
            self.chs[tuple(chn)][0].append(caphi_l1.real) # We did inner products, therefore the imaginary part is 0
            self.chs[tuple(chn)][1].append(caphi_l2.real)
        return (litUWave, litWWave, litUWaveNLO, litWWaveNLO)
