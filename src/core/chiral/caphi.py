# caphi.py
# Zeyuan Ye, Feb 9, 2020

# Obtain caphi in equation (23)

import numpy as np
import constant as const
from matplotlib import pyplot as plt

class Caphi():
    '''
    Decription of attributes
    chs = {chn1 : [caphi1_l1, caphi1_l2], chn2: [caphi1_l1, caphi1_l2], ...}: all possible channels along with the corresponds term of caphi[sigmaMesh]. The procedure would be: litPhi_{j, Ll}(rMesh, sigma) --integrate rMesh--> caphi_l(sigmaMesh) (defination of chaphi1_l1 is here) --sum over chennels--> caphi --inverse--> response function. Each chn has quantum number: [j, L, l1, l2]. For uncoupled case, L = l1 = l2.
    sigmaRMesh: sigmaRMesh for caphi (also caphi_l).
    sigmaI = sqrt(5):
    qChar:
    Overall, please make sure the following parameters are setted properly before calculating caphi:
    sigmaRMesh, litC, chn, sigmaI, qChar, chs (though genChs())
    '''
    chs = {} # See the defination on the top of this class.
    def __init__(self, sigmaRMesh = None, sigmaI = None, qChar = None):
        self.sigmaRMesh = sigmaRMesh # See the defination on the top of this class.
        self.sigmaI = sigmaI
        self.qChar = qChar
    def addSigma(self, litC, chn):
        ''' 
        add a caphi_l(sigma) to chs.
        litC = litSolverChi(...):
        '''
        # Check if litC has prepared. -- Blank here
        # Calculate caphi_l
        litC.eq28SolverPrep(self.sigmaI, qChar = self.qChar) # prepare
        for sigmaR in self.sigmaRMesh:
            (litUWave, litWWave) = litC.eq28Solver(sigmaR) # obtain the litWave
            ########## show v dot litWave ##########
            #if (sigmaR < 60 / const.HBarC) and (sigmaR > 40 / const.HBarC):
            #    self.showUdotW(litC, litUWave, sigmaR)
            ####################
            caphi_l1 = (2 * chn[0] + 1) / 3 * litUWave.dot(np.conj(litUWave) * litC.wrMesh)
            if(chn[2] == chn[3]): # Uncoupled channel, two results caphi_l1 = caphi_l2 = caphi_l / 2, so that when summing over all cahnnels, caphi_l is summed for only once. chn[2] = l1, chn[3] = l2
                caphi_l1 = caphi_l1 / 2
                caphi_l2 = caphi_l1
            else:
                caphi_l2 = (2 * chn[0] + 1) / 3 * litWWave.dot(np.conj(litWWave) * litC.wrMesh)
            self.chs[tuple(chn)][0].append(caphi_l1.real) # We did inner products, therefore the imaginary part is 0
            self.chs[tuple(chn)][1].append(caphi_l2.real)
        return (litUWave, litWWave)

    def showUdotW(self, litC, litUWave, sigmaR):
        # show r int{dr'} r' v(r, r') litWave_sigma
        v00lit = litC.v00MeshRShel.dot(litUWave)
        fig, ax = plt.subplots(2, 1)
        ax[0].set_title(sigmaR * const.HBarC)
        ax[0].plot(litC.rMesh, litUWave)
        ax[1].plot(litC.rMesh, v00lit)
        plt.show()

    def setSigmaR(self, sigmaRMesh):
        self.sigmaRMesh = sigmaRMesh
    def genChs(self, lMax = 10, lMin = 0):
        '''Generate channels which is legal for equation (27)(28) in Efro 1993'''
        for l in range(lMin, lMax + 1): # These are the ranges of quantum numbers according to L
            for j in range(l - 1, l + 2):
                for l1 in [l - 2, l, l + 2]:
                    for l2 in [l - 2, l, l + 2]:
                        chn = np.array([j, l, l1, l2])
                        if self.ifChsLegal(chn):
                            self.chs[tuple(chn)] = [[], []] # add this channel
    def addChn(self, chn):
        assert(self.ifChsLegal(chn)), 'Inputed chennel is illegal'
        self.chs[tuple(chn)] = [[], []]
    def cleanChs(self):
        '''clear all channels'''
        self.chs.clear()
    def deleteCaphi(self):
        '''Clear all caphi'''
        for chn in self.chs:
            self.chs[chn] = [[], []]
    def ifChsLegal(self, chn):
        ''' Check if a channel has legal quantum numbers. These rules can be found in equation(27) Efro 1993.  For uncoupled case, L = l1 = l2'''
        j, L, l1, l2 = tuple(chn) # unpack quantun numbers
        if(j < 0 or L < 0 or l1 < 0 or l2 < 0): return False # all quantum numbers must be non-negtive
        if(j == 0 and L == 1 and L == l1 and L == l2): # first case in rule (i)
            return True
        elif(j == L and L > 0 and L == l1 and L == l2): # second case in rule (i):
            return True
        elif(j == L - 1 and L > 1 and l1 == L - 2 and l2 == L): # case (ii)
            return True
        elif(j == L + 1 and  l1 == L and l2 == L + 2): # case (iii)
            return True
        else: return False
    def sumCaphi(self, rage):
        ''' Summing over contributions from L = rage[0] to L = rage[1] '''
        #print('valid keys in rage: ', [chn for chn in self.chs if (chn[1] <= rage[1] and chn[1] >= rage[0])]) # Show all valid channels
        caphiList = np.array([self.chs[chn] for chn in self.chs if (chn[1] <= rage[1] and chn[1] >= rage[0])])
        caphiList = np.sum(caphiList, axis = 1)
        return np.sum(caphiList, axis = 0)
    def split(self):
        '''Split caphi into three different shapes. This one is only used in chiral potential, or other potential with S-D coupling'''
        caphi0 = np.zeros(self.sigmaRMesh.size) # caphi for (j, L, l) = (1, L, 0)
        caphi2 = np.zeros(self.sigmaRMesh.size) # caphi for (j, L, l) = (1, L, 2)
        caphiL = np.zeros(self.sigmaRMesh.size) # caphi for the rest of channels
        for chn, caphiVal in self.chs.items():
            if(chn[0] == 1 and chn[2] == 0 and chn[1] == 0): # Note that l1 < l2. If j == 1, l1 == 0, l2 must be 2 since the coupling rule.
                caphi0 = caphi0 + np.array(caphiVal[0])
                caphi2 = caphi2 + np.array(caphiVal[1])
            else:
                caphiL = caphiL + np.array(caphiVal[0]) + np.array(caphiVal[1])
        return (caphi0, caphi2, caphiL)
