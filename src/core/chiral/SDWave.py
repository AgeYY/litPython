import src.tool.matOpt as matOpt
import src.core.general.constant as const
import numpy as np
import plotTool as pltool
from matplotlib import pyplot as plt
from scipy import optimize

class SDWave:
    # Find the deteron S and D wave function. Please see your doc/SAndD.pdf, doc/waveFunc.pdf and OneNote for more detail (you probably need to contact zeyuanye@hotmail.com for doc/ dir since it is not pushed to gitlab).
    '''
    input:
    rMesh, wrMesh:
    v00MeshR, v01MeshR, v10MeshR. v11MeshR (rMesh.size * rMesh.size)(float): potentials in coordinate space. Read the value from src.core.chiral.potChiral.
    Eb(float): Binding energy, the experimental result is -2.22 MeV. You can change the con (see below) to find out the Eb by yourself, and compare it with the experimental result.
    '''
    v00MeshR, v01MeshR, v10MeshR, v11MeshR = np.zeros((1,1)), np.zeros((1,1)), np.zeros((1,1)), np.zeros((1,1)) # coupled and uncoupled part of potential in the coordinate space.
    rMesh, wrMesh = np.array([]), np.array([])
    eb = -2.22 / const.HBarC
    sWave, dWave = np.zeros((1,1)), np.zeros((1,1))
    con = 1 # related with the convention

    def __init__(self, v00MeshR = None, v01MeshR = None, v10MeshR = None, v11MeshR = None, rMesh = None, wrMesh = None):
        '''
        input:
        rMesh, wrMesh:
        v00MeshR, v01MeshR, v10MeshR. v11MeshR (rMesh.size * rMesh.size)(float): potentials in coordinate space. Read the value from src.core.chiral.potChiral.
        '''
        self.v00MeshR = v00MeshR
        self.v01MeshR = v01MeshR
        self.v10MeshR = v10MeshR
        self.v11MeshR = v11MeshR
        self.rMesh, self.wrMesh = rMesh, wrMesh
        self.__update()

    def __update(self):
        pass

    def setEb(self, Eb):
        # Eb(float): Binding energy, the experimental result is -2.22 MeV. You can change the con (see below) to find out the Eb by yourself, and compare it with the experimental result.
        self.eb = eb

    def setCon(self, con):
        # convention. con  = 1 in this case.
        self.con = con

    def genAMat(self):
        size = self.rMesh.size
        d2Opt = matOpt.dOpt(self.rMesh) # 2nd order diffrential opt
        d2Opt = d2Opt.dot(d2Opt)
        identity = np.identity(size)
        ########### a matrix, see the defination in your OneNote ##########
        a00, a01, a10, a11 = np.zeros((size, size)), np.zeros((size, size)), np.zeros((size, size)), np.zeros((size, size))
        for i in range(size):
            rTemp = self.con * self.rMesh[i] * self.rMesh * self.wrMesh
            a00[i, :] = -d2Opt[i, :] / const.MNucleon + rTemp * self.v00MeshR[i, :] - self.eb * identity[i, :]
            a01[i, :] = rTemp * self.v01MeshR[i, :]
            a10[i, :] = rTemp * self.v10MeshR[i, :]
            a11[i, :] =  rTemp * self.v11MeshR[i, :] - d2Opt[i, :] / const.MNucleon + 6 / (self.rMesh[i] * self.rMesh[i]) / const.MNucleon * identity[i, :] - self.eb * identity[i, :]
        aMat = np.vstack((np.hstack((a00, a01)), np.hstack((a10, a11))))
        ########## Add the boundary condition ##########
        aMat[0, :] = 0 # Clear the first row and replace it with the boundary condition, u[0] = 0
        aMat[0, 0] = 1
        aMat[size - 1, :] = 0 # Clear the first row and replace it with the boundary condition, u[-1] = 0
        aMat[size - 1, size - 1] = 1
        aMat[size, :] = 0 # Clear the first row and replace it with the boundary condition, w[0] = 0
        aMat[size, size] = 1
        aMat[-1, :] = 0 # Clear the first row and replace it with the boundary condition, w[-1] = 0
        aMat[-1, -1] = 1 # Clear the first row and replace it with the boundary condition, w[-1] = 0
        return aMat

    def norm(self):
        # Normalization
        n2Factor = self.wrMesh * (self.sWave * self.sWave + self.dWave * self.dWave)
        n2Factor = np.sum(n2Factor)
        self.sWave = self.sWave / np.sqrt(n2Factor)
        self.dWave = self.dWave / np.sqrt(n2Factor)
        return 0

    def sdgSolver(self, rtol = 1e-9, atol = 1e-9):
        aMat = self.genAMat()
        size = self.rMesh.size
        #bd = np.zeros(size)
        #bd[size // 2 - 1], bd[-2] = 1, 1
        #size = self.rMesh.size
        ########## Find the wave function ##########
        wave = matOpt.nullspace(aMat, atol = atol, rtol = rtol)[:, 0] # Take out the first sol
        self.sWave = wave[:size]
        self.dWave = wave[size:]
        self.norm()
        return (self.sWave, self.dWave)

    def findDet(self, eb):
        self.eb = eb
        aMat = self.genAMat() / 10 # scaling to avoid overflow
        return np.linalg.det(aMat)

    def findEB(self, a0 = -10 / const.HBarC, b0 = -0.5 / const.HBarC):
        sol = optimize.root_scalar(self.findDet, bracket=[a0, b0], method='brentq')
        self.eb = sol.root
        return sol.root

    def printWave(self, figlab = 0):
        plt.figure(figlab)
        plt.plot(self.rMesh, self.sWave, label = 'u-Wave')
        plt.plot(self.rMesh, self.dWave, label = 'w-Wave')
        plt.legend()
        plt.grid()
        plt.show()

    def testHarmonic(self):
        d2Opt = matOpt.dOpt(self.rMesh) # 2nd order diffrential opt
        d2Opt = d2Opt.dot(d2Opt)
        identity = np.identity(self.rMesh.size)
        a11 = np.zeros((self.rMesh.size, self.rMesh.size))
        for i in np.arange(self.rMesh.size):
            a11[i, :] = -d2Opt[i, :] + 0.01 * identity[i, :] # -d2Opt is changed to d2Opt
        bd = np.zeros(self.rMesh.size)
        bd[0] = 0
        bd[-1] = 1

        a11[0, :] = 0
        a11[0, 0] = 1
        a11[-1, :] = 0
        a11[-1, -1] = 1

        wave = np.linalg.solve(a11, bd)
        print(a11)
        plt.figure(0)
        plt.scatter(self.rMesh, wave)
        plt.show()
