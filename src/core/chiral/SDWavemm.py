# Find out the S- and D- wavefunction in the momentum representation
import src.tool.matOpt as matOpt
import src.core.general.constant as const
import numpy as np
import plotTool as pltool
from matplotlib import pyplot as plt
from scipy import optimize
import myMath

class SDWavemm:
    '''
    input:
    rMesh, wrMesh:
    v00MeshR, v01MeshR, v10MeshR. v11MeshR (rMesh.size * rMesh.size)(float): potentials in coordinate space. Read the value from src.core.chiral.potChiral.
    eb(float): Binding energy, the experimental result is -2.22 MeV. You can change the con (see below) to find out the Eb by yourself, and compare it with the experimental result.
    aMat(double[size * 2][size * 2]): kernel
    '''
    vMesh = np.zeros((1, 1, 1, 1))
    kMesh, wkMesh = np.array([]), np.array([])
    rMesh, wrMesh = np.array([]), np.array([])
    eb = -2.22 / const.HBarC
    sWave, dWave = np.zeros((1,1)), np.zeros((1,1))
    uWave, wWave = np.zeros((1,1)), np.zeros((1,1))
    con = 1 # related with the convention
    aMat = np.zeros((1,1))

    def __init__(self, v00MeshP = None, v01MeshP = None, v10MeshP = None, v11MeshP = None, kMesh = None, wkMesh = None, rMesh = None, wrMesh = None):
        '''
        None is used to indicate that one can set their value later
        input:
        kMesh, wkMesh:
        rMesh, wrMesh:
        v00MeshR, v01MeshR, v10MeshP, v11MeshP (rMesh.size * rMesh.size)(float): potentials in coordinate space. Read the value from src.core.chiral.potChiral.
        '''
        self.vMesh = np.zeros((2, 2, kMesh.size, kMesh.size))
        self.vMesh[0, 0, :, :] = v00MeshP
        self.vMesh[0, 1, :, :] = v01MeshP
        self.vMesh[1, 0, :, :] = v10MeshP
        self.vMesh[1, 1, :, :] = v11MeshP
        self.kMesh, self.wkMesh = kMesh, wkMesh
        self.rMesh, self.wrMesh = rMesh, wrMesh
        self.__update()

    def __update(self):
        self.aMat = np.zeros((self.kMesh.size * 2, self.kMesh.size * 2))
        return 0

    def setEb(self, eb):
        # Eb(float): Binding energy, the experimental result is -2.22 MeV. You can change the con (see below) to find out the Eb by yourself, and compare it with the experimental result.
        self.eb = eb

    def setCon(self, con):
        # Set the convention
        self.con = con
        self.__update()

    def setRMesh(self, rMesh, wrMesh):
        self.rMesh, self.wrMesh = rMesh, wrMesh
        self.__update()

    def addG0(self, eb = - 2.22 / const.HBarC):
        '''
        mat(double**): mat that will eb added with g0, 1 / (eb - p^2 / mnucleon)
        eb(double): binding energy (eigenvalue of sdg eq)
        con(double): convention, in your convention, con = 1
        '''
        g0Arr = 1 / (eb - self.kMesh * self.kMesh / const.MNucleon) * self.kMesh * self.kMesh * self.wkMesh
        g0Arr = np.hstack((g0Arr, g0Arr))
        for i in range(len(self.aMat)):
            self.aMat[i, :] = self.aMat[i, :] * g0Arr
        self.aMat = self.aMat - np.identity(self.kMesh.size * 2)
        return self.aMat

    def genAMat(self, eb = - 2.22 / const.HBarC):
        size = self.kMesh.size
        self.aMat = self.con * np.vstack( (np.hstack( (self.vMesh[0, 0, :, :], self.vMesh[0, 1, :, :]) ), np.hstack( (self.vMesh[1, 0, :, :], self.vMesh[1, 1, :, :]) ) ) ) # Convention here
        self.addG0(eb = eb)
        return self.aMat

    def norm(self):
        # Normalization
        n2Factor = self.kMesh * self.kMesh * self.wkMesh * (self.sWave * self.sWave + self.dWave * self.dWave)
        n2Factor = np.sum(n2Factor)
        self.sWave = self.sWave / np.sqrt(n2Factor)
        self.dWave = self.dWave / np.sqrt(n2Factor)
        return 0

    def convertR(self):
        # uWave := phi_0(r) = u(r) / r, uWave := phi_2(r) = w(r) / r
        self.uWave = myMath.fourier3DRadical(self.kMesh, self.wkMesh, self.sWave, self.rMesh, self.wrMesh) * self.rMesh
        self.wWave = myMath.fourier3DRadical(self.kMesh, self.wkMesh, self.dWave, self.rMesh, self.wrMesh, order = 2) * self.rMesh
        return 0

    def sdgSolver(self, atol = 1e-9):
        self.findEB()
        self.genAMat(eb = self.eb)
        size = self.kMesh.size
        ########## Find the wave function ##########
        wave = matOpt.nullspace(self.aMat, atol = atol)[:, 0]
        self.sWave = wave[:size] / (self.Eb - self.kMesh*self.kMesh / const.MNucleon) # from <p|\gamma> to wave
        self.dWave = wave[size:] / (self.Eb - self.kMesh*self.kMesh / const.MNucleon) # from <p|\gamma> to wave
        self.norm()
        self.convertR()
        return (self.sWave, self.dWave)

    def findDet(self, eb):
        self.genAMat(eb = eb) # scaling to avoid overflow
        return np.linalg.det(self.aMat)

    def findEB(self, be0 = -100 / const.HBarC, be1 = 0):
        sol = optimize.root_scalar(self.findDet, bracket=[be0, be1], method='brentq')
        self.Eb = sol.root
        return sol.root

    def printWave(self, figlab = 0):
        plt.figure(figlab)
        plt.title("S- and D- wavefunction in the momentum space")
        plt.plot(self.kMesh, self.sWave, label = 'S-Wave')
        plt.plot(self.kMesh, self.dWave, label = 'D-Wave')
        plt.legend()
        plt.grid()

        plt.figure(figlab + 1)
        plt.title("u and w wavefunction")
        plt.plot(self.rMesh, self.uWave, label = 'u Wave')
        plt.plot(self.rMesh, self.wWave, label = 'w Wave')
        plt.legend()
        plt.grid()
        plt.show()
