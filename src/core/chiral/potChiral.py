# Chiral potential. For I have no chiral pot yet, use pionless as a test instead
import common
import constant as const
import numpy as np
import math
from scipy import integrate as intg
from potential import Pot as potPion
import lib.chiralPot.chengduC as chengdu
import myMath

def mapMambdaName(mambda):
    '''
    map the potName and the cutoff value. The cutoff can only be 400, 600, 800, 1600, 3200 MeV. Note that the input mambda has unit fm^(-1)
    '''
    potName = ''
    if(int(mambda * const.HBarC) == 400): # convert fm^(-1) to MeV
        potName = 'chengdu_MMWLY_400'
    elif(int(mambda * const.HBarC) == 600):
        potName = 'chengdu_MMWLY_600'
    elif(int(mambda * const.HBarC) == 800):
        potName = 'chengdu_MMWLY_800'
    elif(int(mambda * const.HBarC) == 1600):
        potName = 'chengdu_MMWLY_1600'
    elif(int(mambda * const.HBarC) == 3200):
        potName = 'chengdu_MMWLY_3200'
    else:
        print('The cutoff can only be 400, 600, 800, 1600, 3200')
    return potName

class potChiral:
    mambda = 1
    # Potential in the momentum space as the form of V(pMesh, pMesh), and in the coordinate space has the form V(rMesh, rMesh). Potential is same under the exchange of the first coordinate and the second one ('square' like potential)
    pMesh, wpMesh = np.array([]), np.array([])
    rMesh, wrMesh = np.array([]), np.array([])
    para = []
    potName = ''

    v00MeshP, v01MeshP, v10MeshP, v11MeshP = np.zeros((1,1)), np.zeros((1,1)), np.zeros((1,1)), np.zeros((1,1)) # coupled and uncoupled part of potential in the momentum space
    v00MeshR, v01MeshR, v10MeshR, v11MeshR = np.zeros((1,1)), np.zeros((1,1)), np.zeros((1,1)), np.zeros((1,1)) # coupled and uncoupled part of potential in the coordinate space

    def __init__(self, mambda, pMesh, wpMesh, rMesh, wrMesh, para):
        '''
        input:
        mambda(db): cutoff. Can only be 400, 600, 800, 1600, 3200
        para(list): parameter for the possible expansion
        pMesh, qMesh(db(size)): momentum mesh, where p is the out momentum and q is the incident one.
        wpMesh, wqMesh(db(size)): weight of the momentum mesh
        '''
        self.mambda = mambda
        self.pMesh, self.wpMesh = pMesh, wpMesh
        self.rMesh, self.wrMesh = rMesh, wrMesh
        self.para = para
        self.__update()

    def __update(self):
        '''For the dependent variables'''
        self.potName = mapMambdaName(self.mambda)
        self.v00MeshP = np.zeros((self.pMesh.size, self.pMesh.size))
        self.v01MeshP = np.zeros((self.pMesh.size, self.pMesh.size))
        self.v10MeshP = np.zeros((self.pMesh.size, self.pMesh.size))
        self.v11MeshP = np.zeros((self.pMesh.size, self.pMesh.size))
        self.v00MeshR = np.zeros((self.rMesh.size, self.rMesh.size))
        self.v01MeshR = np.zeros((self.rMesh.size, self.rMesh.size))
        self.v10MeshR = np.zeros((self.rMesh.size, self.rMesh.size))
        self.v11MeshR = np.zeros((self.rMesh.size, self.rMesh.size))

    def setCDMesh(rMesh, wrMesh):
        self.rMesh = rMesh
        self.wrMesh = wrMesh

    def potPionless(self, chsTable):
        '''
        print out the pot mesh. This is a leading order pionless version for testing.
        Input:
        chsTable(db(chsCol)): channels in which the pot has non-zero value. chsCol is the column numbers of the channels, which is 3 in here. It has the following format
          s, l, j
          for example, a channel with 1S0,
          chsTable = {0, 0, 0}
        Output:
        potMesh(db(size * size))
        '''
        vloMat = np.zeros((self.pMesh.size, self.pMesh.size))
        if(np.array_equal(chsTable, np.array([1, 0, 1])) ): # The only non zero partial potential in pionless theory
            pion = potPion(self.mambda, const.A0, 'scattering length')
            for idx in np.arange(self.pMesh.size):
                vloMat[idx, :] = pion.potlo(self.pMesh[idx], self.pMesh, "mm")
        return vloMat

    def potChiral(self, chsTable, uptoQn = 0):
        '''
        Calculate the potmesh. This is a leading order chiral version.
        Input:
        chsTable(db(chsCol)): channels in which the pot has non-zero value. chsCol is the column numbers of the channels, which is 3 in here. It has the following format
          s, l, j
          for example, a channel with 1S0,
          chsTable = {0, 0, 0}
        Output:
        self.v00MeshP, self.v01MeshP, self.v10MeshP, self.v11MeshP
        '''
        S = chsTable[0]
        L = chsTable[1]
        J = chsTable[2]

        uptoQn = uptoQn # Label of EFT order: 0 -> LO, 1 -> NLO, etc.

        potVal = np.zeros((2, 2))
        self.v00MeshP, self.v01MeshP, self.v10MeshP, self.v11MeshP = chengdu.potChengdu(L, S, J, uptoQn, self.pMesh * const.HBarC, self.pMesh * const.HBarC, self.potName) # Convert the p, q mesh from fm^(-1) to MeV
        self.v00MeshP = self.v00MeshP * const.HBarC / const.MNucleon  # convert the potential to fm^(-1), and map to your own convention (a factor of mnucleon)
        self.v01MeshP = self.v01MeshP * const.HBarC / const.MNucleon
        self.v10MeshP = self.v10MeshP * const.HBarC / const.MNucleon
        self.v11MeshP = self.v11MeshP * const.HBarC / const.MNucleon

        # Generate potMesh in the coordinate space by using fourier3DRadical twice. Please check OneNote 'Fourier transform of the wave function and the potential' for more detail.
        self.v00MeshR = myMath.fourier3DRadical2(self.pMesh, self.wpMesh, self.v00MeshP, self.rMesh, self.wrMesh)

        self.v01MeshR = myMath.fourier3DRadical2(self.pMesh, self.wpMesh, self.v01MeshP, self.rMesh, self.wrMesh, orderL = 0, orderR = 2)
        self.v10MeshR = myMath.fourier3DRadical2(self.pMesh, self.wpMesh, self.v10MeshP, self.rMesh, self.wrMesh, orderL = 2, orderR = 0)
        self.v11MeshR = myMath.fourier3DRadical2(self.pMesh, self.wpMesh, self.v11MeshP, self.rMesh, self.wrMesh, orderL = 2, orderR = 2)
    def potChiralNLO(self, chsTable, uptoQn = 1):
        '''
        Calculate the potmesh. This is a leading order chiral version.
        Input:
        chsTable(db(chsCol)): channels in which the pot has non-zero value. chsCol is the column numbers of the channels, which is 3 in here. It has the following format
          s, l, j
          for example, a channel with 1S0,
          chsTable = {0, 0, 0}
        Output:
        self.v00MeshP, self.v01MeshP, self.v10MeshP, self.v11MeshP
        '''
        S = chsTable[0]
        L = chsTable[1]
        J = chsTable[2]

        uptoQn = uptoQn # Label of EFT order: 0 -> LO, 1 -> NLO, etc.

        potVal = np.zeros((2, 2))
        self.v00MeshPNLO, self.v01MeshPNLO, self.v10MeshPNLO, self.v11MeshPNLO = chengdu.potChengdu(L, S, J, uptoQn, self.pMesh * const.HBarC, self.pMesh * const.HBarC, self.potName) # Convert the p, q mesh from fm^(-1) to MeV
        self.v00MeshPNLO = self.v00MeshPNLO * const.HBarC / const.MNucleon  # convert the potential to fm^(-1), and map to your own convention (a factor of mnucleon)
        self.v01MeshPNLO = self.v01MeshPNLO * const.HBarC / const.MNucleon
        self.v10MeshPNLO = self.v10MeshPNLO * const.HBarC / const.MNucleon
        self.v11MeshPNLO = self.v11MeshPNLO * const.HBarC / const.MNucleon

        # Generate potMesh in the coordinate space by using fourier3DRadical twice. Please check OneNote 'Fourier transform of the wave function and the potential' for more detail.
        self.v00MeshRNLO = myMath.fourier3DRadical2(self.pMesh, self.wpMesh, self.v00MeshPNLO, self.rMesh, self.wrMesh)

        self.v01MeshRNLO = myMath.fourier3DRadical2(self.pMesh, self.wpMesh, self.v01MeshPNLO, self.rMesh, self.wrMesh, orderL = 0, orderR = 2)
        self.v10MeshRNLO = myMath.fourier3DRadical2(self.pMesh, self.wpMesh, self.v10MeshPNLO, self.rMesh, self.wrMesh, orderL = 2, orderR = 0)
        self.v11MeshRNLO = myMath.fourier3DRadical2(self.pMesh, self.wpMesh, self.v11MeshPNLO, self.rMesh, self.wrMesh, orderL = 2, orderR = 2)
