# Introduction

This project is for calculating the response function of deuteron under the scattering of an electron, with potential either pionless potential or chiral potential.

# How to use
0. cd to lib, and run make
1. input the require parameters into ./input.csv. Note that _fileName_ is the output file's name.
2. cd to litPython/bin/
3. Run 
```Shell
python calculator.py
```
This would generate data to directory _data/fileName_.
4. Open ploter.py, the 7th line is the input file's name. Run
```Shell
python ploter.py -f fileName
```
to plot the function produced by _calculator.py_.
  or write your own file for ploting.
  
If you got a bad fitting result, you can save caphi data to _fileNameCaphi_ (which is in input.csv). Then open inversion.py and modify fitting parameters in dicList manually. Now run inversion.py as the following:

```Shell
 python inversion.py -f ../data/varQCharTempCaphi.csv -o ../data/varQCharTemp.csv -m w
```

where -f option is csv file contains caphi data, which should be same as option _fileNameCaphi_ in input.csv. -o is the ouput path for the response function. -m is the method of ouputting, _w_ represents overwrite the original ouput file, _a_ represents add to the original ouput file. After this, you can again run

```Shell
python ploter -f ../data/varQCharTemp.csv
```

to see the final response function.

# Hints
1. you can use this command to see the time cost of each function
```
python -m cProfile -s tottime ./calculator.py > ../data/chiPerformence.txt
```

