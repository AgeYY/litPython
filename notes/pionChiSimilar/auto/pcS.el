(TeX-add-style-hook
 "pcS"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "bbm"
    "geometry"
    "graphicx"
    "amsmath"
    "float"
    "listings"
    "indentfirst"
    "braket"
    "authblk"
    "extarrows")
   (LaTeX-add-labels
    "cond"
    "new27"
    "new28"
    "caphiphi"
    "tbpot"
    "waves"
    "stdeq"
    "pots"
    "pot10P"
    "trans"
    "estres"
    "alpha"
    "cpare")
   (LaTeX-add-bibitems
    "stie"
    "lorentz"))
 :latex)

