import numpy as np
import matplotlib.pyplot as plt
def kernel(x, sr, si):
    return  1 / ((x - sr)**2 + si**2)

def wave(x, sr):
    # homogenious solution of the response function
    T = 35
    return 0.01 * np.sin(2 * np.pi / T * (x - sr))

x = np.linspace(0, 200, 100)
sr = 50
si = 3

plt.figure(0)
plt.plot(x, kernel(x, sr, si), label = r'$\frac{1}{(\omega - \sigma_R)^2 + \sigma_I^2}$')
idx = np.all([x > 20, x < 80], axis = 0)
plt.plot(x[idx], wave(x[idx], sr), label = r'Homogenous solution')
plt.plot(x, np.zeros(x.shape), linestyle = '--', color = 'r')
plt.plot(sr*np.ones(100), np.linspace(0, 0.12, 100), linestyle = '--')
plt.legend()
plt.show()
