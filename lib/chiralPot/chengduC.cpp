#include <stdio.h>
#include <string.h>
#include <string>

extern "C"{
  void chengdu_DLSPR_350_C(int* L, int* S, int* J, int* uptoQn, double* pout, double* pin, double potval[2][2]);
  void chengdu_dispatch_C(char* pot_name, int* lenPotName);
  void chengdu_hotpot_C(int* L, int* S, int* J, int* uptoQn, double* pout, double* pin, double potval[2][2]);

  void chengduDLSPR350(int L, int S, int J, int uptoQn, double pout, double pin, double* potval);
  void chengduDispatchC(char* potNameChar);
  void chengduHotPotC(int L, int S, int J, int uptoQn, double pout, double pin, double* potval);
  void chengduHotPotCMesh(int L, int S, int J, int uptoQn, int nPout, double* poutMesh, int nPin, double* pinMesh, double* v00Mesh, double* v01Mesh, double* v10Mesh, double* v11Mesh);

void chengduDLSPR350(int L, int S, int J, int uptoQn, double pout, double pin, double* potval){
  double potval2d[2][2];

  chengdu_DLSPR_350_C(&L, &S, &J, &uptoQn, &pout, &pin, potval2d);
  for(int i = 0; i < 2; i++)
    for(int j = 0; j < 2; j++)
      potval[i * 2 + j] = potval2d[j][i];
}
}

void chengduDispatchC(char* potNameChar){
  std::string potName = potNameChar;
  int lenPotName = potName.length();
  char potNameArr[lenPotName];
  strcpy(potNameArr, potName.c_str());

  chengdu_dispatch_C(potNameArr, &lenPotName);
}

void chengduHotPotC(int L, int S, int J, int uptoQn, double pout, double pin, double* potval){
  double potval2d[2][2];

  chengdu_hotpot_C(&L, &S, &J, &uptoQn, &pout, &pin, potval2d);
  for(int i = 0; i < 2; i++)
    for(int j = 0; j < 2; j++)
      potval[i * 2 + j] = potval2d[j][i];
}

void chengduHotPotCMesh(int L, int S, int J, int uptoQn, int nPout, double* poutMesh, int nPin, double* pinMesh, double* v00Mesh, double* v01Mesh, double* v10Mesh, double* v11Mesh){
  /* Generate the whole required potMesh
   potVal: size = pinMesh.size * poutMesh.size*/
  double potval[4];
  int idx;

  idx = 0;
  for(int i = 0; i < nPout; i++){
    for(int j = 0; j < nPin; j++){
      chengduHotPotC(L, S, J, uptoQn, poutMesh[i], pinMesh[j], potval);
      v00Mesh[idx] = potval[0];
      v01Mesh[idx] = potval[1];
      v10Mesh[idx] = potval[2];
      v11Mesh[idx] = potval[3];
      idx ++;
    }
  }
}

//// test if you can call the fortran function
//int main(){
//  double pout, pin;
//  int uptoQn, L, S, J;
//  double potval[4];
//  // Magnitude of in/out momenta
//  pout = 3;
//  pin = 2.5;
//  // QM # of partial waves
//  L = 0;
//  S = 1;
//  J = 1;
//  // Label of EFT order: 0 -> LO, 1 -> NLO, etc.
//  uptoQn = 0;
//
//  chengduDLSPR350(L, S, J, uptoQn, pout, pin, potval);
//
//  for(int i = 0; i < 2; i++)
//    for(int j = 0; j < 2; j++)
//      printf("%.2e\t", potval[i * 2 + j]);
//
//  for(int i = 0; i < 2; i++)
//    for(int j = 0; j < 2; j++)
//      potval[i * 2 + j] = 0;
//
//  L = 0;
//  S = 0;
//  J = 0;
//
//  char potName[] = "chengdu_DLSPR_350";
//  chengduDispatchC(potName);
//  chengduHotPotC(L, S, J, uptoQn, pout, pin, potval);
//
//  for(int i = 0; i < 2; i++)
//    for(int j = 0; j < 2; j++)
//      printf("%.2e\t", potval[i * 2 + j]);
//}
