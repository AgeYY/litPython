module chengduC

  use nneft_type
  use chengdu
  use iso_c_binding ! rename your fortran function
  implicit none

contains

  subroutine chengdu_DLSPR_350_C(L, S, J, uptoQn, pout, pin, potval) bind(c, name='chengdu_DLSPR_350_C')

    integer       ::  uptoQn, L, S, J
    real(NER)     ::  pout, pin, potval(1:2, 1:2)

    call chengdu_DLSPR_350(L, S, J, uptoQn, pout, pin, potval)

  end subroutine chengdu_DLSPR_350_C

  subroutine chengdu_dispatch_C(pot_name, lenPotName) bind(c, name='chengdu_dispatch_C')
    integer, intent(in) :: lenPotName
    character(kind=c_char), intent(in) :: pot_name(lenPotName)

    character(kind=c_char), target :: pot_nameT(lenPotName)
    character(len = lenPotName), dimension(:), pointer:: pot_nameP

    pot_nameT = pot_name

    ! Convert char array to string
    call c_f_pointer (c_loc(pot_nameT), pot_nameP, [1])

    call chengdu_dispatch(pot_nameP(1))

  end subroutine chengdu_dispatch_C

  subroutine chengdu_hotpot_C(L, S, J, uptoQn, pout, pin, potval) bind(c, name='chengdu_hotpot_C')

    integer       ::  uptoQn, L, S, J
    real(NER)     ::  pout, pin, potval(1:2, 1:2)

    call chengdu_hotpot(L, S, J, uptoQn, pout, pin, potval)

  end subroutine chengdu_hotpot_C

end module chengduC
