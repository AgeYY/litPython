from numpy.ctypeslib import ndpointer
import ctypes
import numpy as np
import os

def potChengdu(LEx, SEx, JEx, uptoQnEx, poutExMesh, pinExMesh, potNameEx):

    pwd = os.path.dirname(__file__)
    libPot = ctypes.CDLL(pwd + '/libPot.so')

    # Protofunction for chengduDispatchC
    chengduDispatchC = ctypes.CFUNCTYPE(
        ctypes.c_char_p
    )(('chengduDispatchC', libPot))

    libPot.chengduHotPotC.argtypes = [ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_double, ctypes.c_double, ctypes.POINTER(ctypes.c_double)]

    libPot.chengduHotPotCMesh.argtypes = [ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.POINTER(ctypes.c_double),ctypes.c_int, ctypes.POINTER(ctypes.c_double), ctypes.POINTER(ctypes.c_double), ctypes.POINTER(ctypes.c_double), ctypes.POINTER(ctypes.c_double), ctypes.POINTER(ctypes.c_double)]
    # Pot name
    potName = potNameEx.encode('utf-8')
    potName = ctypes.c_char_p(potName) # Convert python string to bytes
    chengduDispatchC(potName)

    # init the inputs
    npoutEx = len(poutExMesh)
    npinEx = len(pinExMesh)
    sizeEach = npinEx * npoutEx
    npout = ctypes.c_int(npoutEx)
    npin = ctypes.c_int(npinEx)
    L = ctypes.c_int(LEx)
    S = ctypes.c_int(SEx)
    J = ctypes.c_int(JEx)
    uptoQn = ctypes.c_int(uptoQnEx)
    uptoQn = ctypes.c_int(uptoQnEx)
    poutMesh = (ctypes.c_double * npoutEx)(*poutExMesh)
    pinMesh = (ctypes.c_double * npinEx)(*pinExMesh)
    v00Mesh, v01Mesh, v10Mesh, v11Mesh = (ctypes.c_double * sizeEach)(), (ctypes.c_double * sizeEach)(), (ctypes.c_double * sizeEach)(), (ctypes.c_double * sizeEach)()

    # calculate
    libPot.chengduHotPotCMesh(L, S, J, uptoQn, npin, poutMesh, npout, pinMesh, v00Mesh, v01Mesh, v10Mesh, v11Mesh)
    # convert mesh to np
    #v00MeshNP = np.array([v00Mesh[i] for i in range(sizeEach)])
    #v01MeshNP = np.array([v01Mesh[i] for i in range(sizeEach)])
    #v10MeshNP = np.array([v10Mesh[i] for i in range(sizeEach)])
    #v11MeshNP = np.array([v11Mesh[i] for i in range(sizeEach)])

    #v00MeshNP = np.ndarray((sizeEach, ), 'f', v00Mesh, order='C') 
    #v01MeshNP = np.ndarray((sizeEach, ), 'f', v01Mesh, order='C') 
    #v10MeshNP = np.ndarray((sizeEach, ), 'f', v10Mesh, order='C') 
    #v11MeshNP = np.ndarray((sizeEach, ), 'f', v11Mesh, order='C') 

    v00MeshNP = np.array(v00Mesh[:])
    v01MeshNP = np.array(v01Mesh[:])
    v10MeshNP = np.array(v10Mesh[:])
    v11MeshNP = np.array(v11Mesh[:])
    return v00MeshNP.reshape((npoutEx, -1)), v01MeshNP.reshape((npoutEx, -1)), v10MeshNP.reshape((npoutEx, -1)), v11MeshNP.reshape((npoutEx, -1))
