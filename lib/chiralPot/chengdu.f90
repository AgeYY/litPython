! Bingwei Long  01/04/2020 (V0.27)
! - Added MMWLY_350
! Rui Peng  01/04/2020 (V0.26)
! - Updated MMWLY NLO 1S0 short-range paras
! Rui Peng  01/02/2020 (V0.25)
! - MMWLY 1S0 fitted to scattering length (LO) and kcm = 53.075 Mev and
!   phs = 62.9169
! Rui Peng  12/24/2019 (V0.24)
! - MMWLY 1S0 fitted to scattering length
! Rui Peng  12/24/2019 (V0.232)
! - Factorized DLSPR and MMWLY routines
! Bingwei Long 12/03/2019 (V0.231)
! - Fixed a bug in MMWLY
! Bingwei Long 11/30/2019
! - Added MMWLY LO
! Bingwei Long 10/27/2019
! - Added generic interface: chengdu_dispatch, chengdu_hotpot, chengdu_hotpot_Edep
! Bingwei Long 10/22/2019
! - Added chengdu_LBWDIB_600
! Bingwei Long 10/21/2019
! - Fitted pionless_1000 3S1 to a = 5.41 fm (added as comments)
! Bingwei Long 10/20/2019
! - Fitted pionless_1000 3S1 to deuteron BE
! - Modified chengdu.README, show_mtrx.f90
! Bingwei Long 10/19/2019
! - Fixed bugs in chengdu.Makefile, chengdu_DLSPR_350 and chengdu_DLSPR_400
! Rui Peng 10/17/2019
! - Added Pionless LO for 1000 MeV
! Rui Peng 10/07/2019
! - Added 350 and 400 MeV (3S1 fitted to B_deuteron)
! Bingwei Long  10/04/2019
!   - Added init_PCs4chengdu
! Bingwei Long  07/02/2019
!   - Added 800, 1600, 3200 MeV LO values
! Bingwei Long  03/18/2019
!   - Improved comments
!   - Modified to accommodate changes made to interface of *_epwrap()
! Bingwei Long  01/28/2019

module chengdu

  ! Version 0.27

  use potwrap
  use nopion_epmod
  implicit none

  private
  public  :: chengdu_hotpot, chengdu_hotpot_Edep, chengdu_dispatch,     &
    & chengdu_hotpot_is_Edep,                                           &
    & chengdu_pionless_1000,                                            &
    & chengdu_DLSPR_350, chengdu_DLSPR_400, chengdu_DLSPR,              &
    & chengdu_DLSPR_800, chengdu_DLSPR_1600, chengdu_DLSPR_3200,        &
    & chengdu_LBWDIB_600, chengdu_MMWLY_350, chengdu_MMWLY_400,         &
    & chengdu_MMWLY_600, chengdu_MMWLY_800, chengdu_MMWLY_1600,         &
    & chengdu_MMWLY_3200

  integer, parameter    :: CHENGDU_REGTYPE = REGTYPE_GAUSSIAN
  character(len = 128)  :: hotpot_name = "chengdu_DLSPR"
  logical               :: hotpot_is_Edep = .false.
  real(NER)             :: dlspr1s0(1:14), mmwly1s0(1:6), Cs3s1(1:7),   &
    & Cs3p0(1:5), Cs1p1(1:4), Cs3p1(1:4), Cs3p2(1:5), Cs1d2(1:4),       &
    & Cs3d2(1:4), CS3d3(1:4)

  real(NER), parameter  ::  &
    & spr1s0_600_set2_para(1:14) = (/                                       &
    &   0.28476583881316405E+002_NER,   -0.94720748744753225E-001_NER,      &
    &   0.20650483002051136E+002_NER,   -0.97698777628877326E-001_NER,      &
    &   0.95630015528180693E-003_NER,   -0.11694692770857738E+002_NER,      &
    &   0.86955645956220531E-001_NER,   -0.27572312790623797E-002_NER,      &
    &   0.51272513824358814E-007_NER,   -0.15365873931283828E+002_NER,      &
    &   0.24216603991719265E+000_NER,   -0.25138848487176349E-002_NER,      &
    &   0.55453661564745701E-007_NER,    0.61631676811145938E-012_NER/),    &
    &                                                                       &
    & Cs3s1_600_set2_para(1:7) =  (/                                        &
    & -1.4410457830873011E-003_NER, 0.0_NER, 0.0_NER, 0.0_NER,              &
    & 0.0_NER, 0.0_NER, 0.0_NER/),                                          &
    &                                                                       &
    & Cs3p0_600_set2_para(1:5) =  (/                                        &
    & 0.47499661698324306E-007_NER, 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),   &
    &                                                                       &
    & Cs3p2_600_set2_para(1:5) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                        &
    &                                                                       &
    & Cs3p1_600_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs1p1_600_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs1d2_600_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs3d2_600_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs3d3_600_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/)

  real(NER), parameter ::  &
    & spr1s0_800_set2_para(1:14) = (/                                       &
    &   0.26900213333936705E+002_NER,   -0.85152284790577140E-001_NER,      &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER/),                         &
    &                                                                       &
    & Cs3s1_800_set2_para(1:7) =  (/                                        &
    & 2.9151500814276572E-003_NER, 0.0_NER, 0.0_NER, 0.0_NER,               &
    & 0.0_NER, 0.0_NER, 0.0_NER/),                                          &
    &                                                                       &
    & Cs3p0_800_set2_para(1:5) =  (/                                        &
    & -0.39852401392998397E-007_NER, 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),  &
    &                                                                       &
    & Cs3p2_800_set2_para(1:5) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                        &
    &                                                                       &
    & Cs3p1_800_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs1p1_800_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs1d2_800_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs3d2_800_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs3d3_800_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/)

  real(NER), parameter ::  &
    & spr1s0_1600_set2_para(1:14) = (/                                      &
    &   0.25741081111240778E+002_NER,   -0.75410791126733698E-001_NER,      &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER/),                         &
    &                                                                       &
    & Cs3s1_1600_set2_para(1:7) =  (/                                       &
    & -4.6744212081189567E-003_NER, 0.0_NER, 0.0_NER, 0.0_NER,              &
    & 0.0_NER, 0.0_NER, 0.0_NER/),                                          &
    &                                                                       &
    & Cs3p0_1600_set2_para(1:5) =  (/                                       &
    & 0.32109431145360149E-009_NER, 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),   &
    &                                                                       &
    & Cs3p2_1600_set2_para(1:5) =  (/                                       &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                        &
    &                                                                       &
    & Cs3p1_1600_set2_para(1:4) =  (/                                       &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs1p1_1600_set2_para(1:4) =  (/                                       &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs1d2_1600_set2_para(1:4) =  (/                                       &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs3d2_1600_set2_para(1:4) =  (/                                       &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs3d3_1600_set2_para(1:4) =  (/                                       &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/)

  real(NER), parameter ::  &
    & spr1s0_3200_set2_para(1:14) = (/                                      &
    &   0.25530308969449205E+002_NER,   -0.72062514706775244E-001_NER,      &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER/),                         &
    &                                                                       &
    & Cs3s1_3200_set2_para(1:7) =  (/                                       &
    & -1.2736819542172845E-002_NER, 0.0_NER, 0.0_NER, 0.0_NER,              &
    & 0.0_NER, 0.0_NER, 0.0_NER/),                                          &
    &                                                                       &
    & Cs3p0_3200_set2_para(1:5) =  (/                                       &
    & 0.11866368639056420E-007_NER, 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),   &
    &                                                                       &
    & Cs3p2_3200_set2_para(1:5) =  (/                                       &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                        &
    &                                                                       &
    & Cs3p1_3200_set2_para(1:4) =  (/                                       &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs1p1_3200_set2_para(1:4) =  (/                                       &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs1d2_3200_set2_para(1:4) =  (/                                       &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs3d2_3200_set2_para(1:4) =  (/                                       &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs3d3_3200_set2_para(1:4) =  (/                                       &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/)

  real(NER), parameter ::  &
    & spr1s0_350_set2_para(1:14) = (/                                       &
    &   0.42647716568097287E+002_NER,   -0.15817891988985733E+000_NER,      &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER/),                         &
    &                                                                       &
    & Cs3s1_350_set2_para(1:7) =  (/                                        &
    & -5.4945264707960580E-003_NER, 0.0_NER, 0.0_NER, 0.0_NER,              &
    & 0.0_NER, 0.0_NER, 0.0_NER/),                                          &
    &                                                                       &
    & Cs3p0_350_set2_para(1:5) =  (/                                        &
    & 0.28737080761182509E-007_NER, 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),   &
    &                                                                       &
    & Cs3p2_350_set2_para(1:5) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                        &
    &                                                                       &
    & Cs3p1_350_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs1p1_350_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs1d2_350_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs3d2_350_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs3d3_350_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/)

  real(NER), parameter ::  &
    & spr1s0_400_set2_para(1:14) = (/                                       &
    &   0.39275245500005816E+002_NER,   -0.13862263192892474E+000_NER,      &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER,                           &
    &   0.0_NER,                         0.0_NER/),                         &
    &                                                                       &
    & Cs3s1_400_set2_para(1:7) =  (/                                        &
    & -4.5126679738286763E-003_NER, 0.0_NER, 0.0_NER, 0.0_NER,              &
    & 0.0_NER, 0.0_NER, 0.0_NER/),                                          &
    &                                                                       &
    & Cs3p0_400_set2_para(1:5) =  (/                                        &
    & 0.30164174135677295E-007_NER, 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),   &
    &                                                                       &
    & Cs3p2_400_set2_para(1:5) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                        &
    &                                                                       &
    & Cs3p1_400_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs1p1_400_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs1d2_400_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs3d2_400_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/),                                 &
    &                                                                       &
    & Cs3d3_400_set2_para(1:4) =  (/                                        &
    & 0.0_NER, 0.0_NER, 0.0_NER, 0.0_NER/)

! fitting LO by scatlength
! empirical scatlength = -24.740(20)
  real(NER), parameter  ::  &
    & MMWLY1s0_350_para(1:6) = (/                                            &
    & -0.49527443017614902E-002_NER,       -0.11878940324351621E-002_NER,    &
    &  0.23601955598787758E-007_NER,        0.0_NER,                         &
    &  0.0_NER,                             0.0_NER/)

  real(NER), parameter  ::  &
    & MMWLY1s0_400_para(1:6) = (/                                            &
    & -0.46345113373990766E-002_NER,       -0.13812471862152250E-002_NER,    &
    &  0.21086515472408969E-007_NER,        0.0_NER,                         &
    &  0.0_NER,                             0.0_NER/)

  real(NER), parameter  ::  &
    & MMWLY1s0_450_para(1:6) = (/                                            &
    & -0.43891487903421457E-002_NER,       -0.15417924905996663E-002_NER,    &
    &  0.18652151984588734E-007_NER,        0.0_NER,                         &
    &  0.0_NER,                             0.0_NER/)

  real(NER), parameter  ::  &
    & MMWLY1s0_500_para(1:6) = (/                                            &
    & -0.41940507538893472E-002_NER,       -0.16781049700317883E-002_NER,    &
    &  0.16481265591365977E-007_NER,        0.0_NER,                         &
    &  0.0_NER,                             0.0_NER/)

  real(NER), parameter  ::  &
    & MMWLY1s0_600_para(1:6) = (/                                            &
    & -0.39028006210685457E-002_NER,       -0.19095743809213869E-002_NER,    &
    &  0.13059371072198318E-007_NER,        0.0_NER,                         &
    &  0.0_NER,                             0.0_NER/)

  real(NER), parameter  ::  &
    & MMWLY1s0_800_para(1:6) = (/                                            &
    & -0.35387941376915466E-002_NER,       -0.22691807277402604E-002_NER,    &
    &  0.87320693514134414E-008_NER,        0.0_NER,                         &
    &  0.0_NER,                             0.0_NER/)


  real(NER), parameter  ::  &
    & MMWLY1s0_1600_para(1:6) = (/                                           &
    & -0.29771946913657479E-002_NER,       -0.31495635450086061E-002_NER,    &
    &  0.29710850051967965E-008_NER,        0.0_NER,                         &
    &  0.0_NER,                             0.0_NER/)


  real(NER), parameter  ::  &
    & MMWLY1s0_3200_para(1:6) = (/                                           &
    & -0.26722872208465821E-002_NER,       -0.40482615077028763E-002_NER,    &
    &  0.92099418329107802E-009_NER,        0.0_NER,                         &
    &  0.0_NER,                             0.0_NER/)


contains

  subroutine init_PCs4chengdu()

    PC_gA = 1.29_NER
    PC_mN = 939.0_NER
    PC_mpi = 138.0_NER
    PC_fpi = 92.4_NER

  end subroutine init_PCs4chengdu

  subroutine chengdu_dispatch(pot_name)

    character(len = *), intent(in)  :: pot_name

    hotpot_name = pot_name
    hotpot_is_Edep = .false.
    if (pot_name == "chengdu_LBWDIB_600") hotpot_is_Edep = .true.

  end subroutine chengdu_dispatch

  function chengdu_hotpot_is_Edep()

    logical :: chengdu_hotpot_is_Edep

    chengdu_hotpot_is_Edep = hotpot_is_Edep

  end function chengdu_hotpot_is_Edep

  subroutine chengdu_hotpot(L, S, J, uptoQn, p1, p2, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2
    real(NER), intent(out)  :: potval(:, :)

    select case(hotpot_name)
      case ("chengdu_DLSPR")
        call chengdu_DLSPR(L, S, J, uptoQn, p1, p2, potval)
      case ("chengdu_DLSPR_350")
        call chengdu_DLSPR_350(L, S, J, uptoQn, p1, p2, potval)
      case ("chengdu_DLSPR_400")
        call chengdu_DLSPR_400(L, S, J, uptoQn, p1, p2, potval)
      case ("chengdu_DLSPR_800")
        call chengdu_DLSPR_800(L, S, J, uptoQn, p1, p2, potval)
      case ("chengdu_DLSPR_1600")
        call chengdu_DLSPR_1600(L, S, J, uptoQn, p1, p2, potval)
      case ("chengdu_DLSPR_3200")
        call chengdu_DLSPR_3200(L, S, J, uptoQn, p1, p2, potval)
      case ("chengdu_pionless_1000")
        call chengdu_pionless_1000(L, S, J, uptoQn, p1, p2, potval)
      case ("chengdu_MMWLY_350")
        call chengdu_MMWLY_350(L, S, J, uptoQn, p1, p2, potval)
      case ("chengdu_MMWLY_400")
        call chengdu_MMWLY_400(L, S, J, uptoQn, p1, p2, potval)
      case ("chengdu_MMWLY_600")
        call chengdu_MMWLY_600(L, S, J, uptoQn, p1, p2, potval)
      case ("chengdu_MMWLY_800")
        call chengdu_MMWLY_800(L, S, J, uptoQn, p1, p2, potval)
      case ("chengdu_MMWLY_1600")
        call chengdu_MMWLY_1600(L, S, J, uptoQn, p1, p2, potval)
      case ("chengdu_MMWLY_3200")
        call chengdu_MMWLY_3200(L, S, J, uptoQn, p1, p2, potval)
      case default
        write (standard_error_unit, '(a)')  &
          & trim(hotpot_name)//" not recognized"
        potval = 0.0_NER
    end select

  end subroutine chengdu_hotpot

  subroutine chengdu_hotpot_Edep(L, S, J, uptoQn, p1, p2, Ecm, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2, Ecm
    real(NER), intent(out)  :: potval(:, :)

    select case(hotpot_name)
      case ("chengdu_LBWDIB_600")
        call chengdu_LBWDIB_600(L, S, J, uptoQn, p1, p2, Ecm, potval)
      case default
        write (standard_error_unit, '(a)')  &
          & trim(hotpot_name)//" not recognized"
        potval = 0.0_NER
    end select

  end subroutine chengdu_hotpot_Edep

  subroutine init_dlspr_para(para1s0, para3s1, para3p0, para1p1, &
    & para3p1, para3p2, para1d2, para3d2, para3d3)

    real(NER),intent(in) :: para1s0(1:14), para3s1(1:7), para3p0(1:5), &
    & para1p1(1:4), para3p1(1:4), para3p2(1:5), para1d2(1:4),          &
    & para3d2(1:4), para3d3(1:4)

    dlspr1s0 = para1s0
    Cs3s1    = para3s1
    Cs3p0    = para3p0
    Cs3p2    = para3p2
    Cs1p1    = para1p1
    Cs3p1    = para3p1
    Cs1d2    = para1d2
    Cs3d2    = para3d2
    Cs3d3    = para3d3

  end subroutine init_dlspr_para

  subroutine get_chengdu_DLSPR(L, S, J, uptoQn, p1, p2, Mambda, potval)

    integer,intent(in)    :: L, S, J, uptoQn
    real(NER),intent(in)  :: p1, p2, Mambda
    real(NER),intent(out) :: potval(:, :)

    type(lsj_symbol) :: lsj
    integer :: chnid
    real(NER) :: tmppotval(1:2, 1:2)
    real(NER) :: para0(1:1)

    para0 = 0.0_NER
    call init_PCs4chengdu()
    lsj%L = L
    lsj%S = S
    lsj%J = J

    if (.not. is_lsj_valid(lsj)) then
      write (standard_error_unit, '(a)')  &
        & 'get_chengdu_DLSPR : L, S, J invalid'
      return
    end if
    call convert_lsj_to_chnid(lsj, chnid)

    select case(uptoQn)
      case(0)
        select case(chnid)
          case (CHNID_1S0_PP)
            call VLO_sprbl_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,   &
              & dlspr1s0, p1, p2, tmppotval)
          case (CHNID_3S1_PP)
            call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
              & Cs3s1, p1, p2, tmppotval)
          case (CHNID_3P0_PP)
            call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
              & Cs3p0, p1, p2, tmppotval)
          case default
            tmppotval = 0.0_NER
        end select
      case(1)
        select case(chnid)
          case (CHNID_3P1_PP, CHNID_1P1_PP, CHNID_3D3_PP, CHNID_3P2_PP, &
              & CHNID_3D2_PP, CHNID_1D2_PP)
            call OPE_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,         &
              & para0, p1, p2, tmppotval)
          case default
            tmppotval = 0.0_NER
        end select
    end select

    potval(1:2, 1:2) = tmppotval(1:2, 1:2)

  end subroutine get_chengdu_DLSPR

  subroutine init_mmwly_para(mmwlypara, para3s1, para3p0, para1p1, &
    & para3p1, para3p2, para1d2, para3d2, para3d3)

    real(NER), intent(in) :: mmwlypara(1:6), para3s1(1:7), para3p0(1:5), &
    & para1p1(1:4), para3p1(1:4), para3p2(1:5), para1d2(1:4),            &
    & para3d2(1:4), para3d3(1:4)

    mmwly1s0 = mmwlypara
    Cs3s1    = para3s1
    Cs3p0    = para3p0
    Cs3p2    = para3p2
    Cs1p1    = para1p1
    Cs3p1    = para3p1
    Cs1d2    = para1d2
    Cs3d2    = para3d2
    Cs3d3    = para3d3

  end subroutine init_mmwly_para

  subroutine get_chengdu_MMWLY(L, S, J, uptoQn, p1, p2, Mambda, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2, Mambda
    real(NER), intent(out)  :: potval(:, :)

    type(lsj_symbol) :: lsj
    integer :: chnid
    real(NER) :: tmppotval(1:2, 1:2)
    real(NER) :: para0(1:1)

    para0 = 0.0_NER
    call init_PCs4chengdu()
    lsj%L = L
    lsj%S = S
    lsj%J = J

    if (.not. is_lsj_valid(lsj)) then
      write (standard_error_unit, '(a)')  &
        & 'get_chengdu_MMWLY : L, S, J invalid'
      return
    end if
    call convert_lsj_to_chnid(lsj, chnid)

    select case(uptoQn)
      case(0)
        select case(chnid)
          case (CHNID_1S0_PP)
            call VLO_MMWLY_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,   &
              & mmwly1s0, p1, p2, tmppotval)
          case (CHNID_3S1_PP)
            call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
              & Cs3s1, p1, p2, tmppotval)
         case (CHNID_3P0_PP)
            call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
              & Cs3p0, p1, p2, tmppotval)
          case default
            tmppotval = 0.0_NER
        end select
      case(1)
        select case(chnid)
          case (CHNID_1S0_PP)
            call VNLO_MMWLY_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,   &
              & mmwly1s0, p1, p2, tmppotval)
          case (CHNID_3P1_PP, CHNID_1P1_PP, CHNID_3D3_PP, CHNID_3P2_PP, &
              & CHNID_3D2_PP, CHNID_1D2_PP)
            call OPE_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,         &
              & para0, p1, p2, tmppotval)
          case default
            tmppotval = 0.0_NER
        end select
    end select

    potval(1:2, 1:2) = tmppotval(1:2, 1:2)

  end subroutine get_chengdu_MMWLY


  ! Feautures: Deltaless; separable 1s0
  !
  ! All units are in MeV or (MeV)^{-1}
  ! %%%%%%
  ! In
  ! %%%%%%
  ! uptoQn: number of order
  ! L, S, J: quantum # of partial-wave, as in ^{2S+1}L_J
  !   Non-vanishing channel at LO: 1S0(LSJ = 000), 3S1 - 3D1(LSJ = 011)
  !     , 3P0(LSJ = 110)
  !   For coupled channels, L = lower one of two coupled angular momenta
  ! p1, p2: magnitude of outgoing and incoming c.m. momenta
  !
  ! %%%%%%
  ! Out
  ! %%%%%%
  ! potval is the value of partial-wave potentials
  ! uptoQn = 0 ==> VLO
  ! uptoQn = 1 ==> VNLO (incremental value)
  ! Storage of potval:
  !   2-by-2 matrix
  !   For uncoupled channels, e.g., 1S0(LSJ = 000) or 3P0(LSJ = 110)
  !     potval(1, 1) = <L, p1 | V | L, p2 > and (1, 2), (2, 1), and (2, 2) are
  !     zero
  !   For coupled channels, e.g., 3S1 - 3D1 (LSJ = 011)
  !     potval(1, 1) = < L, p1   | V | L, p2 >
  !     potval(1, 2) = < L, p1   | V | L+2, p2 >
  !     potval(2, 1) = < L+2, p1 | V | L, p2 >
  !     potval(2, 2) = < L+2, p1 | V | L+2, p2 >
  !
  ! Remarks:
  ! * Normalization of V can be explained as follows.
  ! If V were so weak as to validate the Born approximation, the relation of
  ! V in uncoupled channels and the respective scattering length 'a' would have
  ! been
  ! < L, p | V | L, p> = 2/pi * a + ... for p -> 0

  subroutine chengdu_DLSPR_350(L, S, J, uptoQn, p1, p2, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2
    real(NER), intent(out)  :: potval(:, :)

    type(lsj_symbol) :: lsj
    integer :: chnid
    real(NER) :: tmppotval(1:2, 1:2), Mambda

    Mambda = 350.0_NER
    call init_dlspr_para(spr1s0_350_set2_para, Cs3s1_350_set2_para, Cs3p0_350_set2_para,     &
      & Cs1p1_350_set2_para, Cs3p1_350_set2_para, Cs3p2_350_set2_para, Cs1d2_350_set2_para   &
      & , Cs3d2_350_set2_para, Cs3d3_350_set2_para)
    call get_chengdu_DLSPR(L, S, J, uptoQn, p1, p2, Mambda, potval)

  end subroutine chengdu_DLSPR_350

  subroutine chengdu_DLSPR_400(L, S, J, uptoQn, p1, p2, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2
    real(NER), intent(out)  :: potval(:, :)

    type(lsj_symbol) :: lsj
    integer :: chnid
    real(NER) :: tmppotval(1:2, 1:2), Mambda

!     call init_PCs4chengdu()
!     lsj%L = L
!     lsj%S = S
!     lsj%J = J

!     if (.not. is_lsj_valid(lsj)) then
!       write (standard_error_unit, '(a)')  &
!         & 'chengdu_DLSPR_400 : L, S, J invalid'
!       return
!     end if
!     call convert_lsj_to_chnid(lsj, chnid)

    Mambda = 400.0_NER
    call init_dlspr_para(spr1s0_400_set2_para, Cs3s1_400_set2_para, Cs3p0_400_set2_para,     &
      & Cs1p1_400_set2_para, Cs3p1_400_set2_para, Cs3p2_400_set2_para, Cs1d2_400_set2_para   &
      & , Cs3d2_400_set2_para, Cs3d3_400_set2_para)
    call get_chengdu_DLSPR(L, S, J, uptoQn, p1, p2, Mambda, potval)
!     select case(chnid)
!       case (CHNID_1S0_PP)
!         call VLO_sprbl_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,   &
!           & spr1s0_400_set2_para, p1, p2, tmppotval)
!       case (CHNID_3S1_PP)
!         call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
!           & Cs3s1_400_set2_para, p1, p2, tmppotval)
!       case (CHNID_3P0_PP)
!         call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
!           & Cs3p0_400_set2_para, p1, p2, tmppotval)
!       case default
!         tmppotval = 0.0_NER
!     end select

!     potval(1:2, 1:2) = tmppotval(1:2, 1:2)

  end subroutine chengdu_DLSPR_400

  subroutine chengdu_DLSPR(L, S, J, uptoQn, p1, p2, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2
    real(NER), intent(out)  :: potval(:, :)

    type(lsj_symbol) :: lsj
    integer :: chnid
    real(NER) :: tmppotval(1:2, 1:2), Mambda

    Mambda = 600.0_NER
    call init_dlspr_para(spr1s0_600_set2_para, Cs3s1_600_set2_para, Cs3p0_600_set2_para,   &
      & Cs1p1_600_set2_para, Cs3p1_600_set2_para, Cs3p2_600_set2_para, Cs1d2_600_set2_para &
      & , Cs3d2_600_set2_para, Cs3d3_600_set2_para)
    call get_chengdu_DLSPR(L, S, J, uptoQn, p1, p2, Mambda, potval)

  end subroutine chengdu_DLSPR

  subroutine chengdu_DLSPR_800(L, S, J, uptoQn, p1, p2, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2
    real(NER), intent(out)  :: potval(:, :)

    type(lsj_symbol) :: lsj
    integer :: chnid
    real(NER) :: tmppotval(1:2, 1:2), Mambda

    Mambda = 800.0_NER
    call init_dlspr_para(spr1s0_800_set2_para, Cs3s1_800_set2_para, Cs3p0_800_set2_para,   &
      & Cs1p1_800_set2_para, Cs3p1_800_set2_para, Cs3p2_800_set2_para, Cs1d2_800_set2_para &
      & , Cs3d2_800_set2_para, Cs3d3_800_set2_para)
    call get_chengdu_DLSPR(L, S, J, uptoQn, p1, p2, Mambda, potval)

  end subroutine chengdu_DLSPR_800

  subroutine chengdu_DLSPR_1600(L, S, J, uptoQn, p1, p2, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2
    real(NER), intent(out)  :: potval(:, :)

    type(lsj_symbol) :: lsj
    integer :: chnid
    real(NER) :: tmppotval(1:2, 1:2), Mambda

    Mambda = 1600.0_NER
    call init_dlspr_para(spr1s0_1600_set2_para, Cs3s1_1600_set2_para, Cs3p0_1600_set2_para,     &
      & Cs1p1_1600_set2_para, Cs3p1_1600_set2_para, Cs3p2_1600_set2_para, Cs1d2_1600_set2_para  &
      & , Cs3d2_1600_set2_para, Cs3d3_1600_set2_para)
    call get_chengdu_DLSPR(L, S, J, uptoQn, p1, p2, Mambda, potval)

  end subroutine chengdu_DLSPR_1600

  subroutine chengdu_DLSPR_3200(L, S, J, uptoQn, p1, p2, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2
    real(NER), intent(out)  :: potval(:, :)

    type(lsj_symbol) :: lsj
    integer :: chnid
    real(NER) :: tmppotval(1:2, 1:2), Mambda

    Mambda = 3200.0_NER
    call init_dlspr_para(spr1s0_3200_set2_para, Cs3s1_3200_set2_para, Cs3p0_3200_set2_para,     &
      & Cs1p1_3200_set2_para, Cs3p1_3200_set2_para, Cs3p2_3200_set2_para, Cs1d2_3200_set2_para  &
      & , Cs3d2_3200_set2_para, Cs3d3_3200_set2_para)
    call get_chengdu_DLSPR(L, S, J, uptoQn, p1, p2, Mambda, potval)

  end subroutine chengdu_DLSPR_3200

  subroutine chengdu_pionless_1000(L, S, J, uptoQn, p1, p2, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2
    real(NER), intent(out)  :: potval(:, :)

    type(lsj_symbol) :: lsj
    integer :: chnid
    real(NER) :: tmppotval(1:2, 1:2), Mambda
    real(NER) :: pionlesspara1s0(1:1), pionlesspara3s1(1:1)

    call init_PCs4chengdu()
    lsj%L = L
    lsj%S = S
    lsj%J = J

    if (.not. is_lsj_valid(lsj)) then
      write (standard_error_unit, '(a)')  &
        & 'chengdu_pionless_1000 : L, S, J invalid'
      return
    end if
    call convert_lsj_to_chnid(lsj, chnid)

    pionlesspara1s0(1) =  -1.56315E-003_NER   ! Fitted to a = -23.7 fm

    ! pionlesspara3s1(1) =  -1.8275E-003_NER    ! Fitted to a = +5.42 fm
    pionlesspara3s1(1) =  -1.7862E-003_NER    ! Fitted to Bd = 2.22 MeV

    Mambda = 1000.0_NER
    select case(chnid)
      case (CHNID_1S0_PP)
        call VLO_withc0_pionless_NP_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,   &
          & pionlesspara1s0, p1, p2, tmppotval)
      case (CHNID_3S1_PP)
        call VLO_withc0_pionless_NP_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,   &
          & pionlesspara3s1, p1, p2, tmppotval)
      case default
        tmppotval = 0.0_NER
    end select

    potval(1:2, 1:2) = tmppotval(1:2, 1:2)

  end subroutine chengdu_pionless_1000

  subroutine chengdu_LBWDIB_600(L, S, J, uptoQn, p1, p2, Ecm, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2, Ecm
    real(NER), intent(out)  :: potval(:, :)

    type(lsj_symbol) :: lsj
    integer :: chnid
    real(NER) :: tmppotval(1:2, 1:2), Mambda, para(0:20)

    call init_PCs4chengdu()
    lsj%L = L
    lsj%S = S
    lsj%J = J

    if (.not. is_lsj_valid(lsj)) then
      write (standard_error_unit, '(a)')  &
        & 'chengdu_LBWDIB_600 : L, S, J invalid'
      return
    end if
    call convert_lsj_to_chnid(lsj, chnid)

    para(0) = Ecm
    para(1) =  0.52106222046242991E+002_NER
    para(2) = -0.82992540492249800E-001_NER

    Mambda = 600.0_NER
    select case(chnid)
      case (CHNID_1S0_PP)
        call VLO_LBWDIB_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
          & para, p1, p2, tmppotval)
      case (CHNID_3S1_PP)
        call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
          & Cs3s1_600_set2_para, p1, p2, tmppotval)
      case (CHNID_3P0_PP)
        call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
          & Cs3p0_600_set2_para, p1, p2, tmppotval)
      case default
        tmppotval = 0.0_NER
    end select

    potval(1:2, 1:2) = tmppotval(1:2, 1:2)

  end subroutine chengdu_LBWDIB_600

  subroutine chengdu_MMWLY_350(L, S, J, uptoQn, p1, p2, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2
    real(NER), intent(out)  :: potval(:, :)

    type(lsj_symbol) :: lsj
    integer :: chnid
    real(NER) :: tmppotval(1:2, 1:2), Mambda

    Mambda = 350.0_NER
    call init_mmwly_para(MMWLY1s0_350_para, Cs3s1_350_set2_para,               &
      & Cs3p0_350_set2_para, Cs1p1_350_set2_para, Cs3p1_350_set2_para,         &
      & Cs3p2_350_set2_para, Cs1d2_350_set2_para, Cs3d2_350_set2_para,         &
      & Cs3d3_350_set2_para)
    call get_chengdu_MMWLY(L, S, J, uptoQn, p1, p2, Mambda, potval)

  end subroutine chengdu_MMWLY_350

  subroutine chengdu_MMWLY_400(L, S, J, uptoQn, p1, p2, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2
    real(NER), intent(out)  :: potval(:, :)

    type(lsj_symbol) :: lsj
    integer :: chnid
    real(NER) :: tmppotval(1:2, 1:2), Mambda

!     call init_PCs4chengdu()
!     lsj%L = L
!     lsj%S = S
!     lsj%J = J

!     if (.not. is_lsj_valid(lsj)) then
!       write (standard_error_unit, '(a)')  &
!         & 'chengdu_MMWLY_400 : L, S, J invalid'
!       return
!     end if
!     call convert_lsj_to_chnid(lsj, chnid)

    Mambda = 400.0_NER
    call init_mmwly_para(MMWLY1s0_400_para, Cs3s1_400_set2_para, Cs3p0_400_set2_para,       &
      & Cs1p1_400_set2_para, Cs3p1_400_set2_para, Cs3p2_400_set2_para, Cs1d2_400_set2_para  &
      & , Cs3d2_400_set2_para, Cs3d3_400_set2_para)
    call get_chengdu_MMWLY(L, S, J, uptoQn, p1, p2, Mambda, potval)
!     select case(uptoQn)
!       case(0)
!         select case(chnid)
!           case (CHNID_1S0_PP)
!             call VLO_MMWLY_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,   &
!               & MMWLY1s0_400_para, p1, p2, tmppotval)
!           case (CHNID_3S1_PP)
!             call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
!               & Cs3s1_400_set2_para, p1, p2, tmppotval)
!          case (CHNID_3P0_PP)
!             call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
!               & Cs3p0_400_set2_para, p1, p2, tmppotval)
!           case default
!             tmppotval = 0.0_NER
!         end select
!       case(1)
!         select case(chnid)
!           case (CHNID_1S0_PP)
!             call VNLO_MMWLY_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,   &
!               & MMWLY1s0_400_para, p1, p2, tmppotval)
!           case default
!             tmppotval = 0.0_NER
!         end select
!     end select

!     potval(1:2, 1:2) = tmppotval(1:2, 1:2)

  end subroutine chengdu_MMWLY_400

  subroutine chengdu_MMWLY_600(L, S, J, uptoQn, p1, p2, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2
    real(NER), intent(out)  :: potval(:, :)

    type(lsj_symbol) :: lsj
    integer :: chnid
    real(NER) :: tmppotval(1:2, 1:2), Mambda

!     call init_PCs4chengdu()
!     lsj%L = L
!     lsj%S = S
!     lsj%J = J

!     if (.not. is_lsj_valid(lsj)) then
!       write (standard_error_unit, '(a)')  &
!         & 'chengdu_MMWLY_600 : L, S, J invalid'
!       return
!     end if
!     call convert_lsj_to_chnid(lsj, chnid)

    Mambda = 600.0_NER
    call init_mmwly_para(MMWLY1s0_600_para, Cs3s1_600_set2_para, Cs3p0_600_set2_para,       &
      & Cs1p1_600_set2_para, Cs3p1_600_set2_para, Cs3p2_600_set2_para, Cs1d2_600_set2_para  &
      & , Cs3d2_600_set2_para, Cs3d3_600_set2_para)
    call get_chengdu_MMWLY(L, S, J, uptoQn, p1, p2, Mambda, potval)
!     select case(chnid)
!       case (CHNID_1S0_PP)
!         call VLO_MMWLY_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,   &
!           & MMWLY1s0_600_para, p1, p2, tmppotval)
!       case (CHNID_3S1_PP)
!         call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
!           & Cs3s1_600_set2_para, p1, p2, tmppotval)
!       case (CHNID_3P0_PP)
!         call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
!           & Cs3p0_600_set2_para, p1, p2, tmppotval)
!       case default
!         tmppotval = 0.0_NER
!     end select

!     potval(1:2, 1:2) = tmppotval(1:2, 1:2)

  end subroutine chengdu_MMWLY_600

  subroutine chengdu_MMWLY_800(L, S, J, uptoQn, p1, p2, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2
    real(NER), intent(out)  :: potval(:, :)

    type(lsj_symbol) :: lsj
    integer :: chnid
    real(NER) :: tmppotval(1:2, 1:2), Mambda

!     call init_PCs4chengdu()
!     lsj%L = L
!     lsj%S = S
!     lsj%J = J

!     if (.not. is_lsj_valid(lsj)) then
!       write (standard_error_unit, '(a)')  &
!         & 'chengdu_MMWLY_800 : L, S, J invalid'
!       return
!     end if
!     call convert_lsj_to_chnid(lsj, chnid)

    Mambda = 800.0_NER
    call init_mmwly_para(MMWLY1s0_800_para, Cs3s1_800_set2_para, Cs3p0_800_set2_para,       &
      & Cs1p1_800_set2_para, Cs3p1_800_set2_para, Cs3p2_800_set2_para, Cs1d2_800_set2_para  &
      & , Cs3d2_800_set2_para, Cs3d3_800_set2_para)
    call get_chengdu_MMWLY(L, S, J, uptoQn, p1, p2, Mambda, potval)
!     select case(chnid)
!       case (CHNID_1S0_PP)
!         call VLO_MMWLY_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,   &
!           & MMWLY1s0_800_para, p1, p2, tmppotval)
!       case (CHNID_3S1_PP)
!         call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
!           & Cs3s1_800_set2_para, p1, p2, tmppotval)
!       case (CHNID_3P0_PP)
!         call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
!           & Cs3p0_800_set2_para, p1, p2, tmppotval)
!       case default
!         tmppotval = 0.0_NER
!     end select

!     potval(1:2, 1:2) = tmppotval(1:2, 1:2)

  end subroutine chengdu_MMWLY_800

  subroutine chengdu_MMWLY_1600(L, S, J, uptoQn, p1, p2, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2
    real(NER), intent(out)  :: potval(:, :)

    type(lsj_symbol) :: lsj
    integer :: chnid
    real(NER) :: tmppotval(1:2, 1:2), Mambda

!     call init_PCs4chengdu()
!     lsj%L = L
!     lsj%S = S
!     lsj%J = J

!     if (.not. is_lsj_valid(lsj)) then
!       write (standard_error_unit, '(a)')  &
!         & 'chengdu_MMWLY_1600 : L, S, J invalid'
!       return
!     end if
!     call convert_lsj_to_chnid(lsj, chnid)

    Mambda = 1600.0_NER
    call init_mmwly_para(MMWLY1s0_1600_para, Cs3s1_1600_set2_para, Cs3p0_1600_set2_para,        &
      & Cs1p1_1600_set2_para, Cs3p1_1600_set2_para, Cs3p2_1600_set2_para, Cs1d2_1600_set2_para  &
      & , Cs3d2_1600_set2_para, Cs3d3_1600_set2_para)
    call get_chengdu_MMWLY(L, S, J, uptoQn, p1, p2, Mambda, potval)
!     select case(chnid)
!       case (CHNID_1S0_PP)
!         call VLO_MMWLY_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,   &
!           & MMWLY1s0_1600_para, p1, p2, tmppotval)
!       case (CHNID_3S1_PP)
!         call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
!           & Cs3s1_1600_set2_para, p1, p2, tmppotval)
!       case (CHNID_3P0_PP)
!         call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
!           & Cs3p0_1600_set2_para, p1, p2, tmppotval)
!       case default
!         tmppotval = 0.0_NER
!     end select

!     potval(1:2, 1:2) = tmppotval(1:2, 1:2)

  end subroutine chengdu_MMWLY_1600

  subroutine chengdu_MMWLY_3200(L, S, J, uptoQn, p1, p2, potval)

    integer, intent(in)     :: L, S, J, uptoQn
    real(NER), intent(in)   :: p1, p2
    real(NER), intent(out)  :: potval(:, :)

    type(lsj_symbol) :: lsj
    integer :: chnid
    real(NER) :: tmppotval(1:2, 1:2), Mambda

!     call init_PCs4chengdu()
!     lsj%L = L
!     lsj%S = S
!     lsj%J = J

!     if (.not. is_lsj_valid(lsj)) then
!       write (standard_error_unit, '(a)')  &
!         & 'chengdu_MMWLY_3200 : L, S, J invalid'
!       return
!     end if
!     call convert_lsj_to_chnid(lsj, chnid)

    Mambda = 3200.0_NER
    call init_mmwly_para(MMWLY1s0_3200_para, Cs3s1_3200_set2_para, Cs3p0_3200_set2_para,        &
      & Cs1p1_3200_set2_para, Cs3p1_3200_set2_para, Cs3p2_3200_set2_para, Cs1d2_3200_set2_para  &
      & , Cs3d2_3200_set2_para, Cs3d3_3200_set2_para)
    call get_chengdu_MMWLY(L, S, J, uptoQn, p1, p2, Mambda, potval)
!     select case(chnid)
!       case (CHNID_1S0_PP)
!         call VLO_MMWLY_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,   &
!           & MMWLY1s0_3200_para, p1, p2, tmppotval)
!       case (CHNID_3S1_PP)
!         call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
!           & Cs3s1_3200_set2_para, p1, p2, tmppotval)
!       case (CHNID_3P0_PP)
!         call VLO_withc0_epwrap(L, S, J, CHENGDU_REGTYPE, Mambda,  &
!           & Cs3p0_3200_set2_para, p1, p2, tmppotval)
!       case default
!         tmppotval = 0.0_NER
!     end select

!     potval(1:2, 1:2) = tmppotval(1:2, 1:2)

  end subroutine chengdu_MMWLY_3200

end module chengdu
