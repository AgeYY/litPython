# My Spacemacs can run python programme dirctly with hot key SPC-m-c-c. This feature is very convenient. However, it is troublesome to read PYTHONPATH. Therefore this file is used to set path to all python file, using sys.path. If you only run python file in shell, you can write a path.sh which includes all paths, and run it.
import sys
import os

pwd = os.path.dirname(__file__)
sys.path.append(pwd+"/src/core/general")
sys.path.append(pwd+"/src/core/chiral/")
sys.path.append(pwd+"/src/core/pionless/")
sys.path.append(pwd+"/src/tool/")
sys.path.append(pwd+"/src/exe/")
