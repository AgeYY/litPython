# use FFT to smooth curve
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import torch

def signal(x, s = [0, 1], f = [5, 20]):
    #return np.exp(-x**2 / 4) + 0.1 * np.sin(x) + s[0] * np.sin(f[0]*x) + s[1] * np.sin(f[1]*x)
    return np.sin(2 * np.pi * x) + s[0] * np.sin(2 * np.pi* f[0]*x) + s[1] * np.sin(2 * np.pi *f[1]*x)

def fftFilter(y):
    # FFT filter
    ybar = np.fft.rfft(y)
    freq = np.fft.rfftfreq(len(x), d = (x[-1] - x[0]) / x.size)
    #plt.figure(0)
    #plt.scatter(freq, ybar.real)
    #plt.scatter(freq, ybar.imag)
    #plt.show()
    ybar[freq > 1.3] = 0
    yFit = np.fft.irfft(ybar)
    return yFit

def butterFilter(y):
    b, a = signal.butter(8, 0.2, 'lowpass')  
    yFit = signal.filtfilt(b, a, y)
    return yFit

def winFilter(y):
    win = torch.hann_window(10, periodic = False)
    print(win)
    #return yFit
    return 0


x = np.linspace(-5, 5, 100)
y = signal(x, s = [1, 0], f = [7, 0])
yOri = signal(x, s = [0, 0])
#yFit = fftFilter(y)
yFit = winFilter(y)

plt.figure(0)
plt.plot(x, yOri, label = 'original')
plt.plot(x, y, label = 'with noise')
plt.plot(x, yFit, label = 'fitted')
plt.legend()
plt.show()

