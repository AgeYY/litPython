import torch as tch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np

class Net(nn.Module):
    def __init__(self, sizeIn, sizeOut):
        super(Net, self).__init__()
        sizeMd = 50
        self.fc1 = nn.Linear(sizeIn, sizeMd)
        self.fc2 = nn.Linear(sizeMd, sizeMd)
        self.fc3 = nn.Linear(sizeMd, sizeMd)
        self.fc4 = nn.Linear(sizeMd, sizeOut)

    def changeSize(self, sizeIn, sizeOut):
        self.__init__(sizeIn, sizeOut)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = self.fc4(x)
        return x

from scipy import signal
class InvLoss(nn.Module):
    def __init__(self, xpMesh, xrMesh, wrMesh, sigmaI, mambda = 0):
        super().__init__()
        self.xrMesh = xrMesh
        self.kMat = self.__kenel(xpMesh, xrMesh, wrMesh, sigmaI)
        self.mambda = mambda
    def forward(self, rMesh, pMesh):
        rMeshbar = tch.rfft(rMesh, signal_ndim = 1)
        rMeshbar[10:] = 0
        rMesh = tch.irfft(rMeshbar, signal_ndim = 1)
        rMesh = rMesh[1:]
        loss = tch.norm(tch.matmul(self.kMat, rMesh) - pMesh)
        #+ self.mambda * tch.norm(rMeshbar[10:])
        return loss
    def __kenel(self, sigmaR, omega, wOmega, sigmaI):
        self.kMat = tch.empty((len(sigmaR), len(omega)))
        for i in range(self.kMat.shape[0]):
            self.kMat[i, :] = 1 / ( (omega - sigmaR[i])**2 + sigmaI**2 ) * wOmega
        return self.kMat
