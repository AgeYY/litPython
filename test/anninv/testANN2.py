# test the second method of ANN
import testCOMMON
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import src.tool.inout as ios # read csv from input.csv
import src.tool.plotTool as ptt # plot tool
import src.core.general.constant as const
import litInversion as litinv
from scipy.interpolate import interp1d
import myMath as mymath
from src.core.general.novelInv import NInv as ninv
import netWork
import torch
import torch.optim as optim

path = './caphi.csv'
func = ios.readFunc(path, 'r')

def sumCap(func, key, idx):
    # Sum over the first three caphi. key = 'x', or 'y'
    out = np.array(func[idx[0]][key])
    out = out + np.array(func[idx[1]][key])
    out = out + np.array(func[idx[2]][key])
    return out

# Parameter
sigmaI = [[], []] 
mambda = [[], []] 
sigmaI[0] = func[0]['sigmaIMeVChi'][0] / const.HBarC
sigmaI[1] = func[-1]['sigmaIMeVChi'][0] / const.HBarC
mambda[0] = func[0]['mambdaMeV'][0] / const.HBarC
mambda[1] = func[-1]['mambdaMeV'][0] / const.HBarC

# function
caphi = [[], []] # caphi[0] the first caphi with parameter1
sigmaR = [[], []] # same above
sigmaR[0] = np.array(func[0]['x'])
sigmaR[1] = np.array(func[1]['x'])
caphi[0] = sumCap(func, 'y', [0, 1, 2])
caphi[1] = sumCap(func, 'y', [3, 4, 5])
# interpolate to obtain more caphi data
nSigmaR = 100 # new sigmaR Mesh
caphiInter = [[], []]
caphiInter[0] = interp1d(sigmaR[0], caphi[0], kind='cubic')
caphiInter[1] = interp1d(sigmaR[1], caphi[1], kind='cubic')
sigmaR[0] = np.linspace(sigmaR[0][0], sigmaR[0][-1], nSigmaR)
sigmaR[1] = np.linspace(sigmaR[1][0], sigmaR[1][-1], nSigmaR)
caphi[0] = caphiInter[0](sigmaR[0])
caphi[1] = caphiInter[1](sigmaR[1])

nOmega = 300 # set the output omega mesh for the response function
omegaMin = const.ETH
omegaMax = sigmaR[0][-1]
omega = np.linspace(omegaMin, omegaMax, nOmega)
wOmega = np.ones(nOmega) * (omegaMax - omegaMin) / nOmega

#sigmaR[0], omega, wOmega, caphi[0] = torch.from_numpy(sigmaR[0]), torch.from_numpy(omega), torch.from_numpy(wOmega), torch.from_numpy(caphi[0]).float()
sigmaR[1], omega, wOmega, caphi[1] = torch.from_numpy(sigmaR[1]), torch.from_numpy(omega), torch.from_numpy(wOmega), torch.from_numpy(caphi[1]).float()
nSteps = 3000
net = netWork.Net(nSigmaR, nOmega)
#criterion = netWork.InvLoss(sigmaR[0], omega, wOmega, sigmaI[0], mambda = 10)
criterion = netWork.InvLoss(sigmaR[1], omega, wOmega, sigmaI[1], mambda = 10)
optimizer = optim.Adam(net.parameters(), lr=0.0001)
running_loss = np.zeros(nSteps)
for i in range(nSteps):
    # zero the parameter gradients
    optimizer.zero_grad()
    #outputs = net(caphi[0])
    outputs = net(caphi[1])
    #loss = criterion(outputs, caphi[0])
    loss = criterion(outputs, caphi[1])
    loss.backward()
    optimizer.step()
    # print statistics
    running_loss[i] = loss.item()

plt.figure(0)
plt.plot(range(nSteps), running_loss)
plt.show()
########## response
rMeshbar = torch.rfft(outputs.detach(), signal_ndim = 1)
plt.figure(42)
plt.plot(rMeshbar)
plt.show()
rMeshbar[10:] = 0
rMesh = torch.irfft(rMeshbar, signal_ndim = 1)
plt.figure(0)
plt.plot(omega, rMesh[1:] / 193, label = 'response')
plt.legend()
############ response
#plt.figure(0)
#plt.plot(omega, outputs.detach()/ 193, label = 'response')
#plt.legend()

#rMeshbar = torch.rfft(outputs.detach(), signal_ndim = 1)
#plt.figure(42)
#plt.plot(rMeshbar)
#plt.show()
#rMeshbar[10:] = 0
#rMesh = torch.irfft(rMeshbar, signal_ndim = 1)
#plt.figure(42)
#plt.plot(outputs.detach(), label='orginal')
#plt.plot(rMesh, label='after filter')
#plt.legend()
#plt.show()
# recover caphi
#rMesh = outputs.detach()
caphiRe = torch.matmul(criterion.kMat, rMesh[1:])
plt.figure(1)
plt.plot(sigmaR[0], caphiRe, label = 'caphi recoverd')
plt.plot(sigmaR[0], caphi[0], label = 'caphi')
plt.legend()
plt.show()

# save
PATH = './invNet2.pth'
torch.save(net.state_dict(), PATH)
