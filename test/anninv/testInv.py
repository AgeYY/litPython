# use ANN to invert \Phi
import torch as torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import matplotlib.pyplot as plt
import trainDataGen as tData
from netWork import Net

# test Data
npMesh, nrMesh = 50, 50
lrx, urx, lpx, upx = 0, 2, 0, 2
dtgen = tData.dataGen(npMesh, nrMesh, lrx, urx, lpx, upx)
dtgen.setRandFunc(tData.randGaussian)
xpMesh, pMesh, xrMesh, rMesh, kMat = dtgen.genData(1)
rMesh = torch.from_numpy(np.array(rMesh)).float()
pMesh = torch.from_numpy(np.array(pMesh)).float()
nXMesh = len(xpMesh)

PATH = './invNet.pth'
net = Net(nXMesh, nXMesh)
net.load_state_dict(torch.load(PATH))
out = net(pMesh)
plt.figure(0)
plt.plot(xrMesh, rMesh[0], label = 'original response')
plt.plot(xrMesh, out[0].detach(), label = 'ANN result')
plt.legend()
plt.show()
