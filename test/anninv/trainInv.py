# training ANN to invert \Phi
import torch as tch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import matplotlib.pyplot as plt
import trainDataGen as tData
from netWork import Net

# genrate Data
npMesh, nrMesh = 50, 50
lrx, urx, lpx, upx = 0, 2, 0, 2
dtgen = tData.dataGen(npMesh, nrMesh, lrx, urx, lpx, upx)
dtgen.setRandFunc(tData.randGaussian)
xpMesh, pMesh, xrMesh, rMesh, kMat = dtgen.genData(1000)

nXMesh = len(xpMesh)
rMesh = tch.from_numpy(np.array(rMesh)).float()
pMesh = tch.from_numpy(np.array(pMesh)).float()
#print(rMesh, pMesh)
# training
nSteps = 5000
net = Net(nXMesh, nXMesh)
criterion = nn.MSELoss()
#optimizer = optim.SGD(net.parameters(), lr=0.01, momentum=0.9, dampening = 0.1)
optimizer = optim.Adam(net.parameters(), lr=0.0001, weight_decay = 0.1)
running_loss = np.zeros(nSteps)
for i in range(nSteps):
    # zero the parameter gradients
    optimizer.zero_grad()
    outputs = net(pMesh)
    loss = criterion(outputs, rMesh)
    loss.backward()
    optimizer.step()
    # print statistics
    running_loss[i] = loss.item()

plt.figure(0)
plt.plot(range(nSteps), running_loss)
plt.show()

# test
out = net(pMesh[2])
plt.figure(0)
plt.plot(xrMesh, out.detach(), label = 'ANN')
plt.plot(xrMesh, rMesh[2], label = 'Original Response')
plt.legend()
plt.show()

# save
PATH = './invNet.pth'
tch.save(net.state_dict(), PATH)

