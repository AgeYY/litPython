# use ANN to invert \Phi
import numpy as np

def kernel(sigmaR, omega, wOmega, sigmaI = 1):
    kMat = np.empty((sigmaR.size, omega.size))
    for i in range(kMat.shape[0]):
        kMat[i, :] = 1 / ( (omega - sigmaR[i])**2 + sigmaI**2 ) * wOmega
    return kMat
class dataGen():
    def __init__(self, npMesh, nrMesh, lrx, urx, lpx, upx):
        '''
        nrmesh, npMesh: number of points in each r function and p function
        lr, ur, lp, up: lower and upper boundaries for the x ranges of r function and p function

        '''
        self.npMesh, self.nrMesh = npMesh, nrMesh
        self.lrx, self.urx, self.lpx, self.upx = lrx, urx, lpx, upx
    def genData(self, nSample):
        # Generate data
        
        xpMesh = np.linspace(self.lrx, self.urx, self.npMesh) # xMesh for pMesh
        xrMesh = np.linspace(self.lpx, self.upx, self.nrMesh) # xMesh for response
        wrMesh = (self.urx - self.lrx) / self.nrMesh # integral weight
        kMat = kernel(xpMesh, xrMesh, wrMesh)

        rMesh = [] # response function
        pMesh = [] # phi function
        for i in range(nSample):
            r = self.randFunc(xrMesh)
            p = kMat.dot(r)
            rMesh.append(r) # append r and phi
            pMesh.append(p) # append r and phi
        return xpMesh, pMesh, xrMesh, rMesh, kMat

    def setRandFunc(self, func = None):
        # set the function which randomly generate rMesh, with input xrMesh
        if func is None: # default randfunc
            def randFunc(xrMesh):
                an = np.random.rand(3)
                poly = np.poly1d(an) # generate a new function
                r = poly(xrMesh)
                return r
            self.randFunc = randFunc
        else:
            self.randFunc = func

HBarC = 193
# define a gaussian random function on your own
def randGaussian(xrMesh, sigmaI = 5):
    # generate random gaussian function.
    cMin, cMax = 0, 10 / (sigmaI / HBarC)**2
    sigmaMin, sigmaMax = 0, 2 / 8
    c, sigma = np.random.rand(2)
    c = c * (cMax - cMin) + cMin
    sigma = sigma * (sigmaMax - sigmaMin) + sigmaMin
    mu = np.random.normal(loc = 0.7, scale = 0.3, size = 1)
    gau = np.exp(- (xrMesh - mu)**2 / 2 / sigma**2) / np.sqrt(2 * np.pi) / sigma * c
    return gau

#import matplotlib.pyplot as plt
#x = np.linspace(0, 2, 50)
#y = randGaussian(x)
#plt.figure(0)
#plt.plot(x, y)
#plt.show()
