# testResChi.py
# Zeyuan Ye, Feb. 10, 2020

# Test for resChi.py

import testCOMMON
import src.core.chiral.resChi as resChi
import numpy as np
import pandas as pd
import src.core.general.constant as const
from matplotlib import pyplot as plt

########## Input ##########
'''For cutoff = 400, 600, nGau = 130, bGauR = 150 / mambda. For cutoff = 1600, nGau = 200, bGauR = 350 / mambda'''
mambda = 600 / const.HBarC # Cut off
nGau = 130 # number of mesh points for wavefunction and potential in the coordinate space and momentum space
aGau, bGau = 0, 3 * mambda # boundary of pMesh in momentum space
aGauR, bGauR = 0 / mambda, 150 / mambda # boundary of pMesh in momentum space
qChar = np.sqrt(5) # momentum transfer
sigmaI = 5 / const.HBarC # See Efro1993
sigmaRMesh = np.linspace(3 / const.HBarC, 250 / const.HBarC, 50) # See Efro1993
para = [] # Para for potential
rMeshResponse = np.linspace(3 / const.HBarC, 250 / const.HBarC, 100) # unit = fm. xMesh for response function
checkOpt = 'on' # Check if the response is fitted well
checkN = 20 # Number of mesh points for checking, 20 is good enough
omegaMax = 500 / const.HBarC # This can be read off from the base function.
lMin = 0 # This code is robust enough. lMin must be 0 now.
lMax = 13
#################### These parameters are for the response function who is the inversion of (j, L, l) = (1, 0, 0)
paraInv0 = 10 # para for inversion
nMax0 = 10 # Maximum fiiting order of the inversion
base0 = 'base1' # fitting base for inversion. 'base2' for base2 and other string for base1.
fitMethod0 = 'smart'
#################### These parameters are for the response function who is the inversion of (j, L, l) = (1, 0, 2)
paraInv2 = 17 # para for inversion
nMax2 = 10 # Maximum fiiting order of the inversion
base2 = 'base1' # fitting base for inversion. 'base2' for base2 and other string for base1.
fitMethod2 = 'smart'
#################### These parameters are the response function who is the inversion of the rest of channels
paraInvL = 10 # para for inversion
nMaxL = 10 # Maximum fiiting order of the inversion
checkOptL = 'on' # Check if the response is fitted well
baseL = 'base2' # fitting base for inversion. 'base2' for base2 and other string for base1.
fitMethodL = 'normalEq'
########### Check the best fit for L > 0 ##########
resSolver = resChi.resChi()
(caphi0, caphi2, caphiL) = resSolver.resCaphi(mambda, nGau, aGau, bGau, aGauR, bGauR, qChar, sigmaI, sigmaRMesh, lMin, lMax) # This would check the fitting status of L = 0

(rMeshResponse, response0) = resSolver.invCaphi(rMeshResponse, caphi0, paraInv0, omegaMax, nMax0, base = base0, fitMethod = fitMethod0)
(rMeshResponse, response2) = resSolver.invCaphi(rMeshResponse, caphi2, paraInv2, omegaMax, nMax2, base = base2, fitMethod = fitMethod2)
(rMeshResponse, responseL) = resSolver.invCaphi(rMeshResponse, caphiL, paraInvL, omegaMax, nMaxL, base = baseL, fitMethod = fitMethodL)
response = response0 + response2 + responseL

########### See if caphi is splited successfully ##########
#plt.figure(0)
#plt.plot(sigmaRMesh, caphi0, label = 'caphi0')
#plt.plot(sigmaRMesh, caphi2, label = 'caphi2')
#plt.plot(sigmaRMesh, caphiL, label = 'caphiL')
#plt.legend()

########### Find the minimum the lMax ##########
#tail = 6 # show the last 6 channels
#plt.figure(0)
#resChi.plotCaphi(caphi, tail)
#plt.show() # lMax = 7 could keep the relative error smaller than 1%
########## Output data ##########
resR = np.vstack( (rMeshResponse, response) ).T
resRPd = pd.DataFrame(resR)
resRPd.to_csv('../data/resChi' + str(int(mambda)) + '.csv', header = None, index = None)
######### Plot the response function ##########
plt.figure(1)
resChi.plotResponse(rMeshResponse, response0, 0, 0) # output the response function
resChi.plotResponse(rMeshResponse, response2, 2, 2) # output the response function
resChi.plotResponse(rMeshResponse, response, 0, lMax) # output the response function
plt.show()
