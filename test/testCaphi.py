# testGenChs.py
# Zeyuan, Feb. 10, 2020

# Test functions in caphi.py (includes outputting the response function)

import testCOMMON
import numpy as np
from src.core.chiral.caphi import Caphi # calculate caphi
import constant as const # constants
import myMath # personal mathermatical function
from matplotlib import pyplot as plt
from src.core.chiral.litSolverChi import litSolverChi as lits # solve the lit equations
import matOpt # personal matrix operator
import scipy.linalg as linalg
from scipy import special
import litInversion as litinv # inversion
from potChiral import potChiral as potChi # chiral potential
from src.core.chiral.SDWavemm import SDWavemm as SDWave # wave function

########## Check ifChsLegal ##########
# Generate test data
chs = np.zeros((4, 4))
chs[0] = np.array([1, 0, 0, 2]) # SD coupling, legal
chs[1] = np.array([1, 0, 0, 3]) # illegal
chs[2] = np.array([11, 10, 10, 12]) # legal
chs[3] = np.array([11, 11, 10, 12]) # illegal
# Start testing
caphi = Caphi()
for chn in chs:
    print(caphi.ifChsLegal(chn))
########## Check genChs, addChn and other chs related operation ##########
caphi.genChs(1) # generate channels up to L = 1
print(caphi.chs)
chn = [6, 6, 6, 6] # Add a channel
caphi.addChn(chn)
#caphi.addChn([6, 6, 6, 9]) # This channel is illegal. The program will abort and throw error.
print(caphi.chs)
caphi.chs[tuple(chn)] = [1, 1] # give a value to chn
print(caphi.chs)
caphi.deleteCaphi() # delete
print(caphi.chs) # Go back to (None, None) for all chs again
caphi.cleanChs() # Output {} means the channels have been all cleared
########## Check caphi.addSigma() ##########
# Please see _Check caphi.addSigma()_ testLitSolverChi.py for this test
#################### input ####################
mambda = 600 / const.HBarC
nGau = 100
aGau, bGau = 0, 3 * mambda
aGauR, bGauR = 0 / mambda, 150 / mambda
qChar = np.sqrt(5)
sigmaI = 5 / const.HBarC
sigmaRMesh = np.linspace(1 / const.HBarC, 250 / const.HBarC, 50)
chnSD = np.array([1, 0, 1]) # S-D coupled channel
lMax = 1 # max L, must >= 1
partL = [1, lMax]
para = [] # Para for potential
paraInv = 10 # para for inversion
omegaMax = 500 / const.HBarC # This can be read off from the base function.
nMax = 10 # Maximum fiiting order of the inversion
rMeshResponse = np.linspace(1 / const.HBarC, 250 / const.HBarC, 100) # unit = fm. xMesh for response function
checkOpt = 'on' # Check if the response is fitted well
checkN = 20
#################### init ####################
(pMesh, wpMesh) = myMath.legGaussPoint(nGau, aGau, bGau, "plain") # generate momentum mesh points
(rMesh, wrMesh) = myMath.legGaussPoint(nGau, aGauR, bGauR, "plain") # Generate coordinate mesh points. End boundary in coordinate space is usually be taken as 200 / mambda.
potC = potChi(mambda, pMesh, wpMesh, rMesh, wrMesh, para)
potC.potChiral(chnSD) # Generate SD potential mesh
wave = SDWave(potC.v00MeshP, potC.v01MeshP, potC.v10MeshP, potC.v11MeshP, potC.pMesh, potC.wpMesh, potC.rMesh, potC.wrMesh)
# Calculate wavefunction
wave.sdgSolver(atol = const.atolSDGChiralMM) # Calculate S- and D- wave function
litC = lits(v00MeshR = potC.v00MeshR, v01MeshR = potC.v01MeshR, v10MeshR = potC.v10MeshR, v11MeshR = potC.v11MeshR, rMesh = rMesh, wrMesh = wrMesh) # init a 0 potential solver. Potential as well as wave function and binding energy would be seted when considering particular chn
caphi = Caphi(sigmaRMesh, sigmaI, qChar) # init
caphi.genChs(lMax) # generate channels up to L = lMax
#################### caphi ####################
for chn in caphi.chs: # Calculate every chn separately
    potC.potChiral([1, chn[2], chn[0]]) # Generate potential mesh. It only eat the first three quantum numbers. v_l2 would be generated as well. chn has formmat (j, L, l1, l2), while the input of potChiral requires (S, L, J), where S means total spin of the deuteron, usually be taken as 1, L means total orbital momentum which is l1 and l2, J is j, total angular momentum
    # Derteron wave function is unchanged. Set potential.
    litC.setPot(wave.uWave, wave.wWave, potC.v00MeshR, potC.v01MeshR, potC.v10MeshR, potC.v11MeshR, wave.eb)
    litC.setQN(j = chn[0], L = chn[1], l1 = chn[2], l2 = chn[3])
    caphi.addSigma(litC, chn)
plt.figure(0)
for chn in caphi.chs: # Plot caphi of all channels
    print('chn counter: ', chn)
    plt.plot(sigmaRMesh * const.HBarC, np.array(caphi.chs[chn][0]) / const.HBarC / const.HBarC, label = (chn[0], chn[1], chn[2])) # The label is (j, L, l1)
    plt.plot(sigmaRMesh * const.HBarC, np.array(caphi.chs[chn][1]) / const.HBarC / const.HBarC, label = (chn[0], chn[1], chn[3])) # The label is (j, L, l2)
    plt.legend()
########## Check the summation of contributions from L = L1 to L =L2 ##########
#print(caphi.chs.keys())# Please uncomment _show all valid channels_ in function sumCaphi, and compared with this line.
caphiPart0 = caphi.sumCaphi([0, 0]) # Summing over contributions from L = 0 to L = 1
caphiPartL = caphi.sumCaphi(partL) # Summing over contributions from L = 0 to L = 1
plt.figure(1)
plt.plot(sigmaRMesh * const.HBarC, caphiPartL / const.HBarC / const.HBarC, label = 'sum over the contributions of L = 0 to L = ' + str(lMax)) # The label is (j, L, l2)
plt.legend()
plt.figure(2)
plt.plot(sigmaRMesh * const.HBarC, caphiPart0 / const.HBarC / const.HBarC, label = 'caphi for L = 0') # The label is (j, L, l2)
plt.legend()
########## Inversion ##########
paraInvList = [wave.eb, paraInv]
invF = litinv.inversion(sigmaI, mambda)
# fir L = 1 to L = lMax
invF.setBase(litinv.base2, paraInvList, omegaMax)
invF.fitData(sigmaRMesh, caphiPartL, nMax, method = 'smart')
(rMeshResponseL, responseL) = invF.responseFunc(rMeshResponse) # response function. rMeshResponse would be modified a bit
if(checkOpt == 'on'):
    invF.checkFit(sigmaRMesh, caphiPartL, checkN, fig = 3)
# fit L = 0
invF.fitData(sigmaRMesh, caphiPart0, nMax, method = 'Normal')
(rMeshResponse0, response0) = invF.responseFunc(rMeshResponse) # response function. rMeshResponse would be modified a bit
if(checkOpt == 'on'):
    invF.checkFit(sigmaRMesh, caphiPart0, checkN, fig = 3)
# Plot the response function
plt.figure(4)
plt.plot(rMeshResponseL * const.HBarC, responseL / const.HBarC, label = 'L = 1 to L = ' + str(lMax))
plt.plot(rMeshResponse0 * const.HBarC, response0 / const.HBarC, label = 'L = 0')
plt.plot(rMeshResponse0 * const.HBarC, (response0 + responseL) / const.HBarC, label = 'Total')
plt.legend()

########## Test caphi split ##########
(caphi0, caphi2, caphiL) = caphi.split() # Split caphi into three different shapes
plt.figure(5)
plt.plot(sigmaRMesh * const.HBarC, caphi0 / const.HBarC / const.HBarC, label = 'the shape for (j, L, l) = (1, L, 0)')
plt.plot(sigmaRMesh * const.HBarC, caphi2 / const.HBarC / const.HBarC, label = 'the shape for (j, L, l) = (1, L, 2)')
plt.plot(sigmaRMesh * const.HBarC, caphiL / const.HBarC / const.HBarC, label = 'the shape for other channels')
plt.legend()
#################### show all figure ####################
plt.show()
