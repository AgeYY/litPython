import testCOMMON
import phyModel as phyM
from matplotlib import pyplot as plt
import numpy as np

harmo = phyM.harmonic1D(1, 1)
print(harmo.energy(0))

xMesh = np.linspace(-10, 10, 100)
plt.figure(0)
plt.plot(xMesh, harmo.groundWave(xMesh), label = 'ground wave function')
plt.grid()
plt.legend()
plt.show()
