import testCOMMON
from waveFunc import WaveFunc as wfunc
from litSolver import litSolver as lits
import numpy as np
from scipy.optimize import lsq_linear
import litInversion as litinv
import constant as const
import plotTool as pltool
from matplotlib import pyplot as plt
from potential import Pot
import myMath as mymath
from novelInv import NInv as ninv
from unitaryPot import UnitaryPot


checkN = 20
nGau = 300 # Mesh points of the wave function
nSigmaR = 80
nOmega = 50
nSigmaRMax = 200 / const.HBarC
omegaMin = const.ETH
omegaMax = nSigmaRMax
mambda = 600 / const.HBarC
sigmaI = 3 / const.HBarC
qChar = 2.23
lMin = 1
lMax = 2

#pot = UnitaryPot(mambda, const.A0, kappa0 = 0.05) # potential
pot = UnitaryPot(mambda, const.A0, kappa0 = 0.1) # potential
waveFunc = wfunc(mambda, nGau) # init
waveFunc.setPot(pot)

litSolver = lits(waveFunc)
sigmaRMesh = np.linspace(0, nSigmaRMax, nSigmaR) # Convert to fm. I am afraid of the term, -E0 - sigmaR would change the sign if sigmaR is not big enough
omega = np.linspace(omegaMin, omegaMax, nOmega)
wOmega = np.ones(nOmega) * (omegaMax - omegaMin) / nOmega
sigmaRMesh = sigmaRMesh[:]
(phiCapL, phiCapNLO) = litSolver.solverNLOL(qChar, sigmaI, sigmaRMesh, lMin, lMax)
nMax = 10 # Max n value for inversion
invF = ninv(sigmaI)
# normal Eq
xMesh, rMesh = invF.normalEq(sigmaRMesh, phiCapL, omega, wOmega)
invF.plotRes(xMesh, rMesh)
cutwOmega = wOmega[-len(xMesh):]
invF.checkRes(sigmaRMesh, phiCapL, xMesh, cutwOmega, rMesh)
# GD
xMesh, rMesh = invF.GDes(sigmaRMesh, phiCapL, omega, wOmega, alpha = 0.0001)
invF.plotRes(xMesh, rMesh)
cutwOmega = wOmega[-len(xMesh):]
invF.checkRes(sigmaRMesh, phiCapL, xMesh, cutwOmega, rMesh)
# Tranditional
xMesh, rMesh = invF.trad(sigmaRMesh, phiCapL, omega, wOmega, para1 = 1, nMax = 20)
invF.plotRes(xMesh, rMesh)
cutwOmega = wOmega[-len(xMesh):]
invF.checkRes(sigmaRMesh, phiCapL, xMesh, cutwOmega, rMesh)
# GD l2reg
xMesh, rMesh = invF.GDes(sigmaRMesh, phiCapL, omega, wOmega, alpha = 0.0001, reg = invF.l2reg, dreg = invF.l2dreg, mambda = 0.001)
invF.plotRes(xMesh, rMesh)
cutwOmega = wOmega[-len(xMesh):]
invF.checkRes(sigmaRMesh, phiCapL, xMesh, cutwOmega, rMesh)
