import testCOMMON
import numpy as np
from waveFunc import WaveFunc as wfunc
import constant as const
import myMath
from matplotlib import pyplot as plt
import plotTool as pltool
from litSolver import litSolver as lits
import matOpt
import scipy.linalg as linalg
from scipy import special
import litInversion as litinv

mambda = 600 / const.HBarC
nGau = 500
waveFunc = wfunc(mambda, nGau) # init
litSolver = lits(waveFunc)
qChar = np.sqrt(5)
sigmaI = 5 / const.HBarC
# Check the lit function
sigmaRMesh = np.linspace(-const.E0, 250, 50) / const.HBarC # Convert to fm. I am afraid of the term, -E0 - sigmaR would change the sign if sigmaR is not big enough
lMax = 10
for l in np.arange(1, lMax):
    phiCapL = litSolver.solverL(qChar, sigmaI, sigmaRMesh, l, l)
    pltool.simplePlot(sigmaRMesh * const.HBarC, phiCapL / const.HBarC / const.HBarC) # Convert to MeV and compare with the result in the paper
plt.show()
## Ouput data
#sigmaRPhiCap = np.vstack((sigmaRMesh * const.HBarC, phiCapL / const.HBarC / const.HBarC))
#np.savetxt("/home/ian/Work/LIT/lit_darmstadt/data/LITSigmaRPlot0.csv", sigmaRPhiCap.T, delimiter=",")
## Check your lit wave function
#sigmaRMesh = np.array([2])
#phiCap = litSolver.solverL0(qChar, sigmaI, sigmaRMesh)
#pltool.simplePlot(litSolver.xrMesh, np.real(litSolver.phiLITL0Mesh))
#plt.show()
## Check nCoe
#endAt = 50
#pltool.simplePlot(litSolver.xrMesh[4:endAt], litSolver.nCoe[4:endAt])
#plt.show()
#print(1 / (4 * np.pi))
## Check every operator
#nMesh = 100
#aGau = 0
#bGau = 1
#A = 1
#B = -2
#yVal1, yVal2 = 1, np.exp(bGau)
#def potFunc(x, y):
#    #return 0 # Delete the pot
#    return 1 / x / y
#def fFunc(x):
#    #return -2 * np.exp(x) # For pot = 0 and B = -1
#    return -3 * np.exp(x) + (np.e - 1) # For pot = 1 / x/ y, A = 1 and B = -2
#
#(xMesh, wMesh) = myMath.legGaussPoint(nMesh, aGau, bGau, 'plain')
#potMat = np.zeros((nMesh, nMesh))
#for i in np.arange(nMesh):
#    potMat[i, :] = potFunc(xMesh[i], xMesh)
#litSolver.solverL0General(xMesh, wMesh, 0, A, potMat, B, fFunc(xMesh), yVal1, yVal2)
#fLjlGenMesh = litSolver.fLjlGen(qChar, 0)

#pltool.simplePlot(xMesh, litSolver.phiLITL0Mesh)
#plt.show()
#pltool.simplePlot(litSolver.xrMesh, fLjlGenMesh)
#plt.show()

## This is a test of your method for solving the differential equation. Ref: http://mathworld.wolfram.com/BesselDifferentialEquation.html
#dOpt = matOpt.dOpt(xrMesh)
#ddOpt = dOpt.dot(dOpt)
#n = 0
#eye = np.identity(xrMesh.size)
#lhs = ddOpt + ((1 / xrMesh)*eye).dot(dOpt) + eye - (n**2 / xrMesh / xrMesh)*eye
#yrMesh = linalg.null_space(lhs)
#yrMesh *= 1 / yrMesh[0]
#pltool.simplePlot(xrMesh, yrMesh)
#pltool.simplePlot(xrMesh, special.jn(n, xrMesh))
#plt.show()

########## Check the nlo calculation ##########
##### Check the prototype of nlo equation #####
############# Input #############
#nGau = 600 # Mesh points of the wave function
#mambda = 600 / const.HBarC
#sigmaI = 5 / const.HBarC
#sigmaRMin, sigmaRMax = 10 / const.HBarC, 250 / const.HBarC
#NsigmaR = 50
#qChar = np.sqrt(5)
#nMax = 10 # Max n value for inversion
#para = [const.E0, 15]
#
#checkN = 20
############ Calculation #########
#waveFunc = wfunc(mambda, nGau) # init
#litSolver = lits(waveFunc)
#sigmaRMesh = np.linspace(sigmaRMin, sigmaRMax, NsigmaR)
#sigmaRMeshDens = np.linspace(sigmaRMin, sigmaRMax, 200)
#invF = litinv.inversion(sigmaI, mambda)
#omegaMax = 500 / const.HBarC # This can be read off from the base function.
#invF.setBase(litinv.base1, para, omegaMax)
#
#phiCapL = litSolver.solverL(qChar, sigmaI, sigmaRMesh, 0, 0) # L = 0
#invF.fitData(sigmaRMesh, phiCapL, nMax)
#invF.checkFit(sigmaRMesh, phiCapL, checkN)
#(sigmaRMeshDens, rMesh) = invF.responseFunc(sigmaRMeshDens)
#
#phiCapL = litSolver.solverL(qChar, sigmaI, sigmaRMesh, 1, 10) # L = 0
#invF.fitData(sigmaRMesh, phiCapL, nMax)
#invF.checkFit(sigmaRMesh, phiCapL, checkN)
#(sigmaRMeshDens, rMesh1) = invF.responseFunc(sigmaRMeshDens)
#rMesh += rMesh1
#
############ Figure ##########
#pltool.simplePlot(sigmaRMeshDens * const.HBarC, rMesh / const.HBarC) # Convert to MeV and compare with the result in the paper
#plt.show()
