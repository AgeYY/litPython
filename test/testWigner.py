import sympy.physics.wigner as wgn
import sympy

print(sympy.N(wgn.wigner_3j(2,6,4, 0,0,0), 10))
print(wgn.wigner_9j(1,1,1, 1,1,1, 1,1,0 ,prec=64))
print(wgn.wigner_6j(1,1,1, 1,1,1 ,prec=64))
