import testCOMMON
import numpy as np
from waveFunc import WaveFunc as wfunc
import constant as const
import myMath
from matplotlib import pyplot as plt
import plotTool as pltool
from unitaryPot import UnitaryPot
from potential import Pot

def testWave(mambda, nGau, kappa0 = 1/ 20):
    # Generate wave function
    mambda = mambda
    nGau = nGau
    c0Method = 'scattering length'
    #pot = UnitaryPot(mambda, const.A0, kappa0 = kappa0) # potential
    pot = Pot(mambda, const.A0, c0Method) # potential
    waveFunc = wfunc(mambda, nGau, bGaur = 500 / 3) # init
    waveFunc.setPot(pot)
    (xMesh, wMesh, xrMesh, wrMesh) = waveFunc.osXW() # use os to read data
    size = waveFunc.osGau()[0]
    philomm = waveFunc.osPhilomm()
    philocd = waveFunc.osPhilocd()
    phinlomm = waveFunc.osPhinlomm()
    phinlocd = waveFunc.osPhinlocd()
    
    #Check the normalization of the wave functions
    checkNormlomm = myMath.normCalD(philomm, xMesh, wMesh)
    checkNormlocd = myMath.normCalD(philocd, xrMesh, wrMesh)
    print(checkNormlomm, checkNormlocd)
    #Check the orthognality wave functions
    orthomm = np.sum(xMesh * xMesh * wMesh * philomm * phinlomm)
    orthocd = np.sum(xrMesh * xrMesh * wrMesh * philocd * phinlocd)
    print(orthomm, orthocd)
    
    # check the shape of the LO wave function
    plt.figure(0)
    plt.plot(xMesh, philomm, label = "leading order wave function in momentum space")
    plt.plot(xMesh, phinlomm, label = "next leading order wave function in momentum space")
    plt.grid()
    plt.legend()
    # Plot Chesk print out the LO wave function in coordinate space
    plt.figure(1)
    plt.plot(xrMesh, philocd, label = "leading order")
    plt.plot(xrMesh, phinlocd, label = "next leading order")
    plt.legend()
    plt.grid()
    
    # Use the binding energy to check your wave function
    # kMat = np.identity(size) * xMesh * xMesh / const.MNucleon
    kMat = np.identity(size) / xMesh / xMesh / wMesh * xMesh * xMesh / const.MNucleon # This final two xMesh come from the identity of your partial wave. According to your convension, identity = 1 / p / p / weight
    vloMat = np.zeros((size, size))
    vnloMat = np.zeros((size, size))
    vlocdMat = np.zeros((size, size))
    vnlocdMat = np.zeros((size, size))
    
    for i in np.arange(size): # Potential
        vloMat[i, :] = waveFunc.pot.potlo(xMesh[i], xMesh, "mm")
        vnloMat[i, :] = waveFunc.pot.potnlo(xMesh[i], xMesh, "mm")
        vlocdMat[i, :] = waveFunc.pot.potlo(xrMesh[i], xrMesh, "cd")
        vnlocdMat[i, :] = waveFunc.pot.potnlo(xrMesh[i], xrMesh, "cd")
    philommShelled = philomm * xMesh * xMesh * wMesh
    philocdShelled = philocd * xrMesh * xrMesh * wrMesh
    
    hlo = kMat + vloMat # leading order hamiltonian
    e0 = (philommShelled).dot(hlo).dot(philommShelled)
    print("Binding energy before the correction:%.2e"%(e0 * const.HBarC)) # Convert to MeV
    hlo = hlo + vnloMat # leading order hamiltonian
    e0 = (philommShelled).dot(hlo).dot(philommShelled)
    print("Binding energy after the correction:%.2e"%(e0 * const.HBarC)) # Convert to MeV
    e0cd = (philocdShelled).dot(vnlocdMat).dot(philocdShelled) # next leading order correction on coordinate space
    e0mm = (philommShelled).dot(vnloMat).dot(philommShelled) # next leading order correction on momentum space
    print("Next leading order correction in coordinate space:%.2e\nNext leading order correction in momentum space: %.2e"%(e0cd * const.HBarC, e0mm * const.HBarC)) # Convert to MeV
    # For unitaryPot
    print("Next leading order correction in coordinate space (theory):%.2e"%(- 2 * kappa0 / const.A0 / const.MNucleon * const.HBarC)) # Convert to MeV
    #pltool.simplePlot(waveFunc.xMesh, waveFunc.ulommMesh)
    #pltool.simplePlot(waveFunc.xrMesh, waveFunc.unlocdMesh, figLab = 1)
    #plt.show()
    e0lo, e0nlo = waveFunc.findE0()
    #print(e0lo * const.HBarC, e0nlo * const.HBarC)
    print(e0lo * const.HBarC, e0nlo * const.HBarC)
    return e0lo, e0nlo

eloSet = []
enloSet = []
mambdaa = 600 / const.HBarC
mambdab = 600 / const.HBarC
nMambda = 1
mambdaSet = np.linspace(mambdaa, mambdab, nMambda)
for mambda in mambdaSet:
    elo, enlo = testWave(mambda, 300, kappa0 = 0.05)
    eloSet.append(elo * const.HBarC)
    enloSet.append(enlo* const.HBarC)

plt.figure(42)
plt.plot(mambdaSet, eloSet)
plt.plot(mambdaSet, enloSet)
plt.show()
