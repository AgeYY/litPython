import testCOMMON
import myMath
import numpy as np
from matplotlib import pyplot as plt

#Test normCal.
xSquare = lambda x: 1 / x
print(myMath.normCal(xSquare, 0, 9))

# Test legGaussPoint
(xMesh, wMesh) = myMath.legGaussPoint(10, 0, 1, "plain")
fTest = lambda x: np.exp(x)
intg = np.sum(fTest(xMesh) * wMesh)
print(intg)

# Test fredholm1
#def gFunc0(x, y):
#    return x * 10 + y
#def hFunc0(x):
#    return x
#
#xMesh = np.arange(0, 5, 1)
#yArr = myMath.fredholm1(gFunc0, hFunc0, xMesh, xMesh)

## Test fredholm1Func
#def gFunc(x, y):
#    return np.exp(-x*y)
#def hFunc(x):
#    return - (np.exp(-(x + 1)) - 1) / (x + 1)
#
#yFunc = myMath.fredholm1Func(gFunc, hFunc, 400, 0, 1)
#
#xMesh = np.linspace(0.1, 0.9, 40)
#plt.figure(0) # this should be a exponential decaying function
#plt.plot(xMesh, yFunc(xMesh))
#plt.grid()
#plt.show()

########## Test the fourier3DRadical ##########
#bGau = 10
#(xMesh, wMesh) = myMath.legGaussPoint(100, 0, bGau, "plain")
#yMesh = 1 / xMesh
#(rMesh, wrMesh) = myMath.legGaussPoint(100, 0, bGau, "plain")
#ryMesh = myMath.fourier3DRadical(xMesh, wMesh, yMesh, rMesh, wrMesh)
##yMesh2 = myMath.fourier3DRadical(rMesh, wrMesh, ryMesh, xMesh, wMesh)
#plt.figure(0) # This should be a cos(k x) / (x^2)
#ansFunc = 4 * np.pi / np.power((2 * np.pi), 3 / 2) * (1 - np.cos(rMesh * bGau)) / rMesh / rMesh
##plt.plot(xMesh, yMesh2)
#plt.plot(rMesh, ryMesh, label = 'your result')
#plt.plot(rMesh, ansFunc, label = 'exact solution')
#plt.legend()
#plt.grid()
#plt.show()
######### Test fourier3DRadical2 ##########
#a, b = 0, np.pi/2 # integral range
#(kMesh, wkMesh) = myMath.legGaussPoint(10, a, b, "plain")
#vMesh = np.zeros((kMesh.size, kMesh.size))
#for i in np.arange(kMesh.size):
#    vMesh[i, :] = 1 / kMesh / kMesh[i]
#(rMesh, wrMesh) = myMath.legGaussPoint(10, 1, 2, "plain")
#vMeshR = myMath.fourier3DRadical2(kMesh, wkMesh, vMesh, rMesh, wrMesh)
#def vRAns(r, rp):
#    return 2 / np.pi * (np.cos(b * r) - 1) * (np.cos(b * rp) - 1) / rp / rp / r / r
#vMeshRAns = np.zeros((rMesh.size, rMesh.size))
#for i in np.arange(kMesh.size):
#    vMeshRAns[i, :] = vRAns(rMesh, rMesh[i])
#print(vMeshRAns)
#print(vMeshR)
#ans = np.allclose(vMeshR, vMeshRAns, rtol=1e-10)
#
#print(ans)

x1 = np.linspace(0, 10, 100)
x2 = np.linspace(-1, 5, 10)
x = np.linspace(2, 5, 20)
y1 = np.sin(x1)
y2 = np.sin(x2)
y1new, y2new = myMath.ys2x(x1, y1, x2, y2, x)

plt.plot(x, y1new, '-', x, y2new, '--')
plt.show()
