def func(a, b, c = 0, d = 1):
    print(a, b)
    print('c: %f \t d: %f' % (c, d))

para = (1, 2, 3)
func(1, *para)


