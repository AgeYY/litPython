import testCOMMON
import numpy as np
import src.tool.inout as ios
import matplotlib.pyplot as plt
import csv
import constant as const

#func = ios.readFunc('./testInput.csv')
#print(func)
#plt.figure(0)
#plt.plot(func[0]['xVar'], func[0]['yVar'])
#plt.plot(func[1]['xVar'], func[1]['yVar'])
#plt.show()
#
#paras = ios.readIn('./testInput.csv')
#ios.outFunc('./testOut.csv')

paras = ios.readIn('./testInput.csv')

l = ios.dict2list(paras)
l = ios.combineDiff(l)
seed = np.arange(10)

with open('./testOutput.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    for row in l: # loop all combination
        i = 0
        for key, value in paras.items(): # convert it to dict
            paras[key] = row[i]
            i = i + 1
        x = seed
        y = seed * paras['para1'] + paras['para2']
        ios.outFunc(paras, list(x), list(y), writer = writer)

func = ios.readFunc('./testOutput.csv', 'r')
def labelFunc(dic):
    return 'para1 = ' + str(dic['para1']) + '\n para2 = ' + str(dic['para2'])
plt.figure(0)
for aFunc in func:
    plt.plot(aFunc['x'], aFunc['y'], label = labelFunc(aFunc))
plt.legend()
plt.show()
