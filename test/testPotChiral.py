import __init__
import matplotlib.pyplot as plt
from potChiral import potChiral as potChi
from potential import Pot as potPion
import myMath
import constant as const
import numpy as np

########### Test if potChi can output the correct pionless potential ##########
mambda = 3200 / const.HBarC
nGau = 200
aGau, bGau = 0, 3 * mambda
para = []
chs = np.array([1, 0, 1]) # SLJ
(pMesh, wpMesh) = myMath.legGaussPoint(nGau, aGau, bGau, "plain") # generate momentum mesh points
(rMesh, wrMesh) = myMath.legGaussPoint(nGau, aGau, 50 / mambda, "plain") # Generate coordinate mesh points. End boundary in coordinate space is usually be taken as 600 / mambda.
# Method in potChi
potC = potChi(mambda, pMesh, wpMesh, rMesh, wrMesh, para)
resultChi = potC.potPionless(chs)
# Method in potPion
resultPion = np.zeros((pMesh.size, pMesh.size))
pion = potPion(mambda, const.A0, 'scattering length')
for idx in np.arange(pMesh.size):
    resultPion[idx, :] = pion.potlo(pMesh[idx], pMesh, "mm")
ans = np.array_equal(resultChi, resultPion)
print(ans)
# Method in potChiral, p, q are setted as the same mesh
chs = np.array([1, 1, 0])
potC.potChiral(chs)

plt.figure(1)
plt.title('Potential in the coordinate space')
plt.subplot(221)
plt.imshow(potC.v00MeshR)
plt.colorbar()
plt.subplot(222)
plt.imshow(potC.v01MeshR)
plt.colorbar()
plt.subplot(223)
plt.imshow(potC.v10MeshR)
plt.colorbar()
plt.subplot(224)
plt.imshow(potC.v11MeshR)
plt.colorbar()
plt.show()
