# use ANN to invert \Phi
import torch as tch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import matplotlib.pyplot as plt

class Net(nn.Module):
    def __init__(self, sizeIn, sizeOut):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(sizeIn, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 84)
        self.fc4 = nn.Linear(84, sizeOut)

    def changeSize(self, sizeIn, sizeOut):
        self.__init__(sizeIn, sizeOut)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = self.fc4(x)
        return x

#def kernel(sigmaR, omega, wOmega, sigmaI = 1):
#    kMat = np.empty((sigmaR.size, omega.size))
#    for i in range(kMat.shape[0]):
#        kMat[i, :] = 1 / ( (omega - sigmaR[i])**2 + sigmaI**2 ) * wOmega
#    return kMat
#
## Generate data
#nSample = 10 # 50 functions
#nMax = 3 # the highest order of the poly
#nXMesh = 50 # 50 points in each function
#lx, ux = -10, 10 # lower and upper bound for xrMesh and xpMesh
#
#xpMesh = np.linspace(lx, ux, nXMesh) # xMesh for pMesh
#xrMesh = np.linspace(lx, ux, nXMesh) # xMesh for response
#wrMesh = (ux - lx) / nXMesh # integral weight
#kMat = kernel(xpMesh, xrMesh, wrMesh)
#
#rMesh = [] # response function
#pMesh = [] # phi function
#for i in range(nSample):
#    an = np.random.rand(nMax)
#    poly = np.poly1d(an) # generate a new function
#    r = poly(xrMesh)
#    p = kMat.dot(r)
#    rMesh.append(r) # append r and phi
#    pMesh.append(p) # append r and phi

#for i in range(nSample):
#    plt.figure(i)
#    plt.plot(xrMesh, rMesh[i])
#    plt.show()

rMesh = tch.from_numpy(np.array(rMesh)).float()
pMesh = tch.from_numpy(np.array(pMesh)).float()
#print(rMesh, pMesh)
# training
nSteps = 5000
net = Net(nXMesh, nXMesh)
criterion = nn.MSELoss()
optimizer = optim.SGD(net.parameters(), lr=0.000001, momentum=0.9)
running_loss = np.zeros(nSteps)
for i in range(nSteps):
    # zero the parameter gradients
    optimizer.zero_grad()
    outputs = net(pMesh)
    loss = criterion(outputs, rMesh)
    loss.backward()
    optimizer.step()
    # print statistics
    running_loss[i] = loss.item()

plt.figure(0)
plt.plot(range(nSteps), running_loss)
plt.show()

# test
out = net(pMesh[2])
plt.figure(0)
plt.plot(xrMesh, out.detach(), label = 'ANN')
plt.plot(xrMesh, rMesh[2], label = 'Original Response')
plt.legend()
plt.show()

# save
PATH = './invNet.pth'
tch.save(net.state_dict(), PATH)

