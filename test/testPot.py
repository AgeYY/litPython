import testCOMMON
import constant as const
from potential import Pot
from potential import Pot

pot = Pot(200 / const.HBarC, 5.42)
print("%-20s%-20s" % ("Yours", "Ans"))
print("%-20.2e%-20.2e" % (pot.potlo(1 / const.HBarC, 1 / const.HBarC, "mm"), -0.00667262))
print("%-20.2e%-20.2e" % (pot.potnlo(1 / const.HBarC, 1 / const.HBarC, "mm"), -0.18205))
print("%-20.2e%-20.2e" % (pot.potlo(1000 / const.HBarC, 1000 / const.HBarC, "mm"), -0.00667262))
print("%-20.2e%-20.2e" % (pot.potnlo(1000 / const.HBarC, 1000 / const.HBarC, "mm"), -0.18205))
print("%-20.2e%-20.2e" % (pot.potlo(1, 1, "cd"), -0.126))
print("%-20.2e%-20.2e" % (pot.potnlo(1, 1, "cd"), -0.439))
print(pot.E0 * const.HBarC)

rMesh = np.linspace(0, 1000 / const.HBarC, 100)
fig, ax = plt.subplots()
ax.plot(pot.potlo(1 / const.HBarC, rMesh))
