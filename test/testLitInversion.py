import testCOMMON
from waveFunc import WaveFunc as wfunc
from litSolver import litSolver as lits
import numpy as np
from scipy.optimize import lsq_linear
import litInversion as litinv
import constant as const
import plotTool as pltool
from matplotlib import pyplot as plt
from potential import Pot
import myMath as mymath
from unitaryPot import UnitaryPot

checkN = 20
nGau = 500 # Mesh points of the wave function
nSigmaR = 80
nOmega = 50
nSigmaRMax = 200 / const.HBarC
omegaMin = const.ETH
omegaMax = nSigmaRMax
mambda = 600 / const.HBarC
sigmaI = 3 / const.HBarC
qChar = 2.23
lMin = 1
lMax = 10

#pot = UnitaryPot(mambda, const.A0, kappa0 = 0.05) # potential
pot = UnitaryPot(mambda, const.A0) # potential
waveFunc = wfunc(mambda, nGau) # init
waveFunc.setPot(pot)

litSolver = lits(waveFunc)
sigmaRMesh = np.linspace(0, nSigmaRMax, nSigmaR) # Convert to fm. I am afraid of the term, -E0 - sigmaR would change the sign if sigmaR is not big enough
sigmaRMesh = sigmaRMesh[:]
(phiCapL, phiCapNLO) = litSolver.solverNLOL(qChar, sigmaI, sigmaRMesh, lMin, lMax)
nMax = 10 # Max n value for inversion
invF = litinv.inversion(sigmaI, mambda)
fitOmega = 500 / const.HBarC
para = [const.E0, 20]
########## Tranditional Method ##########
invF.setBase(litinv.base2, para, fitOmega)
invF.fitData(sigmaRMesh, phiCapL, nMax, method = 'normalEq')
invF.checkFit(sigmaRMesh, phiCapL, checkN)
omega = np.linspace(omegaMin, omegaMax, nOmega)
wOmega = np.ones(nOmega) * (omegaMax - omegaMin) / nOmega
xMesh, rMesh = invF.responseFunc(omega)
plt.figure(0)
plt.plot(xMesh, rMesh, label = 'Traditional Method')
########## Directly use invert of matrix ##########
def kernel(sigmaR, sigmaI, omega, wOmega):
    kMat = np.empty((sigmaR.size, omega.size))
    for i in range(kMat.shape[0]):
        kMat[i, :] = 1 / ( (omega - sigmaR[i])**2 + sigmaI**2 ) * wOmega
    return kMat

kMat = kernel(sigmaRMesh, sigmaI, omega, wOmega)
########### Test kernel and rMesh
#phiMesh = kMat[:, :].dot(rMesh)
#plt.figure(42)
#plt.plot(sigmaRMesh, phiMesh, label='recover from rMesh')
#plt.plot(sigmaRMesh, phiCapL, label='Original Phi')
#plt.legend()
#plt.show()
########## normal equation
rMesh = np.linalg.pinv(kMat.T.dot(kMat)).dot(kMat.T).dot(phiCapL)
plt.figure(0)
plt.plot(omega, rMesh, label = 'normal eqauation')

########## Gradient Descent
def linearReg(kMat, phiCapL, alpha = 0.0001, iter_number = 1000, reg = None, dreg = None, mambda = 0):
    alpha=alpha # learning rate
    rMesh=np.random.rand(nOmega)
    iter_number =iter_number 
    J = np.empty(iter_number)
    for i in np.arange(iter_number):    
        if dreg is None:
            rMesh = rMesh - alpha*np.dot(kMat.T,(np.dot(kMat,rMesh)-phiCapL))/kMat.shape[0]
        else:
            rMesh = rMesh - alpha*np.dot(kMat.T,(np.dot(kMat,rMesh)-phiCapL))/kMat.shape[0] + dreg(mambda, rMesh)
        diff = kMat.dot(rMesh)-phiCapL
        if reg is None:
            J[i] = diff.dot(diff.T)/2/kMat.shape[0]
        else:
            J[i] = diff.dot(diff.T)/2/kMat.shape[0] + reg(mambda, rMesh)/2/kMat.shape[0]
    return rMesh, J

rMesh, J = linearReg(kMat, phiCapL)
plt.figure(1)
plt.plot(J,"r-");

plt.figure(0)
plt.plot(omega, rMesh, label = 'linear Regression', linestyle = '--')

########## linear Reg with regulator ##########
def reg(mambda, rMesh):
    # regulator
    return mambda * rMesh.dot(rMesh)
def dreg(mambda, rMesh):
    # regulator
    return mambda * rMesh

rMesh, J = linearReg(kMat, phiCapL, reg = reg, dreg = dreg, mambda = 0.0005)
plt.figure(1)
plt.plot(J,"r-");

plt.figure(0)
plt.title('Response fcuntion')
plt.plot(omega, rMesh, label = 'linear Regression with reg')
plt.legend()
plt.grid()
plt.show()
