import testCOMMON
import numpy as np
import matOpt
from matplotlib import pyplot as plt
import phyModel as phyM
import myMath

def yFunc(x):
    return np.exp(x)
xMesh = np.linspace(1, 10, 400)
yMesh = yFunc(xMesh)
dyMesh = matOpt.dOpt(xMesh).dot(yMesh) # First dirivative
ddyMesh = matOpt.dOpt(xMesh).dot(dyMesh) # First dirivative
#plt.figure(0)
#plt.plot(xMesh, yMesh, label = "y")
#plt.plot(xMesh, dyMesh, label = "y\'")
#plt.plot(xMesh, ddyMesh, label = "y\'\'")
#plt.grid()
#plt.legend()
#plt.show()

# Find the eigenvalue of a harmonic osillator
mass = 1
harmo = phyM.harmonic1D(mass, 1)
(xMesh, wMesh) = myMath.legGaussPoint(500, -10, 10, "plain")
dOpt = matOpt.dOpt(xMesh)
ddOpt = dOpt.dot(dOpt)
hamiltonK = - ddOpt / 2 / mass
yMesh = harmo.groundWave(xMesh)
yMeshLef = np.conj(yMesh) * wMesh
groundE = yMeshLef[3:-3].dot(hamiltonK.dot(yMesh)[3:-3]) # 2nd D, so cut 4 points
print("The ground energy: %.2e\nYour ans:%.2e"%(harmo.energy(0), groundE))
#plt.figure(0)
#plt.plot(xMesh, yMesh, label = "y")
#plt.plot(xMesh, dOpt.dot(yMesh), label = "y\'")
#plt.plot(xMesh, ddOpt.dot(yMesh), label = "y\'\'")
#plt.grid()
#plt.legend()
#plt.show()
