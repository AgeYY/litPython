import __init__
import constant as const
import numpy as np
from potChiral import potChiral as potChi
import myMath
from src.core.chiral.SDWavemm import SDWavemm as SDWave
import matplotlib.pyplot as plt

mambda = 3200 / const.HBarC
nGau = 300
aGau, bGau = 0, 3 * mambda
para = []
chs = np.array([1, 0, 1])
########## Generate mesh points ##########
(pMesh, wpMesh) = myMath.legGaussPoint(nGau, aGau, bGau, "plain") # generate momentum mesh points
(rMesh, wrMesh) = myMath.legGaussPoint(nGau, aGau, 50 / mambda, "plain") # Generate coordinate mesh points. End boundary in coordinate space is usually be taken as 600 / mambda.
# Method in potChi
potC = potChi(mambda, pMesh, wpMesh, rMesh, wrMesh, para)
# Method in potChiral, p, q are setted as the same mesh
potC.potChiral(chs, uptoQn = 0) # Generate potential mesh
sdWave = SDWave(potC.v00MeshP, potC.v01MeshP, potC.v10MeshP, potC.v11MeshP, potC.pMesh, potC.wpMesh, potC.rMesh, potC.wrMesh)
########### print potential mesh ##########
#plt.figure(1)
#plt.title('Potential in the coordinate space')
#plt.subplot(221)
#plt.imshow(sdWave.vMesh[0, 0, :, :])
#plt.colorbar()
#plt.subplot(222)
#plt.imshow(sdWave.vMesh[0, 1, :, :])
#plt.colorbar()
#plt.subplot(223)
#plt.imshow(sdWave.vMesh[1, 0, :, :])
#plt.colorbar()
#plt.subplot(224)
#plt.imshow(sdWave.vMesh[1, 1, :, :])
#plt.colorbar()

########## print potential mesh ##########
plt.figure(1)
plt.title('Potential in the momentum space')
plt.plot(rMesh, potC.v00MeshP[int(nGau / 6), :])
plt.show()

plt.figure(0)
sdWave.genAMat()
plt.imshow(sdWave.aMat[:, :])
plt.colorbar()
plt.show()
######### find the binding energy ##########
EbArr = np.linspace(-10 / const.HBarC, 0, 50)
detArr = np.zeros(EbArr.size)
for i in np.arange(EbArr.size):
    detArr[i] = sdWave.findDet(EbArr[i])
plt.figure(0)
plt.plot(EbArr * const.HBarC, detArr)
plt.grid()
plt.show()
######### wave function ##########
EB = sdWave.findEB(-10 / const.HBarC, 0)
print(EB * const.HBarC)
#detEb = sdWave.findDet(EB)
#
#sdWave.sdgSolver(atol = const.atolSDGChiralMM) # Calculate S- and D- wave function
#sdWave.printWave() # Print out the wavefunction and compare it with the ones in doc/waveFunc.pdf
