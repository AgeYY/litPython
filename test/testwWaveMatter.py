import testCOMMON
import numpy as np
from src.core.chiral.caphi import Caphi # calculate caphi
import constant as const # constants
from matplotlib import pyplot as plt
import matOpt # personal matrix operator
from scipy import special
import sympy
import sympy.physics.wigner as wgn

caphi = Caphi()
caphi.genChs(10) # generate channels up to L = 1
print(caphi.chs)

def alphaLjl(L, j, l):
    sign = 1
    if (j // 2 == 0): sign = -1
    return sign * np.sqrt(15) * np.sqrt((2 * l + 1) * (2 * L + 1)) * sympy.N(wgn.wigner_3j(L,l,2, 0,0,0) * wgn.wigner_6j(2,1,1, j,l,L), 16)

for key in caphi.chs:
    print('channels: ', key[1], key[0], key[2])
    print('alpha value: ', alphaLjl(key[1], key[0], key[2]))
    print('channels: ', key[1], key[0], key[3])
    print('alpha, value: ', alphaLjl(key[1], key[0], key[3]))
