import testCOMMON
import numpy as np
from waveFuncn2lo import WaveFuncn2lo as wfunc
import constant as const
import myMath
from matplotlib import pyplot as plt
import plotTool as pltool
from litSolvern2lo import LitSolvern2lo as lits
import matOpt
import scipy.linalg as linalg
from scipy import special
import litInversion as litinv
from unitaryPotn2lo import U2Pot
 
mambda = 600 / const.HBarC
nGau = 500
aGau = 0
bGau = mambda * 3
aGaur = 0
bGaur = 1000 / 3
qChar = np.sqrt(5)
sigmaI = 2 / const.HBarC
# Check the lit function
lMax = 10
kappa0 = 0.1
kappa1 = 1 / const.A0 - kappa0
sigmaRMin, sigmaRMax, NsigmaR = 0, 250 / const.HBarC, 100
para = [-1.41, 10]
nMax = 12
checkN = 30

pot = U2Pot(mambda, const.A0, kappa0 = kappa0, kappa1 = kappa1) # potential
waveFunc = wfunc(mambda, nGau, bGaur = bGaur) # init
waveFunc.setPot(pot)
waveFunc.loSolver()
waveFunc.nloSolver()
waveFunc.n2loSolver()

vlocdMesh = np.zeros((nGau, nGau))
vnlocdMesh = np.zeros((nGau, nGau))
vn2locdMesh = np.zeros((nGau, nGau))

# generate potential
for i in range(nGau):
    for j in range(nGau):
        vlocdMesh[i, j] = pot.potlo(waveFunc.xrMesh[i], waveFunc.xrMesh[j], 'cd')
        vnlocdMesh[i, j] = pot.potnlo(waveFunc.xrMesh[i], waveFunc.xrMesh[j], 'cd')
        vn2locdMesh[i, j] = pot.potn2lo(waveFunc.xrMesh[i], waveFunc.xrMesh[j], 'cd')

litSolver = lits(mambda, nGau, waveFunc.aGau, bGau, aGaur, bGaur, waveFunc.xrMesh, waveFunc.wrMesh)
litSolver.setLO(vlocdMesh, waveFunc.ulocdMesh, waveFunc.e0)
litSolver.setNLO(vnlocdMesh, waveFunc.unlocdMesh, waveFunc.e1) # e1: NLO correction of energy
litSolver.setN2LO(vn2locdMesh, waveFunc.un2locdMesh, waveFunc.e2)
sigmaRMesh = np.linspace(sigmaRMin, sigmaRMax, NsigmaR)
sigmaRMeshDens = np.linspace(sigmaRMin, sigmaRMax, 200)
invF = litinv.inversion(sigmaI, mambda)
omegaMax = 500 / const.HBarC # This can be read off from the base function.
invF.setBase(litinv.base1, para, omegaMax)

phiCapLO, phiCapNLO, phiCapN2LO = litSolver.solverN2LO(qChar, sigmaI, sigmaRMesh, 0, 0) # L = 0
invF.fitData(sigmaRMesh, phiCapN2LO, nMax)
invF.checkFit(sigmaRMesh, phiCapN2LO, checkN)
(sigmaRMeshDens, rMesh) = invF.responseFunc(sigmaRMeshDens)

phiCapLO, phiCapNLO, phiCapN2LO = litSolver.solverN2LO(qChar, sigmaI, sigmaRMesh, 1, 10) # L = 0
invF.fitData(sigmaRMesh, phiCapN2LO, nMax)
invF.checkFit(sigmaRMesh, phiCapN2LO, checkN)
(sigmaRMeshDens, rMesh1) = invF.responseFunc(sigmaRMeshDens)
rMesh += rMesh1

########### Figure ##########
pltool.simplePlot(sigmaRMeshDens * const.HBarC, rMesh / const.HBarC) # Convert to MeV and compare with the result in the paper
plt.show()
