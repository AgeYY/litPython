import numpy as np
import pandas as pd

# How Python deal with product
aMat = np.array([[1, 1], [1, 1]])
a = np.array([1, 2])
print(a * aMat)
print(aMat * a)
a = a.reshape((-1, 1))
print(a * aMat)
# Delete array
arr = np.array([[1,2,3,4], [5,6,7,8], [9,10,11,12]])
arr = np.delete(arr, [0, 1], 0)
print(arr)
arr = np.delete(arr.flat, 0)
print(arr)
# inverse speed
arr = np.array([[1,2,3,4], [5,6,7,8], [9,10,11,12]])
arr = np.delete(arr, [0, 1], 0)
print(arr)
arr = np.delete(arr.flat, 0)
print(arr)
# Check 
arr = np.array([1, 2, 3])
mat1 = (arr * np.identity(3)).dot(np.ones((3, 3)) * arr)
print(mat1)
########## numpy to DataFrame ##########
arrFunc = pd.DataFrame(np.vstack((arr, arr)).T)
print(arrFunc)
############### Test fill between ###############
import matplotlib.pyplot as plt

plt.plot([0,1],[0,1],ls="--",c="b")
plt.fill_between([0,1],[0,1], alpha=.3)
plt.fill_between([0,1],[1,0], alpha=.3)
plt.savefig('../fig/testformat.pdf')
plt.show()

