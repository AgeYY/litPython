import __init__
import constant as const
import numpy as np
from potChiral import potChiral as potChi
import myMath
from src.core.chiral.SDWave import SDWave
import matplotlib.pyplot as plt

mambda = 600 / const.HBarC
nGau = 300
aGau, bGau = 0, 3 * mambda
para = []
chs = np.array([1, 0, 1])
########## Generate mesh points ##########
(pMesh, wpMesh) = myMath.legGaussPoint(nGau, aGau, bGau, "plain") # generate momentum mesh points
(rMesh, wrMesh) = myMath.legGaussPoint(nGau, aGau, 100 / mambda, "plain") # Generate coordinate mesh points. End boundary in coordinate space is usually be taken as 600 / mambda.
# Method in potChi
potC = potChi(mambda, pMesh, wpMesh, rMesh, wrMesh, para)
# Method in potChiral, p, q are setted as the same mesh
potC.potChiral(chs) # Generate potential mesh
sdWave = SDWave(potC.v00MeshR, potC.v01MeshR, potC.v10MeshR, potC.v11MeshR, potC.rMesh, potC.wrMesh)
########## print potential mesh ##########
#plt.figure(0)
#plt.title('Potential in the moementum space')
#plt.subplot(221)
#plt.imshow(potC.v00MeshR)
#plt.colorbar()
#plt.subplot(222)
#plt.imshow(potC.v01MeshR)
#plt.colorbar()
#plt.subplot(223)
#plt.imshow(potC.v10MeshR)
#plt.colorbar()
#plt.subplot(224)
#plt.imshow(potC.v11MeshR)
#plt.colorbar()
#
#plt.figure(1)
#plt.title('Potential in the coordinate space')
#plt.subplot(221)
#plt.imshow(potC.v00MeshP)
#plt.colorbar()
#plt.subplot(222)
#plt.imshow(potC.v01MeshP)
#plt.colorbar()
#plt.subplot(223)
#plt.imshow(potC.v10MeshP)
#plt.colorbar()
#plt.subplot(224)
#plt.imshow(potC.v11MeshP)
#plt.colorbar()
#
#plt.show()
########## zero potential ##########
#sizePot = len(potC.v01MeshR)
#vZeros = np.zeros((sizePot, sizePot))
#sdWave = SDWave(vZeros, vZeros, vZeros, vZeros, potC.rMesh, potC.wrMesh)
########## Find binding energy ##########
#EbArr = np.linspace(-5 / const.HBarC, 0, 50)
#detArr = np.zeros(EbArr.size)
#for i in np.arange(EbArr.size):
#    detArr[i] = sdWave.findDet(EbArr[i])
#plt.figure(0)
#plt.plot(EbArr * const.HBarC, detArr)
#plt.grid()
#plt.show()
########## wave function ##########
eb = sdWave.findEB()
detEb = sdWave.findDet(eb)
print(eb * const.HBarC, detEb)

(sWave, dWave) = sdWave.sdgSolver(atol = 1e-3) # Calculate S- and D- wave function
(rMesh, wrMesh) = myMath.legGaussPoint(nGau, aGau, 100 / mambda, "plain") # Generate coordinate mesh points. End boundary in coordinate space is usually be taken as 600 / mambda.
intS = sWave.dot(wrMesh)
intD = dWave.dot(wrMesh)
print('ratio int (w) / int(u): %f' % (intD / intS))
sdWave.printWave() # Print out the wavefunction and compare it with the ones in doc/waveFunc.pdf
