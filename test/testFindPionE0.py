# find the pionless LO binding energy
import testCOMMON
import constant as const
import numpy as np
from scipy import optimize
from matplotlib import pyplot as plt

def ilambda(mambda, be):
    # mambda: cutoff.
    # be: binding energy, should be negtive
    inside = 2 * np.exp(-2 * be * const.MNucleon / mambda / mambda) * np.sqrt(-be * const.MNucleon) * np.pi - mambda * np.sqrt(2 * np.pi)
    return np.pi * const.MNucleon * inside

def c0a(mambda, be, a0):
    return 1 / (2 * np.pi**2 * const.MNucleon / a0 - const.MNucleon * np.power(np.pi, 3 / 2) * np.sqrt(2) * mambda)

def c0aEqILambda(be, mambda, a0):
    return 1 / c0a(mambda, be, a0) - ilambda(mambda, be)

plt.figure(0)
x = np.linspace(-0.5 / const.HBarC, -10/ const.HBarC, 100)
plt.plot(x, c0aEqILambda(x, 1000 / const.HBarC, const.A0))
plt.show()
sol = optimize.root_scalar(c0aEqILambda, args=(400 / const.HBarC, const.A0), bracket=[-0.5 / const.HBarC, -10/ const.HBarC], method='brentq')
print(sol.root * const.HBarC)


