import testCOMMON
import numpy as np
from waveFunc import WaveFunc as wfunc
import constant as const
import myMath
from matplotlib import pyplot as plt
from src.core.chiral.litSolverChi import litSolverChi as lits
from src.core.chiral.nloLitSolverChi import NLOLitSolverChi as litsNLO
import matOpt
import scipy.linalg as linalg
from scipy import special
import litInversion as litinv
from potChiral import potChiral as potChi
from src.core.chiral.SDWavemm import SDWavemm as SDWave
from src.core.chiral.caphi import Caphi

########## Some useful functioin for testing ##########
def showLitWave(figLab = 5, litUWave = None, litWWave = None):
    plt.figure(figLab)
    plt.plot(rMesh, litUWave.real, label = "real part of lit uWave")
    plt.plot(rMesh, litUWave.imag, label = "imag part of lit uWave")
    plt.plot(rMesh, litWWave.real, label = "real part of lit wWave")
    plt.plot(rMesh, litWWave.imag, label = "imag part of lit wWave")
    plt.legend()
    plt.grid()

########## input ##########
mambda = 600 / const.HBarC
nGau = 300
aGau, bGau = 0, 3 * mambda
aGauR, bGauR = 0 / mambda, 150 / mambda
qChar = np.sqrt(5)
sigmaI = 5 / const.HBarC
sigmaRMesh = np.array([10 / const.HBarC])
chs = np.array([1, 0, 1]) # S-D coupled channel
para = []

########## init ##########
(pMesh, wpMesh) = myMath.legGaussPoint(nGau, aGau, bGau, "plain") # generate momentum mesh points
(rMesh, wrMesh) = myMath.legGaussPoint(nGau, aGauR, bGauR, "plain") # Generate coordinate mesh points. End boundary in coordinate space is usually be taken as 200 / mambda.
potC = potChi(mambda, pMesh, wpMesh, rMesh, wrMesh, para)
potC.potChiral(chs) # Generate potential mesh
wave = SDWave(potC.v00MeshP, potC.v01MeshP, potC.v10MeshP, potC.v11MeshP, potC.pMesh, potC.wpMesh, potC.rMesh, potC.wrMesh)
# Calculate wavefunction
wave.findEB(-10 / const.HBarC, 0)
wave.sdgSolver(atol = const.atolSDGChiralMM) # Calculate S- and D- wave function

litC = lits(wave.uWave, wave.wWave, rMesh, wrMesh, potC.v00MeshR, potC.v01MeshR, potC.v10MeshR, potC.v11MeshR, eb = wave.eb)
########### Check input ##########
#plt.figure(0)
#plt.title('Potential in the moementum space')
#plt.subplot(221)
#plt.imshow(litC.v00MeshRShel)
#plt.colorbar()
#plt.subplot(222)
#plt.imshow(litC.v01MeshRShel)
#plt.colorbar()
#plt.subplot(223)
#plt.imshow(litC.v10MeshRShel)
#plt.colorbar()
#plt.subplot(224)
#plt.imshow(litC.v11MeshRShel)
#plt.colorbar()
#
#plt.show()
#
#litC.printWave() # Print out the wavefunction and compare it with the ones in doc/waveFunc.pdf
########## Check the fLjlGenMesh ##########
#litC.setQChar(np.sqrt(5))
#print(litC.alphaLjl(1, 0, 1))
#print("The ans should be: -1.414213562373095")
#plt.figure(0)
#plt.plot(rMesh, litC.fLjlGen(1, 0, 1))
#plt.show()
########### Check aMatL ##########
#size = 100
#(rMesh, wrMesh) = myMath.legGaussPoint(size, aGauR, bGauR, "plain") # Generate coordinate mesh points. End boundary in coordinate space is usually be taken as 200 / mambda.
##################### Test 1: pot = 0 and eb = 0 ####################
#v00MeshR, v01MeshR, v10MeshR, v11MeshR = np.zeros((size, size)), np.zeros((size, size)), np.zeros((size, size)), np.zeros((size, size)) # Use zero potential to test
#eb = -2 / const.HBarC
#litC = lits(wave.uWave, wave.wWave, rMesh, wrMesh, v00MeshR, v01MeshR, v10MeshR, v11MeshR, eb = eb) # In this special test, wavefunction is not used.
#aMat = litC.genAMat()
############################### Analytical solution in this case ##############################
#d2Opt = matOpt.dOpt(rMesh)
#d2Opt = d2Opt.dot(d2Opt)
#mat00 = - d2Opt / const.MNucleon - eb * np.identity(size)
#mat11 =  + 6 / rMesh / rMesh / const.MNucleon + mat00
#zeroMat = np.zeros( (size, size) )
#aMatAna = np.hstack( \
#    (np.vstack((mat00, zeroMat)), \
#     np.vstack((zeroMat, mat11))) \
#)
#
#plt.figure(3)
#plt.subplot(121)
#plt.title("The numeric solution")
#plt.imshow(aMat)
#plt.colorbar()
#plt.subplot(122)
#plt.title("The analytical solution")
#plt.imshow(aMatAna)
#plt.colorbar()
#plt.show()
##################### Test 2: pot = 0 and eb = 1000 ####################
#litC.setEb(1000)
#aMat = litC.genAMat()
#
#plt.figure(4) # There should be a deep digonal line
#plt.title("The numeric solution")
#plt.imshow(aMat)
#plt.colorbar()
#plt.show()
##################### Test 3: pot00 = 1, pot01 = 2, pot10 = 3, pot11 = 4 and eb = 2 ####################
#v00MeshR, v01MeshR, v10MeshR, v11MeshR = np.identity(size), np.identity(size) * 2, np.identity(size) * 3, np.identity(size) * 4 # Use zero potential to test
#litC = lits(wave.uWave, wave.wWave, rMesh, wrMesh, v00MeshR, v01MeshR, v10MeshR, v11MeshR, eb = 2) # wave functions are not used in here
#aMat = litC.genAMat()
#plt.figure(5) # There should be a deep digonal line
#plt.subplot(131)
#plt.title("v01")
#plt.imshow(v01MeshR)
#plt.colorbar()
#plt.subplot(132)
#plt.title("v01 shelled")
#plt.imshow(litC.v01MeshRShel)
#plt.colorbar()
#plt.subplot(133)
#plt.title("The numeric solution of aMat")
#plt.imshow(aMat)
#plt.colorbar()
#plt.show()
########### Check addSigma ##########
#litC.setEb(-2.2 / const.HBarC)
#aMat = litC.genAMat()
#litC.eq28SolverPrep(sigmaI, qChar)
#aMatSigma = litC.eq28Solver(sigmaRMesh)[0] # !!! Check the result with sigmaR equals 0 and 1 separately. Please uncomment return value for litSolverChi
#
#plt.figure(4) # There should be a deep digonal line
#plt.subplot(131)
#plt.title("The numeric solution")
#plt.imshow(aMat)
#plt.colorbar()
#plt.subplot(132)
#plt.title("The real part of aMat where sigma is added: \n")
#plt.imshow(aMatSigma.real)
#plt.colorbar()
#plt.subplot(133)
#plt.title("The imag part of aMat where sigma is added: \n")
#plt.imshow(aMatSigma.imag)
#plt.colorbar()
#plt.show()
############# Check litWave ##########
#litC.eq28SolverPrep(sigmaI, qChar)
#(litUWave, litWWave) = litC.eq28Solver(sigmaRMesh[0]) # !!! Check the result with sigmaR equals 0 and 1 separately.
#showLitWave(5, litUWave, litWWave)
#plt.show()
########## Check caphi.addSigma() ##########
#sigmaRMesh = np.linspace(1 / const.HBarC, 50 / const.HBarC, 50)
#caphi = Caphi(sigmaRMesh, sigmaI, qChar) # init
#chn = (1, 0, 0, 2)
#caphi.addChn(chn) # add S D channel
#(litUWave, litWWave) = caphi.addSigma(litC, chn)
#plt.figure(6)
#plt.plot(sigmaRMesh * const.HBarC, np.array(caphi.chs[chn][0]) / const.HBarC / const.HBarC)
#plt.plot(sigmaRMesh * const.HBarC, np.array(caphi.chs[chn][1]) / const.HBarC / const.HBarC)
#plt.show()
